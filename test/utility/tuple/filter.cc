/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/utility/tuple/filter.hpp>

using namespace stapl;

exit_code stapl_main(int argc, char** argv)
{
  using lower = std::tuple<
    std::integral_constant<std::size_t, 0ul>,
    std::integral_constant<std::size_t, 2ul>
  >;

  using upper = std::tuple<
    std::integral_constant<std::size_t, 4ul>,
    std::integral_constant<std::size_t, 5ul>,
    std::integral_constant<std::size_t, 6ul>
  >;

  using expected = std::tuple<
    std::integral_constant<std::size_t, 4ul>,
    std::integral_constant<std::size_t, 6ul>
  >;

  using result =
    typename tuple_ops::result_of::heterogeneous_filter<lower, upper>::type;

  static_assert(std::is_same<result, expected>::value,
    "Testing <0,2> <4,5,6>"
  );
  return EXIT_SUCCESS;
}
