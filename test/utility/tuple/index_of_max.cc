/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#define REPORT_WITH_COLOR

#include <stapl/runtime.hpp>
#include <stapl/utility/tuple/index_of_max.hpp>
#include <boost/mpl/int.hpp>

#include "../../test_report.hpp"

using namespace stapl;


exit_code stapl_main(int argc, char** argv)
{
  using tuple0 = std::tuple<
    boost::mpl::int_<0>,
    boost::mpl::int_<3>,
    boost::mpl::int_<2>
  >;

  static_assert(tuple_ops::index_of_max<tuple0>::value == 1,
      "Test with 3-tuple");

  using tuple1 = std::tuple<
    boost::mpl::int_<2>,
    boost::mpl::int_<0>,
    boost::mpl::int_<1>
  >;

  static_assert(tuple_ops::index_of_max<tuple1>::value == 0,
      "Test with max as first");

  using tuple2 = std::tuple<
    boost::mpl::int_<2>
  >;

  static_assert(tuple_ops::index_of_max<tuple2>::value == 0,
      "Test with 1-tuple");

  return EXIT_SUCCESS;
}
