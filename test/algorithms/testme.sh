#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1
PASSED_TEST=0

echo "-------- Testing `pwd` -------------"

for binary in "test_non_mutating" "test_mutating" "test_numeric" "test_sorting";
do
  echo "================================================"
  echo "Running ./$binary Using $run_command"
  echo "================================================"
  eval $run_command ./$binary
  if [ $? -ne 0 ]; then
    echo "ERROR:: $@ - $* exited abnormally"
  else
    echo "PASS:: $@ - $* exited normally"
  fi
  for cpart1 in "-cpart1 balanced" "-cpart1 block 25" "-cpart1 block 32";
  do
    for cpart2 in "-cpart2 block 32";
    do
      ARGS="$cpart1 $cpart2"
      echo "================================================"
      echo "Running ./$binary $ARGS Using $run_command Processors"
      echo "================================================"
      eval $run_command ./$binary $ARGS
      if [ $? -ne 0 ]; then
        echo "ERROR:: $@ - $* exited abnormally"
      else
        echo "PASS:: $@ - $* exited normally"
      fi
    done
  done
done
