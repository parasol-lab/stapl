/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#include <stapl/runtime.hpp>
#include <stapl/containers/sequential/bitset/bitset.hpp>

#include "../../../expect.hpp"

void test_empty()
{
  constexpr std::size_t n = 4;
  stapl::sequential::bitset bs{ n };

  stapl::tests::expect_eq(std::distance(bs.begin(), bs.begin()), 0)
    << "Distance is 0 for empty bitset iterators";

  int sum = std::accumulate(bs.begin(), bs.end(), 0);

  stapl::tests::expect_eq(sum, 0) << "Empty iterator range";
}

void test_single_word()
{
  constexpr std::size_t n = 4;

  stapl::sequential::bitset bs{ n };

  bs.set(2);
  stapl::tests::expect(bs.get(2)) << "Set value is true";

  int sum = 0;
  for (auto it = bs.begin(); it != bs.end(); ++it)
    sum += *it;

  stapl::tests::expect_eq(sum, 2) << "Iterate only single set value";
}

void test_two_words()
{
  constexpr std::size_t n = 70;

  stapl::sequential::bitset bs{ n };

  bs.set(2);
  bs.set(68);
  stapl::tests::expect(bs.get(2) && bs.get(68)) << "Set values are true";

  int sum = 0;
  for (auto it = bs.begin(); it != bs.end(); ++it)
    sum += *it;

  stapl::tests::expect_eq(sum, 70) << "Iterate only two set values";
}

stapl::exit_code stapl_main(int argc, char** argv)
{
  constexpr std::size_t n = 100;

  stapl::sequential::bitset bs{ n };

  bool passed = true;
  for (std::size_t i = 0; i < n; ++i)
    passed &= !bs.get(i);

  stapl::tests::expect(passed) << "Initially set to false";

  bs.set(1);
  stapl::tests::expect(bs.get(1)) << "Set value is true";

  passed = true;
  for (std::size_t i = 0; i < n; ++i)
    if (i != 1)
      passed &= !bs.get(i);

  stapl::tests::expect(passed) << "All other values are still false";

  std::size_t sum = 0;

  for (std::size_t i = 0; i < n; ++i)
    if (bs.get(i))
      sum += i;

  stapl::tests::expect_eq(sum, 1ul) << "manual iteration over set bits";

  bs.set(2);
  bs.set(n - 1);
  auto b = bs.begin();
  auto e = bs.end();

  stapl::tests::expect_eq(std::distance(b, e), 3ul) << "iterator distance";

  sum = std::accumulate(bs.begin(), bs.end(), 0ul);
  stapl::tests::expect_eq(sum, n + 2)
    << "iterator accumulate only processes set indices";

  std::array<std::size_t, 3> expected_vals = { { 1, 2, n - 1 } };

  stapl::tests::expect(
    std::equal(bs.begin(), bs.end(), expected_vals.begin()))
    << "iterator range";

  test_single_word();
  test_two_words();
  test_empty();

  return 0;
}
