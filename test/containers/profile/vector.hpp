/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

////////////////////////////////////////////////////////////////////////////////
/// @file
/// Unit testing / profiling of methods of the stapl::vector and typical views
/// defined over it. std::vector is used for sequential comparison.
////////////////////////////////////////////////////////////////////////////////

#ifndef STAPL_PROFILING_VECTOR_HPP
#define STAPL_PROFILING_VECTOR_HPP

#include "list_vector.hpp"

namespace stapl {

namespace profiling {

template<typename ADT>
void add_vector_profilers(prof_cont_t<ADT>& p,
                          std::string const& name, ADT& adt,
                          std::vector<size_t> const& idx,
                          std::vector<typename ADT::value_type> const& vals,
                          int argc, char** argv)
{
  p.push_back(new push_back_profiler<ADT>(name, &adt, vals, argc, argv));
  p.push_back(new pop_back_profiler<ADT>(name, &adt, vals, argc, argv));
  p.push_back(new add_profiler<ADT>(name, &adt, vals, argc, argv));
  p.push_back(new insert_beg_profiler<ADT>(name, &adt, idx, vals, argc, argv));
  p.push_back(new erase_beg_profiler<ADT>(name, &adt, vals.size(), argc, argv));
}

template<typename T>
void add_vector_profilers(prof_cont_t<std::vector<T>>& p,
                          std::string const& name, std::vector<T>& vec,
                          std::vector<size_t> const& idx,
                          std::vector<T> const& vals,
                          int argc, char** argv)
{
  using ADT = std::vector<T>;

  p.push_back(new push_back_profiler<ADT>(name, &vec, vals, argc, argv));
  p.push_back(new pop_back_profiler<ADT>(name, &vec, vals, argc, argv));
  p.push_back(new insert_beg_profiler<ADT>(name, &vec, idx, vals, argc, argv));
  p.push_back(new erase_beg_profiler<ADT>(name, &vec, vals.size(), argc, argv));
}

} // namespace profiling

} // namespace stapl



#endif // STAPL_PROFILING_VECTOR_HPP
