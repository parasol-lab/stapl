#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1

eval $run_command ./array 1000
if test $? != 0
then
    echo "ERROR:: while testing array"
fi

grep error: const_iterator_write > /dev/null 2>&1
if test $? != 0
then
    echo "ERROR:: const_iterator_write.cc compiled without error"
else
    echo "Testing const_iterator write : PASSED"
fi

eval $run_command ./static_array 1000
if test $? != 0
then
    echo "ERROR:: while testing static array"
fi

eval $run_command ./algorithms 1000
if test $? != 0
then
    echo "ERROR:: while testing algorithms"
fi

eval $run_command ./array_of_vectors 100
if test $? != 0
then
    echo "ERROR:: while testing algorithms"
fi

eval $run_command ./array_viewbased_dist
if test $? != 0
then
    echo "ERROR:: while testing viewbased distributions of array"
fi

eval $run_command ./row_dist_arrays 100 100
if test $? != 0
then
    echo "ERROR:: while testing array distributions with explicit location sets"
fi

eval $run_command ./array_redistribution
if test $? != 0
then
    echo "ERROR:: while testing redistribution of array"
fi

eval $run_command ./array_test
if test $? != 0
then
    echo "ERROR:: while testing const correctness in array_test"
fi

#eval $run_command ./composed 1000 100
#if test $? != 0
#then
#    echo "ERROR:: while testing composed"
#fi

#eval $run_command ./migrate 1000 100
#if test $? != 0
#then
#    echo "ERROR:: while testing migrate"
#fi
