/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_TEST_UTILITIES_HPP
#define STAPL_TEST_UTILITIES_HPP

#include <string>
#include <vector>
#include <boost/program_options.hpp>
#include "./algorithms/test_report.h"

//////////////////////////////////////////////////////////////////////
/// @brief Parse the options on the command line and populate a map of
/// the values provided for each option.
/// @return variables_map is a map from the option (e.g., -t) to the
/// value provided after it (if any)
//////////////////////////////////////////////////////////////////////
boost::program_options::variables_map
get_opts(int argc, char **argv)
{
  try {
    boost::program_options::options_description desc("Allowed options");
    desc.add_options()
      ("help,h", "Print this help message")
      ("data,d",
       boost::program_options::value<std::vector<std::string> >(),
       "Data set")
      ("list,l", "Print tests provided")
      ("noref", "Disable verification with reference")
      ("out,o", "Output file name")
      ("test,t", boost::program_options::value<std::vector<std::string> >(),
       "Test to run")
      ("version,v", boost::program_options::value<std::vector<std::string> >(),
       "Test class to run")
    ;

    boost::program_options::variables_map vm;
    boost::program_options::store(
      boost::program_options::command_line_parser(argc, argv).options(desc)
      .style(boost::program_options::command_line_style::default_style |
             boost::program_options::command_line_style::allow_long_disguise)
      .run(), vm);
    boost::program_options::notify(vm);

    return vm;
  }
  catch(boost::program_options::unknown_option opt) {
    test_error(argv[0], opt.what());
  }
  catch(...) {
    test_error(argv[0], ": exception thrown");
  }
}
#endif
