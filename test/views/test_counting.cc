/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/skeletons/map_reduce.hpp>
#include <stapl/skeletons/transformations/coarse/zip.hpp>
#include <stapl/skeletons/functional/skeleton_traits.hpp>
#include <stapl/skeletons/spans/blocked.hpp>
#include <stapl/algorithms/functional.hpp>
#include <stapl/views/counting_view.hpp>

#include "../test_report.hpp"

using namespace stapl;


stapl::exit_code stapl_main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cout << "usage: exe n" << std::endl;
    exit(1);
  }

  long n = atol(argv[1]);

  long sum = map_reduce(
    stapl::identity<long>(), stapl::plus<long>(), counting_view<long>(n)
  );

  STAPL_TEST_REPORT(
    sum == (n*(n-1))/2,
    "Testing counting_view (default initial value)"
  );

  sum = map_reduce(
    stapl::identity<long>(), stapl::plus<long>(), counting_view<long>(n, -(n/2))
  );

  STAPL_TEST_REPORT(
    ((n%2==0) && (sum==-(n/2)))||(sum==0),
    "Testing counting_view (with initial value)"
  );

  sum = map_reduce(
    stapl::identity<long>(), stapl::plus<long>(), counting_view<long>(2));

  STAPL_TEST_REPORT(
    sum == 1,
    "Testing counting_view (with two elements)"
  );

  return EXIT_SUCCESS;
}
