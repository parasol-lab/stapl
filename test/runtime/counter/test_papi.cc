/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE papi_timers
#include <chrono>
#include <thread>
#include "utility.h"
#include <stapl/runtime/counter/counter.hpp>
#include <stapl/runtime/counter/papi/papi_counter.hpp>
#include <stapl/runtime/counter/papi/papi_cycle_counter.hpp>
#include <stapl/runtime/counter/papi/papi_timer_n_counter.hpp>
#include <stapl/runtime/counter/papi/papi_timer.hpp>
#include <cmath>

using namespace stapl;

static int initialize(void)
{
  if (PAPI_is_initialized()!=PAPI_LOW_LEVEL_INITED &&
      PAPI_library_init(PAPI_VER_CURRENT)!=PAPI_VER_CURRENT)
    std::abort();
  return 0;
}

int init = initialize();

static void loop(void)
{
  std::this_thread::sleep_for(std::chrono::seconds{1});
}


BOOST_AUTO_TEST_CASE( test_papi_counter )
{
  counter<papi_counter> c(PAPI_L1_DCM, PAPI_TOT_CYC);
  std::cout << c.native_name() << ": ";

  c.start();
  loop();
  counter<papi_counter>::value_type v = c.stop();

  typedef counter<papi_counter>::value_type::const_iterator iterator_type;
  for (iterator_type begin = v.begin(), end = v.end(); begin!=end; ++begin) {
    std::cout << *begin << ' ';
  }
  std::cout << std::endl;
}

BOOST_AUTO_TEST_CASE( test_papi_cycle_counter )
{
  counter<papi_cycle_counter> c;
  std::cout << c.native_name() << ": " << c.read() << ' ';

  c.start();
  loop();
  std::cout << c.stop() << std::endl;
}

BOOST_AUTO_TEST_CASE( test_papi_timer_n_counter )
{
  counter<papi_timer_n_counter> c(PAPI_L1_DCM);
  std::cout << c.native_name() << ": ";

  c.start();
  loop();
  counter<papi_timer_n_counter>::value_type v = c.stop();

  std::cout << v.time << "  ";
  typedef counter<papi_counter>::value_type::const_iterator iterator_type;
  for (iterator_type begin = v.counters.begin(), end = v.counters.end();
         begin!=end; ++begin) {
    std::cout << *begin << ' ';
  }
  std::cout << std::endl;
}

BOOST_AUTO_TEST_CASE( test_papi_timer )
{
  counter<papi_timer> c;
  std::cout << c.native_name() << ": " << c.read() << ' ';

  c.start();
  loop();
  std::cout << c.stop() << std::endl;
}
