/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TEST_SERIALIZATION_UTILITY_H
#define STAPL_RUNTIME_TEST_SERIALIZATION_UTILITY_H

#define BOOST_TEST_MODULE STAPL_RUNTIME_TEST_MODULE

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

#ifdef BOOST_TEST_USE_INCLUDED
# include <boost/test/included/unit_test.hpp>
#else
# define BOOST_TEST_DYN_LINK
# include <boost/test/unit_test.hpp>
#endif

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <functional>
#include <valarray>

// Bring in required instances for Boost.Serialization
#include "../../../src/runtime/serialization.cc"


struct buffer
{
  char*       buf;
  std::size_t size;

  explicit buffer(std::size_t s)
  : buf(new char[3 * s]),
    size(s)
  {
    std::fill(buf,          buf + size,   0);
    std::fill(buf + size,   buf + 2*size, 'a');
    std::fill(buf + 2*size, buf + 3*size, 0);
  }

  ~buffer(void)
  {
    std::fill(buf, buf + 3*size, 0);
    delete[] buf;
  }

  bool overwritten(void) const noexcept
  {
    typedef std::not_equal_to<char> wf_type;
    if (std::find_if(buf, (buf + size),
                     [](char c) { return (c!=0); }) != (buf+size))
      return true;

    if ( std::find_if((buf + 2*size), (buf + 3*size),
                      [](char c) { return (c!=0); }) != (buf + 3*size) )
      return true;

    // not overwritten
    return false;
  }

  const void* address(void) const noexcept
  { return &buf[size]; }

  void* address(void) noexcept
  { return &buf[size]; }
};

namespace stapl {

namespace runtime {

void assert_fail(const char* assertion,
                 const char* file, unsigned int line, const char* function)
{
  std::cerr << assertion
            << " (file: "     << file
            << ", function: " << function
            << ", line: "     << line << ")\n";
  std::abort();
}

void assert_fail(const char* assertion, const char* function)
{
  std::cerr << assertion << " (function: " << function << ")\n";
  std::abort();
}

} // namespace runtime

} // namespace stapl


template<typename T>
void check_equal(T const& x, T const& y) noexcept
{
  BOOST_REQUIRE(x==y);
}


template<typename T1, typename T2>
void check_equal(std::pair<T1, T2> const& x,
                 std::pair<T1, T2> const& y) noexcept
{
  BOOST_CHECK_EQUAL(x.first,  y.first);
  BOOST_CHECK_EQUAL(x.second, y.second);
}

template<typename T>
void check_equal(std::valarray<T> const& x,
                 std::valarray<T> const& y) noexcept
{
  BOOST_CHECK_EQUAL(x.size(), y.size());
  for (std::size_t i = 0; i < x.size(); ++i)
    check_equal(x[i], y[i]);
}


template<typename V, typename T>
void check_equal_associative(T const& x, T const& y)
{
  BOOST_CHECK_EQUAL(x.size(), y.size());
  std::vector<V> v1{std::begin(x), std::end(x)};
  std::vector<V> v2{std::begin(y), std::end(y)};
  std::sort(v1.begin(), v1.end());
  std::sort(v2.begin(), v2.end());
  BOOST_CHECK_EQUAL(v1.size(), v2.size());
  for (typename std::vector<V>::size_type i = 0; i < v1.size(); ++i)
    check_equal(v1[i], v2[i]);
}


template<typename Key, typename T>
void check_equal(std::multimap<Key, T> const& x,
                 std::multimap<Key, T> const& y)
{ check_equal_associative<std::pair<Key, T>>(x, y); }


template<typename T>
void check_equal(std::reference_wrapper<T> const& x,
                 std::reference_wrapper<T> const& y) noexcept
{ check_equal(x.get(), y.get()); }

#endif
