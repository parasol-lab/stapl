/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE intrusive_mpsc_queue<T>
#include "utility.h"
#include <stapl/runtime/concurrency/intrusive_mpsc_queue.hpp>
#include <algorithm>
#include <numeric>
#include <vector>

using namespace stapl::runtime;


struct A
: public intrusive_mpsc_queue_hook
{
  unsigned int thread;
  unsigned int value;

  A(unsigned int th, unsigned int v)
  : thread(th),
    value(v)
  { }
};


static intrusive_mpsc_queue<A> q;


void do_it(unsigned int tid, unsigned int N)
{
  for (unsigned int i=0; i<N; ++i) {
    A* a = new A(tid, i);
    q.push(*a);
  }
}


BOOST_AUTO_TEST_CASE( queue_writes )
{
  unsigned int NTHREADS = 32;

#if defined(STAPL_RUNTIME_USE_OMP)

# pragma omp parallel shared(q)
  {
#  pragma omp single
    NTHREADS = omp_get_num_threads();
    int i = omp_get_thread_num();

    do_it(i, (i*i));
  }

#else

  // create some threads
  std::vector<std::thread> threads;
  threads.reserve(NTHREADS);
  for (unsigned int i=0; i<NTHREADS; ++i) {
    threads.emplace_back([i] { do_it(i, (i*i)); });
  }

  BOOST_CHECK_EQUAL(NTHREADS, threads.size());

  // wait for them
  for (unsigned int i=0; i<threads.size(); ++i) {
    threads[i].join();
  }

#endif

  // gather all values from all threads pushed to the queue
  std::vector<std::vector<unsigned int> > v(NTHREADS);
  for (A* a = q.try_pop(); a != 0; a = q.try_pop()) {
    v[a->thread].push_back(a->value);
    delete a;
  }

  // values between threads can interleave, but for the same thread they have
  // to be in the same order
  for (unsigned int i=0; i<NTHREADS; ++i) {
    std::vector<unsigned int> const& tmp_v = v[i];
    std::vector<unsigned int> check_v(i*i);
    std::iota(check_v.begin(), check_v.end(), 0);
    BOOST_CHECK_EQUAL(tmp_v.size(), check_v.size());
    BOOST_REQUIRE(
      std::equal(check_v.begin(), check_v.end(), tmp_v.begin())==true
    );
  }
}
