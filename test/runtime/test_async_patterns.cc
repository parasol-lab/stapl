/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// This test stresses various complex patterns of RMI communication, focusing
/// on data exchanges, common in forwarding. An immature implementation may hang
/// easily on this test.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <iostream>
#include "test_utils.h"

using namespace stapl;

struct p_test
: public p_object
{
  const unsigned int m_id;
  const unsigned int m_lt;
  const unsigned int m_rt;
  unsigned int       m_value;

  p_test(void)
  : m_id(this->get_location_id()),
    m_lt((m_id==0) ? (this->get_num_locations()-1) : (m_id - 1)),
    m_rt((m_id==this->get_num_locations()-1) ? 0 : (m_id + 1)),
    m_value(0)
  { this->advance_epoch(); }

  void reset(void)
  { m_value = 0; }

  void set_value(unsigned int v)
  { m_value = v; }

  void set_remote_value(unsigned int location, unsigned int value)
  { async_rmi(location, get_rmi_handle(), &p_test::set_value, value); }

  // Tests exchange of values through async_rmi()
  void test_exchange_values(void)
  {
    reset();
    rmi_fence(); // wait for all locations to reset

    async_rmi(m_rt, get_rmi_handle(), &p_test::set_value, 1);
    block_until([this] { return (this->m_value!=0); });
  }

  // Tests exchange of values in a nested RMI call
  void test_exchange_values_nested(void)
  {
    reset();
    rmi_fence(); // wait for all locations to reset

    async_rmi(m_rt, get_rmi_handle(), &p_test::set_remote_value, m_id, 1);
    block_until([this] { return (this->m_value!=0); });
  }

  void execute(void)
  {
    // Test using default/maximum aggregation/poll_rate on the first pass
    test_exchange_values();
    test_exchange_values_nested();

    // minimal settings on the second pass
    set_aggregation( 1 );
    test_exchange_values();
    test_exchange_values_nested();
  }
};


exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();
#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
