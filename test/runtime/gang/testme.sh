#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1
TEST_OUTPUT_PREFIX='(STAPL RTS gang)'

eval $run_command ./test_gang
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_gang - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_gang - [passed]"
fi

eval $run_command ./test_gang_switch
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_gang_switch - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_gang_switch - [passed]"
fi

eval $run_command ./test_gang_comm
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_gang_comm - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_gang_comm - [passed]"
fi

eval $run_command ./test_nested_gangs
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_nested_gangs - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_nested_gangs - [passed]"
fi

eval $run_command ./test_intragang
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_intragang - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_intragang - [passed]"
fi

eval $run_command ./test_intergang
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_intergang - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_intergang - [passed]"
fi

eval $run_command ./test_lss
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_lss - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_lss - [passed]"
fi
