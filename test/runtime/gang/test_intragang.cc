/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test intragang communication with complex patterns.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <iostream>
#include <cstdlib>
#include <functional>
#include "../test_utils.h"

static void async_pattern(void)
{
  p_test_object g;

  stapl::async_rmi(g.get_right_neighbor(), g.get_rmi_handle(),
                   &p_test_object::set, 0, g.get_location_id());
  stapl::async_rmi(g.get_right_neighbor(), g.get_rmi_handle(),
                   &p_test_object::test, 0, g.get_location_id());

  stapl::rmi_fence(); // wait for async_rmi calls to finish
  STAPL_RUNTIME_TEST_CHECK(g.get(0), int(g.get_left_neighbor()));
}


// Creates a p_object and makes some local communication
static void comm_pattern(const unsigned int N)
{
  p_test_object g;

  // async_rmi and sync_rmi
  stapl::async_rmi(g.get_right_neighbor(), g.get_rmi_handle(),
                   &p_test_object::set, 0, N*g.get_location_id());
  stapl::async_rmi(g.get_right_neighbor(), g.get_rmi_handle(),
                   &p_test_object::test, 0, N*g.get_location_id());
  const int rg = stapl::sync_rmi(g.get_right_neighbor(), g.get_rmi_handle(),
                                 &p_test_object::get, 0);
  STAPL_RUNTIME_TEST_CHECK(int(N*g.get_location_id()), rg);

  // reduction
  typedef std::plus<unsigned int> plus_op;
  unsigned int RV = 0;
  for (unsigned int s = 1; s<g.get_num_locations(); ++s) {
    RV = plus_op()(RV, s);
  }
  const unsigned int r1 =
    stapl::sync_reduce_rmi(plus_op(), g.get_rmi_handle(),
                           &p_test_object::get_location_id);
  STAPL_RUNTIME_TEST_CHECK(r1, RV);

  stapl::future<unsigned int> r =
    stapl::allreduce_rmi(plus_op(),
                         g.get_rmi_handle(), &p_test_object::get_location_id);
  const unsigned int out = r.get();
  STAPL_RUNTIME_TEST_CHECK(out, RV);

  // create gang and execute again
  if (N!=0) {
    stapl::execute([N] { comm_pattern(N-1); });
  }

  const unsigned int r2 =
    stapl::sync_reduce_rmi(plus_op(), g.get_rmi_handle(),
                           &p_test_object::get_location_id);
  STAPL_RUNTIME_TEST_CHECK(r2, RV);
  stapl::rmi_fence(); // wait for sync_rmi_reduce calls to finish
  STAPL_RUNTIME_TEST_CHECK(int(N*g.get_left_neighbor()), g.get(0));
}


struct p_test
: public stapl::p_object
{
  void test_async(void)
  {
    async_pattern();
  }

  void test_comm_pattern(void)
  {
    for (int i=0; i<5; ++i) {
      comm_pattern(5);
    }
  }

  void execute(void)
  {
    test_async();
    test_comm_pattern();
    test_async();
  }
};


stapl::exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();
#ifndef _TEST_QUIET
  std::cout << stapl::get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
