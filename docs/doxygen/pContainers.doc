//*****************************************************************************
// The following is the pContainers component of STAPL, along with the detailed
// descriptions and subsections.  All subsections should appear in the desired
// order of appearance, and are defined by @defgroup.  Source code that belongs
// in a section or subsection should indicate as such with @addtogroup.
//*****************************************************************************

/**
 * @defgroup pContainers pContainers
* \b OVERVIEW
*
*A \e pContainer is the parallel equivalent of the STL container
*and is backward compatible with STL containers through its ability to
*provide iterators.  Each pContainer provides (semi--) random access
*to its elements, a prerequisite for efficient parallel processing.
*Random access to the subsets of a pContainer's data is provided by an
*internal distribution maintained by the pContainer.  The distribution is
*updated when elements are added or removed from the pContainer, and when
*migration of elements between locations is requested.  The distribution has
*two primary components.  The container manager maintains the subsets of
*elements stored on a location.  Each subset is referred to as a bContainer. A
*pContainer instance may have more than one bContainer per location depending
*on the desired data distribution and independent migration of elements.  The
*second component of the distribution is the directory, which enables any
*location to determine the location on which an element of the pContainer is
*stored.
*
* \b IMPLEMENTATION
*
*The pContainers currently implemented in STAPL are array and vector.
.
*
*Each provides a common interface needed for parallel operations and also
*provides methods similar to the STL counterpart (e.g., vector::push_back).
**/

// Container Framework

//////////////////////////////////////////////////////////////////////
/// @ingroup pContainers
/// @defgroup pcf Parallel Container Framework
/// @brief Base classes that facilitate the development of new parallel
/// containers.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup pcf
/// @defgroup pcfTraits Traits Component Customization
/// @brief Classes that specify customizable types that control container
/// behavior, and may be changed for a given container instance.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup pcf
/// @defgroup pcfManip Element Manipulation
/// @brief Base classes that implement element manipulation capabilities
/// of a container.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup pcf
/// @defgroup pcfDist Data Distribution Management
/// @brief Base classes that implement data distribution policies.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup pcf
/// @defgroup pcfRMI Distributed Object Access
/// @brief Base classes that implement remote access of a container
/// instance.
//////////////////////////////////////////////////////////////////////


// Array

//////////////////////////////////////////////////////////////////////
/// @ingroup pContainers
/// @defgroup parray Array
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup parray
/// @defgroup parrayTraits Array Traits
/// @brief Specification of the customizable types of a container that
/// can be changed for a given container instance.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup parray
/// @defgroup parrayDistObj Distributed Object Access
/// @brief Specializations of @ref stapl::proxy that provide a remote
/// reference for the container.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup parray
/// @defgroup parrayDist Memory and Domain Management
/// @brief Internal classes implementing functionality related to data
/// distribution.
//////////////////////////////////////////////////////////////////////


// Vector

//////////////////////////////////////////////////////////////////////
/// @ingroup pContainers
/// @defgroup pvector Vector
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup pvector
/// @defgroup pvectorTraits Vector Traits
/// @brief Specification of the customizable types of a container that
/// can be changed for a given container instance.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup pvector
/// @defgroup pvectorDistObj Distributed Object Access
/// @brief Specializations of @ref stapl::proxy that provide a remote
/// reference for the container.
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/// @ingroup pvector
/// @defgroup pvectorDist Memory and Domain Management
/// @brief Internal classes implementing functionality related to data
/// distribution.
//////////////////////////////////////////////////////////////////////



