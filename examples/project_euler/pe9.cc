/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <vector>
#include <algorithm>
#include <utility>
#include <stapl/array.hpp>
#include <stapl/algorithm.hpp>
#include <stapl/runtime/counter/default_counters.hpp>

using namespace stapl;

// work function is used to check whether a, b, c make a Pythagorean triple
struct inner
{
  typedef int value_type;
  typedef int result_type;

  value_type m_a;

  inner(value_type a)
    : m_a(a)
  {}

  template <typename Value>
  result_type operator()(Value B)
  {
    // The problem states a, b, and c must add up to 1000
    value_type C = 1000 - m_a - B;

    if (((m_a*m_a) + (B*B)) == (C*C))
      return m_a*B*C;
    else
      return 0;
  }

  void define_type(typer&t)
  {
    t.member(m_a);
  }
};

// work function stores the value of a and calls a map reduce in order to nest
// the map_reduce functions so every value of a can be tested with every value
// of b
struct outer
{
  typedef int result_type;

  template <typename Value>
  result_type operator()(Value A)
  {
    inner i_f(A);

    // represented in pseudocode as a for loop from 1 to 500
    return map_reduce(i_f,stapl::max<int>(),stapl::counting_view<int>(500,1));
  }
};


stapl::exit_code stapl_main(int, char **)
{
  stapl::counter<stapl::default_timer> timer;
  timer.start();

  // Represented in pseudocode as a for loop from 1 to 1000
  auto a = map_reduce(outer(), stapl::max<int>(),
             stapl::counting_view<int>(1000,1));

  auto time = timer.stop();
  stapl::do_once([=]{std::cout << time << std::endl;});
  stapl::do_once([=]{std::cout << a << std::endl;});
  return EXIT_SUCCESS;
}
