#include <algorithm>
#include <benchmarks/algorithms/timer.hpp>
#include <benchmarks/algorithms/utilities.hpp>
#include <chrono>
#include <cmath>
#include <float.h>
#include <iostream>
#include <stapl/array.hpp>
#include <stapl/domains/indexed.hpp>
#include <stapl/numeric.hpp>
#include <stapl/utility/do_once.hpp>
#include <stapl/vector.hpp>
#include <tuple>

using namespace std;

struct BLSResult {
  double best_period;
  double best_duration;
  double best_phase;
  double best_d_value;

  void define_type(stapl::typer &t) {
    t.member(best_period);
    t.member(best_duration);
    t.member(best_phase);
    t.member(best_d_value);
  }
};

template <typename T> T 
min_value(const vector<T> &v) {
  return *min_element(v.begin(), v.end());
}

/**
 * Generates a vector of linearly spaced values.
 *
 * @param start The starting value of the sequence.
 * @param end The ending value of the sequence.
 * @param num The number of values to generate.
 * @return A vector containing the linearly spaced values.
 */
vector<double> linspace(double start, double end, size_t num) {
  vector<double> result(num);
  double step = (end - start) / (num - 1);
  for (size_t i = 0; i < num; ++i) {
    result[i] = start + i * step;
  }
  return result;
}

/**
 * @brief Generates a sequence of numbers from start to end with a specified step.
 *
 * This function creates a vector of doubles starting from the 'start' value,
 * incrementing by 'step', and stopping before the 'end' value.
 *
 * @param start The starting value of the sequence.
 * @param end The end value of the sequence (not inclusive).
 * @param step The increment value between each number in the sequence.
 * @return A vector of doubles containing the generated sequence.
 */
vector<double> arange(double start, double end, double step) {
  vector<double> result;
  for (double value = start; value < end; value += step) {
    result.push_back(value);
  }
  return result;
}

/**
 * @brief Normalizes the given flux vector by subtracting the mean and dividing by the standard deviation.
 *
 * This function takes a vector of flux values, calculates the mean, and subtracts the mean from each element.
 * It then calculates the standard deviation of the resulting values and divides each element by the standard deviation.
 * The result is a vector of normalized flux values with a mean of 0 and a standard deviation of 1.
 *
 * @param flux A reference to a vector of double values representing the flux.
 * @return A vector of double values representing the normalized flux.
 */
vector<double> normalize(vector<double> &flux) {
  vector<double> normalized_flux(flux.size());
  double mean_flux = accumulate(flux.begin(), flux.end(), 0.0) / flux.size();

  for (size_t i = 0; i < flux.size(); ++i) {
    normalized_flux[i] = flux[i] - mean_flux;
  }

  double squared_sum =
      inner_product(normalized_flux.begin(), normalized_flux.end(),
                    normalized_flux.begin(), 0.0);

  double sigma = sqrt(squared_sum / flux.size());

  for (auto &f : normalized_flux) {
    f /= (sigma + numeric_limits<double>::epsilon());
  }

  return normalized_flux;
}

/**
 * @brief Computes the relative time vector.
 *
 * This function takes a vector of time values and computes a new vector where each element
 * is the difference between the corresponding time value and the minimum time value in the input vector.
 *
 * @param time A reference to a vector of double values representing time.
 * @return A vector of double values representing the relative time.
 */
vector<double> compute_trel(vector<double> &time) {
  double t0 = min_value(time);
  vector<double> t_rel(time.size(), 0.0);

  for (size_t i = 0; i < time.size(); ++i) {
    t_rel[i] = time[i] - t0;
  }

  return t_rel;
}

/**
 * @brief Computes the weights for the BLS (Box Least Squares) algorithm based on the given flux errors.
 *
 * This function calculates the weights for each flux error by taking the inverse square of the flux error.
 * The weights are then normalized by dividing each weight by the sum of all weights.
 *
 * @param flux_err A vector of flux errors.
 * @return A vector of normalized weights.
 */
vector<double> compute_weights(vector<double> &flux_err) {
  vector<double> weights(flux_err.size());
  for (size_t i = 0; i < flux_err.size(); ++i) {
    weights[i] = 1.0 / (flux_err[i] * flux_err[i]);
  }

  double sum_weights = accumulate(weights.begin(), weights.end(), 0.0);

  assert(fabs(sum_weights) > numeric_limits<double>::epsilon() &&
         "Sum of weights must be greater than epsilon");

  for (auto &w : weights) {
    w /= sum_weights;
  }

  return weights;
}

/**
 * @brief Computes the d value of a candidate for astrophysical analysis.
 *
 * This function calculates the d value, which is used to determine the best candidate
 * based on the given time-relative values, flux, weights, period, duration, and phase.
 * The candidate with the lowest d value is considered the best.
 *
 * @param t_rel Vector of time-relative values.
 * @param flux Vector of flux values.
 * @param weights Vector of weight values.
 * @param period The period of the candidate.
 * @param duration The duration of the candidate.
 * @param phase The phase of the candidate.
 * @return The computed d value for the candidate.
 */
double model(vector<double> &t_rel, vector<double> &flux,
             vector<double> &weights, double period, double duration,
             double phase) {
  vector<size_t> is_transit(flux.size());
  for (size_t i = 0; i < flux.size(); ++i) {
    is_transit[i] = ((fmod(t_rel[i], period) >= phase) &&
                     fmod(t_rel[i], period) <= phase + duration)
                        ? 1
                        : 0;
  }

  double r =
      inner_product(weights.begin(), weights.end(), is_transit.begin(), 0.0);

  double s = 0.0;
  for (size_t i = 0; i < flux.size(); ++i) {
    s += weights[i] * flux[i] * is_transit[i];
  }

  double wx =
      inner_product(weights.begin(), weights.end(), flux.begin(), 0.0, plus<>(),
                    [](double w, double f) { return w * f * f; });

  double d_value =
      wx - (s * s) / (r * (1 - r)) + numeric_limits<double>::epsilon();

  return d_value;
}

/**
 * @brief Main function to perform the Box Least Squares (BLS) algorithm.
 *
 * This function loops over all candidate parameters and determines which one
 * has the lowest d-value, indicating the best fit for the given time and flux data.
 *
 * @param time Vector of time data points.
 * @param flux Vector of flux data points corresponding to the time data.
 * @param flux_err Vector of flux error values corresponding to the flux data.
 * @param s_params Vector of tuples containing candidate parameters (period, duration, phase).
 * 
 * @return BLSResult containing the best fit parameters (period, duration, phase) and the corresponding d-value.
 */
BLSResult bls(vector<double> &time, vector<double> &flux,
              vector<double> &flux_err,
              vector<tuple<double, double, double>> &&s_params) {

  vector<double> t_rel = compute_trel(time);
  vector<double> normalized_flux = normalize(flux);
  vector<double> weights = compute_weights(flux_err);

  BLSResult result;
  result.best_d_value = DBL_MAX;

  for (const auto &params : s_params) {
    double period = get<0>(params);
    double duration = get<1>(params);
    double phase = get<2>(params);

    double d_value =
        model(t_rel, normalized_flux, weights, period, duration, phase);

    if (d_value < result.best_d_value) {
      result.best_d_value = d_value;
      result.best_period = period;
      result.best_duration = duration;
      result.best_phase = phase;
    }
  }

  return result;
}

/**
 * @struct map_wf_coarse
 * @brief A functor that performs the Box Least Squares (BLS) algorithm on a given spectrum vector view.
 *
 * This struct is designed to apply the BLS algorithm to a range of spectrum vectors and find the one with the best fit.
 *
 * @param m_time A vector of time values.
 * @param m_flux A vector of flux values.
 * @param m_flux_err A vector of flux error values.
 *
 * @fn map_wf_coarse::map_wf_coarse(vector<double> &time, vector<double> &flux, vector<double> &flux_err)
 * @brief Constructor to initialize the time, flux, and flux error vectors.
 * @param time A reference to a vector of time values.
 * @param flux A reference to a vector of flux values.
 * @param flux_err A reference to a vector of flux error values.
 *
 * @tparam T The type of the spectrum vector view.
 * @fn map_wf_coarse::operator()(T &&spec_vector_view)
 * @brief Applies the BLS algorithm to each element in the spectrum vector view and returns the best result.
 * @param spec_vector_view A view of the spectrum vectors to be processed.
 * @return BLSResult The result of the BLS algorithm with the best fit.
 *
 * @fn map_wf_coarse::define_type(stapl::typer &t)
 * @brief Defines the type for serialization.
 * @param t A reference to the stapl::typer object.
 */
struct map_wf_coarse {

  vector<double> m_time;
  vector<double> m_flux;
  vector<double> m_flux_err;

  map_wf_coarse(vector<double> &time, vector<double> &flux,
                vector<double> &flux_err)
      : m_time(time), m_flux(flux), m_flux_err(flux_err) {}

  template <typename T> 
  BLSResult operator()(T &&spec_vector_view) {
    BLSResult temporary;
    BLSResult global;
    global.best_d_value = DBL_MAX;

    auto start = spec_vector_view.container().begin();
    auto end = spec_vector_view.container().end();
    for (; start != end; start++) {
      temporary = bls(m_time, m_flux, m_flux_err, std::move(*start));
      if (temporary.best_d_value < global.best_d_value) {
        global = temporary;
      }
    }

    return global;
  }

  void define_type(stapl::typer &t) {
    t.member(m_time);
    t.member(m_flux);
    t.member(m_flux_err);
  }
};

/**
 * @struct reduce_wf
 * @brief A functor for reducing two BLSResult candidates to the one with the better d-value.
 *
 * This functor is used to compare two BLSResult candidates and return the one with the smaller d-value.
 *
 * @tparam T1 Type of the first candidate.
 * @tparam T2 Type of the second candidate.
 *
 * @param candidate_l The first BLSResult candidate.
 * @param candidate_r The second BLSResult candidate.
 * @return The BLSResult candidate with the smaller d-value.
 */
struct reduce_wf {
  template <typename T1, typename T2>
  BLSResult operator()(T1 &&candidate_l, T2 &&candidate_r) {
    BLSResult left_c, right_c;
    left_c = candidate_l;
    right_c = candidate_r;

    return left_c.best_d_value < right_c.best_d_value ? left_c : right_c;
  }
};

/**
 * @struct wf_coarse
 * @brief A functor that generates a coarse grid of periods, durations, and phases for astrophysical analysis.
 *
 * This functor iterates over a range of spectral data and periods, generating a vector of tuples
 * containing period, duration, and phase values. The durations are fixed to a range from 1 to 11
 * with a step of 1, and the phases are generated from 0 to the period value with a step of 0.5.
 *
 * @tparam SPEC Type of the spectral data container.
 * @tparam PERIOD Type of the periods container.
 */
struct wf_coarse {
  template <typename SPEC, typename PERIOD>
  void operator()(SPEC &&specs, PERIOD &&periods) const {
    auto start = specs.begin();
    auto end = specs.end();

    auto periods_start = periods.begin();

    vector<double> durations = linspace(1, 11, 11);

    for (; start != end; start++, periods_start++) {
      vector<double> phases = arange(0, *periods_start, 0.5);
      for (const auto &d : durations) {
        for (const auto &phi : phases) {
          (*start).push_back(make_tuple(*periods_start, d, phi));
        }
      }
    }
  }
};

/**
 * @brief Main function for the BLS STAPL application.
 *
 * This function performs the Box Least Squares (BLS) algorithm to detect periodic
 * signals in time-series data. It generates fake data for testing purposes and
 * measures the execution time for multiple samples.
 *
 * @details
 * The function generates fake time-series data with a known periodic signal and
 * applies the BLS algorithm to detect the period. The execution time for each
 * sample is recorded and reported.
 *
 * @expected_output
 * Given the input parameters:
 * - period: 50.0
 * - duration: 5.0
 * - phase: 0.1 * period
 * - size of time array: 5000
 * 
 * The output is:
 * - d_value: 0.0000000000002841
 * 
 * Given the input parameters:
 * - period: 50.0
 * - duration: 5.0
 * - phase: 0.1 * period
 * - size of time array: 10000
 * 
 * The output is:
 * - d_value: -0.0000000000001062
 */
stapl::exit_code stapl_main(int argc, char *argv[]) {

  if (argc < 2) {
    stapl::do_once([] {
      std::cout << "usage: ./bls_stpl solution_size num_samples" << std::endl;
    });
    return EXIT_FAILURE;
  }

  size_t size = 5000;
  size_t num_samples = 1;

  if (argc == 2) {
    size = atol(argv[1]);
  } else if (argc == 3) {
    size = atol(argv[1]);
    num_samples = atoi(argv[2]);
  }

  counter_t timer;

  // fake data to test
  double real_period = 50.0;
  double real_phase = 5.0;
  double real_duration = 0.1 * real_period;
  double real_diff = 0.05;

  double threshold = cosf64x(M_PI * real_duration / real_period);
  vector<double> time = linspace(0, 400, size);
  vector<double> flux(time.size());

  for (size_t i = 0; i < time.size(); ++i) {
    flux[i] =
        cosf64x(2.0 * M_PI * (time[i] - real_phase - real_duration / 2.0) /
                real_period) > threshold
            ? 1.0
            : 0.0;
  }

  for (size_t i = 0; i < time.size(); ++i) {
    flux[i] = 1.0 - real_diff * flux[i];
  }
  vector<double> flux_err(time.size(), 0.01);
  // end of fake data

  std::vector<double> cont_samples(num_samples, 0.);
  for (size_t sample = 0; sample < num_samples; ++sample) {

    timer.reset();
    timer.start();

    using SPECParameters = vector<tuple<double, double, double>>;
    using cont_t = stapl::array<SPECParameters>;
    using view_t = stapl::array_view<cont_t>;
    cont_t spec_array(101);
    view_t spec_view(spec_array);
    vector<double> periods = linspace(45, 55, 101);
    stapl::array_view<vector<double>> periods_view(periods);
    stapl::map_func<stapl::skeletons::tags::with_coarsened_wf>(
        wf_coarse(), spec_view, periods_view);

    BLSResult result =
        stapl::map_reduce<stapl::skeletons::tags::with_coarsened_wf>(
            map_wf_coarse(time, flux, flux_err), reduce_wf(), spec_view);

    cont_samples[sample] = timer.stop();
  }

  report_result("BLS STAPL", "STAPL", true, cont_samples);

  return EXIT_SUCCESS;
}