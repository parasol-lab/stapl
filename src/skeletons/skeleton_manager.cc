/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/
#include <stapl/runtime/serialization/typer_traits.hpp>
#include <stapl/skeletons/executors/skeleton_manager.hpp>

namespace stapl {
namespace skeletons {

skeleton_manager::skeleton_manager(void)
  : m_is_done(false)
{ }

//////////////////////////////////////////////////////////////////////
/// @brief This method resumes the spawning process by spawning the
/// element in the front of the memento deque if it is not a lazy
/// element.
///
/// If all the elements of the memento double-ended queue are already
/// resumed and there is nothing else left to spawn, the skeleton manager
/// assumes it is done with the spawning process and will not be invoked
/// anymore by the @c paragraph.
//////////////////////////////////////////////////////////////////////
void skeleton_manager::resume()
{
  if (m_memento_stack.is_empty()) {
    this->m_is_done = true;
  } else {
    m_memento_stack.resume();
  }
}

} // namespace skeletons
} // namespace stapl