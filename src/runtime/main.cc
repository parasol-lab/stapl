/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <stapl/runtime/runtime.hpp>
#include <stapl/runtime/executor/anonymous_executor.hpp>
#include <cstdlib>
#include <limits>

namespace stapl {

void main_wf::operator()(void)
{
  anonymous_executor e; // FIXME remove this
  const exit_code ex = m_f(m_argc, m_argv);
  if (ex!=EXIT_SUCCESS) {
    std::exit(ex.get_code()); // FIXME
  }
  ex.wait();
}

} // namespace stapl


#ifndef NO_STAPL_MAIN

//////////////////////////////////////////////////////////////////////
/// @brief The starting point for SPMD user code execution.
///
/// It replaces the sequential equivalent:
/// @code int main(int argc, char* argv[]) @endcode
///
/// @ingroup ARMI
//////////////////////////////////////////////////////////////////////
extern stapl::exit_code stapl_main(int argc, char *argv[]);


int main(int argc, char *argv[])
{
  using namespace stapl;

  option opt{argc, argv};
  initialize(opt);
  execute(main_wf{argc, argv, &stapl_main},
          opt.get<unsigned int>("STAPL_MAIN_LEVELS",
                                std::numeric_limits<unsigned int>::max()));
  finalize();

  return EXIT_SUCCESS;
}

#endif
