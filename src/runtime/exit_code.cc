/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <stapl/runtime/exit_code.hpp>
#include <stapl/runtime/runtime.hpp>
#include <stapl/runtime/synchronization.hpp>

namespace stapl {

// Returns from an application
exit_code::exit_code(int code)
: m_code(code)
{
  using namespace runtime;

  if (m_code==EXIT_SUCCESS) {
    // finish all RMIs
    rmi_fence(this_context::get());
  }
}


// Barrier for all locations exiting an application
void exit_code::wait(void) const
{
  using namespace runtime;

  // unsuccessful execution
  if (m_code!=EXIT_SUCCESS)
    return;

  // termination detection and fence for world executor
  auto& ctx = this_context::get();
  auto& l   = ctx.get_location_md();
  rmi_fence(ctx, [&l]
                 {
                   auto* const ex = l.try_get_executor();
                   if (ex) {
                     while ((*ex)()==runnable_base::Active);
                   }
                   return true;
                 });
}

} // namespace stapl
