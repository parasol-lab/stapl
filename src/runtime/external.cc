/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <stapl/runtime/context.hpp>
#include <stapl/runtime/exception.hpp>
#include <stapl/runtime/non_rmi/external.hpp>

namespace stapl {

// Returns the location ids that are going to make the external call
std::set<unsigned int> external_callers(void)
{
  using namespace runtime;

  std::set<unsigned int> ec;

  context* const ctx = this_context::try_get();
  if (!ctx) {
    // only one location here
    ec.insert(0);
  }
  else {
    // we know we have specific number of processes, we are searching for the
    // leader locations
    gang_md const& g  = ctx->get_gang_md();
    const auto nlocs  = g.size();
    const auto nprocs = g.get_description().get_num_processes();
    std::set<process_id> pids;
    for (gang_md::size_type i = 0; i<nlocs && ec.size()!=nprocs; ++i) {
      const process_id pid = g.get_process_id(i);
      if (pids.find(pid)==pids.end()) {
        pids.insert(pid);
        ec.insert(i);
      }
    }
    STAPL_RUNTIME_ASSERT(ec.size()==nprocs);
  }
  return ec;
}

} // namespace stapl
