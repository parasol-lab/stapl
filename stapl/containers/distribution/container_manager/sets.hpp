/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_CONTAINER_MANAGER_SETS_HPP
#define STAPL_CONTAINERS_CONTAINER_MANAGER_SETS_HPP

#include <stapl/containers/sequential/bitset/bitset.hpp>
#include <stapl/views/type_traits/is_contiguous_domain.hpp>

#include <vector>

#include <boost/iterator/iterator_facade.hpp>

namespace stapl {

namespace cm_impl {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief A mapping function used to transform a GID by adding the
/// starting GID of its base container
//////////////////////////////////////////////////////////////////////
struct multi_offset_mapping_function
{
  template<typename GID, typename BaseContainer>
  static GID apply(GID g, BaseContainer const& bc)
  {
    return g + bc.domain().first();
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief A mapping function used as an identity for GIDs
//////////////////////////////////////////////////////////////////////
struct multi_identity_mapping_function
{
  template<typename GID, typename BaseContainer>
  static GID apply(GID g, BaseContainer&&)
  {
    return g;
  }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Iterator for the single_gid_set, that traverses all elements
/// in the GID set and produces local references
/// @tparam BaseContainer The type of the base container
/// @tparam Iterator The iterator for the GID set
/// @tparam MappingFunction A function to translate GIDs on dereference
/// @tparam Reference The proxy type of the elements in the base container
//////////////////////////////////////////////////////////////////////
template <typename BaseContainer,
          typename Iterator,
          typename MappingFunction,
          typename Reference>
class single_gid_set_iterator
  : public boost::iterator_facade<single_gid_set_iterator<BaseContainer,
                                                          Iterator,
                                                          MappingFunction,
                                                          Reference>,
                                  Reference,
                                  boost::forward_traversal_tag,
                                  Reference>
{
private:
  friend class boost::iterator_core_access;

  using base_container_type = BaseContainer;
  using base_iterator = Iterator;
  using mapping_function_type = MappingFunction;
  using reference_type = Reference;

  base_container_type* m_base_container;
  base_iterator m_current_it;

public:
  single_gid_set_iterator(base_container_type* base_container,
                          base_iterator current_it)
    : m_base_container(base_container)
    , m_current_it(std::move(current_it))
  { }

private:
  reference_type dereference(void) const
  {
    const auto gid
      = mapping_function_type::apply(*m_current_it, *m_base_container);

    return m_base_container->make_reference(gid);
  }

  bool equal(single_gid_set_iterator const& other) const
  {
    return m_current_it == other.m_current_it;
  }

  void increment(void)
  {
    ++m_current_it;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief A set of GIDs, where all GIDs are in the same base container
/// @tparam Registry The container manager's registry type
/// @tparam Storage How the GIDs will be stored internally
//////////////////////////////////////////////////////////////////////
template <typename Registry,
          typename Storage = std::vector<typename Registry::gid_type>>
class single_gid_set
{
  using registry_type = Registry;
  using base_container_type = typename registry_type::base_container_type;

public:
  using gid_type = typename base_container_type::gid_type;

private:
  using vector_type = Storage;
  using reference_type = typename base_container_type::reference;
  using mapping_function_type = detail::multi_identity_mapping_function;

public:
  using iterator = single_gid_set_iterator<base_container_type,
                                           typename vector_type::iterator,
                                           mapping_function_type,
                                           reference_type>;
private:
  vector_type m_set;
  registry_type* m_registry;

public:
  single_gid_set(Registry* registry)
    : m_set(), m_registry(registry)
  {
    stapl_assert(registry->size() == 1,
                 "Single implied GID set is for a single base container");
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Add a GID to the set
  //////////////////////////////////////////////////////////////////////
  void add(gid_type const& v)
  {
    this->add_impl(
      v, typename std::is_same<vector_type, std::vector<gid_type>>::type{});
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by the first GID in the set
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    return iterator{m_registry->begin(), m_set.begin()};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by one past the last GID
  /// in the set
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return iterator{m_registry->begin(), m_set.end()};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Swap the storage with another GID set
  //////////////////////////////////////////////////////////////////////
  void swap(single_gid_set& other)
  {
    stapl_assert(this->m_registry = other.m_registry,
                 "Swapping should only be for the same container");
    this->m_set.swap(other.m_set);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Clear the GID set
  //////////////////////////////////////////////////////////////////////
  void clear()
  {
    m_set.clear();
  }

private:
  void add_impl(gid_type const& v, std::true_type)
  {
    m_set.push_back(v);
  }

  void add_impl(gid_type const& v, std::false_type)
  {
    m_set.insert(v);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief A set of GIDs, where every element in the registry is considered
/// in the set
/// @tparam Registry The container manager's registry type
//////////////////////////////////////////////////////////////////////
template<typename Registry>
class single_implied_gid_set
{
  using registry_type = Registry;
  using base_container_type = typename registry_type::base_container_type;

public:
  using gid_type = typename base_container_type::gid_type;
  using iterator = typename base_container_type::iterator;

private:
  base_container_type* m_base_container;

public:
  single_implied_gid_set(Registry* registry)
    : m_base_container(std::addressof(*registry->begin()))
  {
    stapl_assert(registry->size() == 1,
                 "Single implied GID set is for a single base container");
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Add a GID to the set. Does nothing for implied sets.
  //////////////////////////////////////////////////////////////////////
  void add(gid_type const& v)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by the first GID in the set
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    return m_base_container->begin();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the one past the last element in the registry
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return m_base_container->end();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Swap the storage with another GID set. Does nothing.
  //////////////////////////////////////////////////////////////////////
  void swap(single_implied_gid_set& other)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Clear the GID set. Does nothing.
  //////////////////////////////////////////////////////////////////////
  void clear()
  { }
};


//////////////////////////////////////////////////////////////////////
/// @brief A set of GIDs represented as a bitset, where all GIDs are in
/// the same base container
/// @tparam Registry The container manager's registry type
//////////////////////////////////////////////////////////////////////
template<typename Registry>
class single_bitmap_gid_set
{

  using registry_type = Registry;
  using base_container_type = typename registry_type::base_container_type;
public:
  using gid_type = typename base_container_type::gid_type;

private:
  static_assert(
    is_contiguous_domain<typename base_container_type::domain_type>::value,
    "Can't use bitmap with non-contiguous domains");

public:
  using bitset_type = stapl::sequential::bitset;
  using reference_type = typename base_container_type::reference;
  using set_index_iterator = typename bitset_type::const_iterator;
  using mapping_function_type = detail::multi_offset_mapping_function;
  using iterator = single_gid_set_iterator<base_container_type,
                                           set_index_iterator,
                                           mapping_function_type,
                                           reference_type>;

private:
  stapl::sequential::bitset m_bitmap;
  registry_type* m_registry;
  gid_type m_start;

public:
  single_bitmap_gid_set(Registry* registry)
    : m_bitmap(registry->begin()->domain().size())
    , m_registry(registry)
    , m_start(registry->begin()->domain().first())
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Add a GID to the set. Does nothing for implied sets.
  //////////////////////////////////////////////////////////////////////
  __attribute__((always_inline)) inline void add(gid_type const& v)
  {
    m_bitmap.set(v - m_start);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by the first GID in the set
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    return iterator{m_registry->begin(), m_bitmap.begin()};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by one past the last GID
  /// in the set
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return iterator{m_registry->begin(), m_bitmap.end()};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Swap the storage with another GID set
  //////////////////////////////////////////////////////////////////////
  void swap(single_bitmap_gid_set& other)
  {
    this->m_bitmap.swap(other.m_bitmap);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Clear the GID set
  //////////////////////////////////////////////////////////////////////
  void clear()
  {
    m_bitmap.clear();
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Iterator for the multi_gid_set, that traverses all elements
/// in the GID set and produces local references
/// @tparam Map An unordered_map that maps base container iterators to GID sets
/// @tparam MappingFunction A function to translate GIDs on dereference
/// @tparam Reference The proxy type of the elements in the base container
//////////////////////////////////////////////////////////////////////
template<typename Map, typename MappingFunction, typename Reference>
class multi_gid_set_iterator
: public boost::iterator_facade<
           multi_gid_set_iterator<Map, MappingFunction, Reference>,
           Reference,
           boost::forward_traversal_tag,
           Reference>
{
private:
  friend class boost::iterator_core_access;

  using map_iterator = typename Map::const_iterator;
  using vector_type = typename Map::mapped_type;
  using vector_iterator = typename vector_type::const_iterator;
  using mapping_function_type = MappingFunction;
  using reference_type = Reference;

  const Map* m_map;
  map_iterator m_vector_it;
  vector_iterator m_current_it;

public:
  multi_gid_set_iterator()
    : m_map(nullptr), m_vector_it(), m_current_it()
  { }

  multi_gid_set_iterator(const Map* m,
                         map_iterator vector_it,
                         vector_iterator current_it)
    : m_map(m), m_vector_it(vector_it), m_current_it(current_it)
  { }

private:
  reference_type dereference(void) const
  {
    auto& bc = *m_vector_it->first;
    auto gid = mapping_function_type::apply(*m_current_it, bc);

    return bc[gid];
  }

  bool equal(multi_gid_set_iterator const& other) const
  {
    return m_vector_it == other.m_vector_it
      && m_current_it == other.m_current_it;
  }

  void increment(void)
  {
    // If at the end of the current gid set, go to the next non-empty one
    if (std::next(m_current_it) == m_vector_it->second.end()) {
      m_vector_it = std::find_if(
        std::next(m_vector_it),
        m_map->end(),
        [](typename Map::value_type const& kv) { return !kv.second.empty(); });

      // If we're at the end of all the gid sets, set to invalid iterator
      if (m_vector_it == m_map->end()) {
        m_vector_it = map_iterator{};
        m_current_it = vector_iterator{};
      } else {
        m_current_it = m_vector_it->second.begin();
      }
    } else {
      ++m_current_it;
    }
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief A set of GIDs, where GIDs can be stored across multiple
/// base containers
/// @tparam Registry The container manager's registry type
/// @tparam Storage How the GIDs will be stored internally
//////////////////////////////////////////////////////////////////////
template <typename Registry,
          typename Storage = std::vector<typename Registry::gid_type>>
class multi_gid_set
{
  using base_container_type = typename Registry::base_container_type;
  using registry_iterator = typename Registry::const_iterator;

public:
  using gid_type = typename base_container_type::gid_type;

private:
  using reference_type =
    typename std::iterator_traits<registry_iterator>::value_type::reference;
  using map_type = std::
    unordered_map<registry_iterator, Storage, stapl::hash<registry_iterator>>;

  using mapping_function_type = detail::multi_identity_mapping_function;

  map_type m_sets;
  Registry* m_registry;

public:
  using iterator
    = multi_gid_set_iterator<map_type, mapping_function_type, reference_type>;

  multi_gid_set(Registry* registry) : m_registry(registry)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Add a GID to the set
  //////////////////////////////////////////////////////////////////////
  void add(gid_type const& v)
  {
    auto it = m_registry->find(v);

    stapl_assert(
      it != m_registry->end(),
      "Trying to add a vertex to the frontier on the wrong location");

    this->add_impl(
      v,
      m_sets[it],
      typename std::is_same<Storage, std::vector<gid_type>>::type{});
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by the first GID in the set
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    // Find first non-empty vector
    auto first_non_empty_it
      = std::find_if(m_sets.begin(),
                     m_sets.end(),
                     [](typename map_type::value_type const& kv) {
                       return !kv.second.empty();
                     });

    if (first_non_empty_it == m_sets.end())
      return this->end();

    return iterator{ &m_sets,
                     first_non_empty_it,
                     first_non_empty_it->second.begin() };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by one past the last GID
  /// in the set
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return iterator{};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Clear the GID set
  //////////////////////////////////////////////////////////////////////
  void clear()
  {
    m_sets.clear();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Swap the storage with another GID set
  //////////////////////////////////////////////////////////////////////
  void swap(multi_gid_set& other)
  {
    this->m_sets.swap(other.m_sets);
  }

private:
  void add_impl(gid_type const& v, Storage& set, std::true_type)
  {
    set.push_back(v);
  }

  void add_impl(gid_type const& v, Storage& set, std::false_type)
  {
    set.insert(v);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief A set of GIDs (stored as bitsets), where GIDs can be stored
/// across multiple base containers
/// @tparam Registry The container manager's registry type
//////////////////////////////////////////////////////////////////////
template<typename Registry>
class multi_bitmap_gid_set
{
  using base_container_type = typename Registry::base_container_type;
  using registry_iterator = typename Registry::const_iterator;

public:
  using gid_type = typename base_container_type::gid_type;

private:
  using reference_type =
    typename std::iterator_traits<registry_iterator>::value_type::reference;

  using map_type = std::unordered_map<registry_iterator,
                                      stapl::sequential::bitset,
                                      stapl::hash<registry_iterator>>;

  using mapping_function_type = detail::multi_offset_mapping_function;

  map_type m_sets;
  Registry* m_registry;

public:
  using iterator
    = multi_gid_set_iterator<map_type, mapping_function_type, reference_type>;

  multi_bitmap_gid_set(Registry* registry)
    : m_registry(registry)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Add a GID to the set
  //////////////////////////////////////////////////////////////////////
  void add(gid_type const& v)
  {
    auto it = m_registry->find(v);

    stapl_assert(
      it != m_registry->end(),
      "Trying to add a vertex to the frontier on the wrong location");

    const gid_type start = it->domain().first();

    auto bitset_it = m_sets.find(it);

    if (bitset_it == m_sets.end())
      bitset_it = m_sets
                    .emplace(std::piecewise_construct,
                             std::forward_as_tuple(it),
                             std::forward_as_tuple(it->domain().size()))
                    .first;

    bitset_it->second.set(v - start);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by the first GID in the set
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    auto first_non_empty_it
      = std::find_if(m_sets.begin(),
                     m_sets.end(),
                     [](typename map_type::value_type const& kv) {
                       return !kv.second.empty();
                     });

    if (first_non_empty_it == m_sets.end())
      return this->end();

    return iterator{ &m_sets,
                     first_non_empty_it,
                     first_non_empty_it->second.begin() };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by one past the last GID
  /// in the set
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return iterator{};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Clear the GID set
  //////////////////////////////////////////////////////////////////////
  void clear()
  {
    m_sets.clear();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Swap the storage with another GID set
  //////////////////////////////////////////////////////////////////////
  void swap(multi_bitmap_gid_set& other)
  {
    this->m_sets.swap(other.m_sets);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief A hierarchical iterator that traverses all elements in the
/// base containers managed by a container manager.
/// @tparam Registry The container manager's registry
/// @tparam Reference The proxy type of the elements in the base container
//////////////////////////////////////////////////////////////////////
template<typename Registry, typename Reference>
class multi_implied_gid_set_iterator
: public boost::iterator_facade<
           multi_implied_gid_set_iterator<Registry, Reference>,
           Reference,
           boost::forward_traversal_tag,
           Reference>
{
private:
  friend class boost::iterator_core_access;

  using registry_iterator = typename Registry::iterator;
  using base_container_iterator =
    typename Registry::base_container_type::iterator;
  using reference_type = Reference;

  Registry* m_registry;
  registry_iterator m_bc_it;
  base_container_iterator m_current_it;

public:
  multi_implied_gid_set_iterator()
    : m_registry(nullptr), m_bc_it(), m_current_it()
  { }

  multi_implied_gid_set_iterator(Registry* registry,
                         registry_iterator registry_it,
                         base_container_iterator current_it)
    : m_registry(registry), m_bc_it(registry_it), m_current_it(current_it)
  { }

private:
  reference_type dereference(void) const
  {
    return *m_current_it;
  }

  bool equal(multi_implied_gid_set_iterator const& other) const
  {
    return m_bc_it == other.m_bc_it
      && m_current_it == other.m_current_it;
  }

  void increment(void)
  {
    // If at the end of the current bc, go to the next one
    if (std::next(m_current_it) == m_bc_it->end()) {
      ++m_bc_it;

      // If we're at the end of all the bcs, set to invalid iterator
      if (m_bc_it == m_registry->end()) {
        m_bc_it = registry_iterator{};
        m_current_it = base_container_iterator{};
      } else {
        m_current_it = m_bc_it->begin();
      }
    // Still within the current base container
    } else {
      ++m_current_it;
    }
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief A set of GIDs, where every element in every base container
/// managedy by a container manager is considered part of the set.
/// @tparam Registry The container manager's registry type
//////////////////////////////////////////////////////////////////////
template<typename Registry>
class multi_implied_gid_set
{
  using base_container_type = typename Registry::base_container_type;

public:
  using gid_type = typename base_container_type::gid_type;

private:
  using reference_type = typename base_container_type::reference;

  Registry* m_registry;

public:
  using iterator = multi_implied_gid_set_iterator<Registry, reference_type>;

  multi_implied_gid_set(Registry* registry)
    : m_registry(registry)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Add a GID to the set. Does nothing for implied sets.
  //////////////////////////////////////////////////////////////////////
  void add(gid_type const& v)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the element referenced by the first GID in the set
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    if (m_registry->size() == 0)
      return this->end();

    return iterator{ m_registry,
                     m_registry->begin(),
                     m_registry->begin()->begin() };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief An iterator to the one past the last element in the registry
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return iterator{};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Clear the GID set. Does nothing.
  //////////////////////////////////////////////////////////////////////
  void clear()
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Swap the storage with another GID set. Does nothing.
  //////////////////////////////////////////////////////////////////////
  void swap(multi_implied_gid_set& other)
  {
    stapl_assert(other.m_registry == this->m_registry,
                 "Can only swap gid sets of the same registry");
  }
};

} // namespace cm_impl

//////////////////////////////////////////////////////////////////////
/// @brief A metafunction that computes whether or not a GID set is
/// an implied GID set
//////////////////////////////////////////////////////////////////////
template<typename GidSet>
struct is_implied_gid_set
  : std::false_type
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for the multi-base-container implied GID set
//////////////////////////////////////////////////////////////////////
template<typename Registry>
struct is_implied_gid_set<cm_impl::multi_implied_gid_set<Registry>>
  : std::true_type
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for the single-base-container implied GID set
//////////////////////////////////////////////////////////////////////
template<typename Registry>
struct is_implied_gid_set<cm_impl::single_implied_gid_set<Registry>>
  : std::true_type
{ };

} // namespace stapl

#endif
