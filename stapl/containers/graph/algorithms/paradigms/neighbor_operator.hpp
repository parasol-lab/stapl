/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_KLA_NEIGHBOR_OPERATOR_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_KLA_NEIGHBOR_OPERATOR_HPP

#include <stapl/containers/graph/views/graph_view.hpp>
#include <stapl/containers/distribution/container_manager/sets.hpp>
#include <stapl/containers/graph/algorithms/paradigms/visit_logger.hpp>
#include <stapl/containers/graph/algorithms/paradigms/frontier.hpp>
#include <stapl/paragraph/paragraph.hpp>
#include <stapl/algorithms/functional.hpp>
#include <stapl/views/repeated_view.hpp>
#include <stapl/views/localize_element.hpp>

namespace stapl {

namespace kla_detail {

//////////////////////////////////////////////////////////////////////
/// @brief Enum to specify under what conditions a vertex operator will
///        be invoked through a neighbor operator.
//////////////////////////////////////////////////////////////////////
enum class reinvoke_behavior
{
  never, upto_k, avoid_hubs
};


//////////////////////////////////////////////////////////////////////
/// @brief Functor to visit the target vertex with the user provided
/// neighbor-operator.
///
/// Implements the KLA visit pattern, which invokes the user's
/// neighbor-operator on the visited vertex and reinvokes the
/// vertex operator if the both the visit was successful and
/// the sufficient propagation condition is met (e.g., the
/// current level is less than the max allowed level for the
/// KLA superstep).
///
/// @tparam VertexOp Type of the user provided vertex-operator.
/// @tparam NeighborOp Type of the user provided neighbor-operator.
/// @tparam Derived The derived neighbor operator wrapper which
/// implements the logic for propagation
/// @ingroup pgraphAlgoDetails
//////////////////////////////////////////////////////////////////////
template<typename VertexOp, typename NeighborOp, typename Derived>
class neighbor_operator_apply_base
{
  using frontier_type = typename VertexOp::frontier_type;
  using descriptor_type = typename frontier_type::descriptor_type;

  VertexOp   m_vertex_op;
  NeighborOp m_neighbor_op;

  Derived const& derived() const
  {
    return static_cast<Derived const&>(*this);
  }

public:
  using result_type = void;

  //////////////////////////////////////////////////////////////////////
  /// @param wf A copy of the currently executing vertex_operator_apply,
  /// containing information about the current and the max KLA-level of
  /// this visit.
  /// @param uf The user provided neighbor-operator.
  //////////////////////////////////////////////////////////////////////
  neighbor_operator_apply_base(VertexOp wf, NeighborOp uf)
    : m_vertex_op(std::move(wf)), m_neighbor_op(std::move(uf))
  {
    // Increment the level by one.
    m_vertex_op.increment_level();
  }

  template<typename Graph, typename Vertex>
  void operator()(Graph* graph, Vertex&& target) const
  {
    constexpr bool frontier_has_storage
      = !is_implied_gid_set<typename frontier_type::set_type>::value;

    sgl::visit_logger::pre_neighbor_op(target, m_neighbor_op);

    // invoke the neighbor operator and record whether or not we should
    // reinvoke the vertex operator on this vertex afterwards
    const bool reinvoke = m_neighbor_op(std::forward<Vertex>(target));

    // If the sufficient propagation condition holds (e.g., we have not reached
    // the end of a KLA superstep)
    const bool can_repropagate = derived().propagation_condition(
      std::forward<Vertex>(target));

    sgl::visit_logger::post_neighbor_op(
      target, m_neighbor_op, reinvoke, can_repropagate);

    if (reinvoke) {
      if (can_repropagate) {
        m_vertex_op(std::forward<Vertex>(target));
      } else {
        if (frontier_has_storage) {
          frontier_type* frontier
            = graph->template get_frontier<frontier_type>();
          frontier->add(target.descriptor());
        }
      }
    }
  }

  NeighborOp const& neighbor_op() const
  {
    return m_neighbor_op;
  }

  VertexOp const& vertex_op() const
  {
    return m_vertex_op;
  }

  void define_type(typer& t)
  {
    t.member(m_vertex_op);
    t.member(m_neighbor_op);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Functor to visit the target vertex with the user provided
/// neighbor-operator.
///
///
/// @tparam ReinvokeBehavior Under which conditions the vertex operator
///         should be reinvoked
/// @tparam VertexOp Type of the user provided vertex-operator.
/// @tparam NeighborOp Type of the user provided neighbor-operator.
/// @ingroup pgraphAlgoDetails
//////////////////////////////////////////////////////////////////////
template <reinvoke_behavior ReinvokeBehavior,
          typename VertexOp,
          typename NeighborOp>
struct neighbor_operator_apply;

//////////////////////////////////////////////////////////////////////
/// @brief Functor to visit the target vertex with the user provided
/// neighbor-operator.
///
/// This is a facade for @see neighbor_operator_apply_base where
/// the trigger for propagation is only if the level criterion
/// is met (i.e., revisitation will be in the same KLA superstep).
///
/// @tparam VertexOp Type of the user provided vertex-operator.
/// @tparam NeighborOp Type of the user provided neighbor-operator.
/// @ingroup pgraphAlgoDetails
//////////////////////////////////////////////////////////////////////
template<typename VertexOp, typename NeighborOp>
struct neighbor_operator_apply<reinvoke_behavior::upto_k, VertexOp, NeighborOp>
 : public neighbor_operator_apply_base<VertexOp, NeighborOp,
     neighbor_operator_apply<reinvoke_behavior::upto_k, VertexOp, NeighborOp>
   >
{
  typedef neighbor_operator_apply_base<VertexOp, NeighborOp,
     neighbor_operator_apply<reinvoke_behavior::upto_k, VertexOp, NeighborOp>
   > base_type;

public:
  template<typename... Args>
  neighbor_operator_apply(Args&&... args)
   : base_type(std::forward<Args>(args)...)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Condition to determine if a vertex operator should be invoked
  ///        on the same vertex after the neighbor operator. In this case,
  ///        we only consider the KLA superstep condition.
  /// @param target The vertex that was sucessfully visited
  //////////////////////////////////////////////////////////////////////
  template<typename Vertex>
  bool propagation_condition(Vertex&&) const
  {
    return this->vertex_op().level() <= this->vertex_op().max_level();
  }

  void define_type(typer& t)
  {
    t.base<base_type>(*this);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Functor to visit the target vertex with the user provided
/// neighbor-operator.
///
/// This is a specialization for @see neighbor_operator_apply_base where
/// the trigger for propagation is both the level criterion
/// and that the visited vertex's out-degree falls below a threshold.
///
/// @tparam VertexOp Type of the user provided vertex-operator.
/// @tparam NeighborOp Type of the user provided neighbor-operator.
/// @ingroup pgraphAlgoDetails
//////////////////////////////////////////////////////////////////////
template <typename VertexOp, typename NeighborOp>
struct neighbor_operator_apply<reinvoke_behavior::avoid_hubs,
                               VertexOp,
                               NeighborOp>
  : public neighbor_operator_apply_base<VertexOp,
                                        NeighborOp,
                                        neighbor_operator_apply<
                                          reinvoke_behavior::avoid_hubs,
                                          VertexOp,
                                          NeighborOp>>
{
  typedef neighbor_operator_apply_base<VertexOp,
                                       NeighborOp,
                                       neighbor_operator_apply<
                                         reinvoke_behavior::avoid_hubs,
                                         VertexOp,
                                         NeighborOp>>
    base_type;

public:
  template<typename... Args>
  neighbor_operator_apply(Args&&... args)
   : base_type(std::forward<Args>(args)...)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Condition to determine if a vertex operator should be invoked
  ///        on the same vertex after the neighbor operator. In this case,
  ///        we consider both the KLA superstep condition and if this
  ///        vertex's out-degree is below a given threshold (which is defined
  ///        in the vertex operator wrapper (@see vertex_operator_apply)).
  /// @param target The vertex that was sucessfully visited
  //////////////////////////////////////////////////////////////////////
  template<typename Vertex>
  bool propagation_condition(Vertex&& target) const
  {
    return this->vertex_op().level() <= this->vertex_op().max_level() &&
     target.size() < this->vertex_op().degree_threshold();
  }

  void define_type(typer& t)
  {
    t.base<base_type>(*this);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Functor to visit the target vertex with the user provided
/// neighbor-operator.
///
/// This is a specialization for @see neighbor_operator_apply_base where
/// the vertex operator is never reinvoked.
///
/// @tparam VertexOp Type of the user provided vertex-operator.
/// @tparam NeighborOp Type of the user provided neighbor-operator.
/// @ingroup pgraphAlgoDetails
//////////////////////////////////////////////////////////////////////
template<typename VertexOp, typename NeighborOp>
struct neighbor_operator_apply<reinvoke_behavior::never, VertexOp, NeighborOp>
 : private NeighborOp
{
private:
  using frontier_type = typename VertexOp::frontier_type;
  using descriptor_type = typename frontier_type::descriptor_type;

  NeighborOp const& neighbor_op() const
  {
    return static_cast<NeighborOp const&>(*this);
  }

public:
  template<typename Unused>
  neighbor_operator_apply(Unused const&, NeighborOp neighbor_op)
   : NeighborOp(std::move(neighbor_op))
  { }

  template<typename Graph, typename Vertex>
  void operator()(Graph* graph, Vertex&& target) const
  {
    constexpr bool frontier_has_storage
      = !is_implied_gid_set<typename frontier_type::set_type>::value;

    sgl::visit_logger::pre_neighbor_op(target, this->neighbor_op());

    const bool is_active = this->neighbor_op()(std::forward<Vertex>(target));

    if (is_active && frontier_has_storage) {
      frontier_type* frontier = graph->template get_frontier<frontier_type>();
      frontier->add(target.descriptor());
    }

    sgl::visit_logger::post_neighbor_op(
      target, this->neighbor_op(), is_active, false);

  }

  template<typename Vertex>
  bool propagation_condition(Vertex&&) const
  {
    return false;
  }

  void define_type(typer& t)
  {
    t.base<NeighborOp>(*this);
  }
};

} // namespace kla_detail

} // namespace stapl

#endif
