/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_ORDERING_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_ORDERING_HPP

namespace stapl {

namespace sgl {

//////////////////////////////////////////////////////////////////////
/// @brief Valid operator orderings for a graph algorithm.
///
/// For some algorithms, the orderings of vertex operator and neighbor
/// operator are sometimes important. For example, invoking a vertex operator
/// on the same vertex twice in a row without an intermediate neighbor operator
///  would result in an invalid state for an algorithm like label-propagation.
///
/// We ask the algorithm writer to provide a list of operator orderings that
/// is valid:
/// - vop -> nop -> vop
/// - vop -> vop
///
/// The (vop -> nop -> vop) ordering is called an interleaved ordering and
/// (vop -> vop) is called a successive ordering.
///
/// We are left with four cases:
/// - Only (vop -> nop -> vop) is allowed (interleaved)
///   - *Note*: this is the case for label-propagation
/// - Both (vop -> nop -> vop) and (vop -> vop) is allowed (both)
/// - *Note*: this is the case for most graph algorithms
/// - Only (vop -> vop) is allowed (successive)
///   - *Note*: a useful algorithm would not have this restriction
/// - Neither is allowed (neither)
///   - *Note*: a useful algorithm would not have this restriction
//////////////////////////////////////////////////////////////////////
enum class ordering_type {
  neither, interleaved, successive, both
};

using both_ordering_type
  = std::integral_constant<sgl::ordering_type, ordering_type::both>;

using interleaved_ordering_type
  = std::integral_constant<sgl::ordering_type, ordering_type::interleaved>;

using default_ordering_type = both_ordering_type;

constexpr both_ordering_type both_ordering = {};
constexpr interleaved_ordering_type interleaved_ordering = {};
constexpr default_ordering_type default_ordering = {};

} // namespace sgl

} // namespace stapl

#endif
