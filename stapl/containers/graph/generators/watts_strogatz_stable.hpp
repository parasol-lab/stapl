/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_GENERATORS_WS_STABLE_HPP
#define STAPL_CONTAINERS_GRAPH_GENERATORS_WS_STABLE_HPP

namespace stapl {

namespace generators {

//////////////////////////////////////////////////////////////////////
/// @brief A stable implementation of watts_strogatz that generates the
/// the same random Watts-Strogatz graph for a specified number of
/// simulated processors (irrespective to the actual number of processes
/// being run on).
///
/// @todo Need to fix the stability of @ref ws_rewire and use
/// @ref make_watts_strogatz instead.
//////////////////////////////////////////////////////////////////////
template<typename G>
struct watts_strogatz_stable
{
  protected:
    G&     m_graph;
    size_t m_n;
    size_t m_k;
    double m_p;

  public:
    watts_strogatz_stable(G& g, size_t k, double p)
      : m_graph(g), m_n(g.size()), m_k(k), m_p(p)
    { }

    void add_edges(size_t simulated_procs)
    {
      size_t p_per = simulated_procs/get_num_locations();
      size_t p_start = get_location_id()*p_per;
      size_t p_end = p_start + p_per;

      size_t block_size = m_n/simulated_procs;

      //simulate each processor
      //(in the loop, p := get_simulated_location_id() and
      //  simulated_procs := get_num_simulated_locations())
      for (size_t p = p_start; p < p_end; ++p) {
        srand48((p+65)*4321);

        size_t subview_start = block_size*p;
        size_t subview_end = subview_start+block_size;

        for (size_t i=subview_start; i<subview_end; ++i) {
          std::set<size_t> dests;
          for (size_t k=1; k<m_k/2+1; ++k) {
            size_t j = (i+k)%m_n;
            // if we draw a good prob, just add the edge
            // (there is a chance we already randomly added the edge;
            //   in this case, we should redo the random rewiring)
            // otherwise, randomly "rewire" the edge
            if (drand48() >= m_p) {
              if (!dests.insert(j).second) {
                while (!dests.insert(generate_random(i)).second);
              }
            } else {
              while (!dests.insert(generate_random(i)).second);
            }
          }
          for (auto&& d : dests) {
            m_graph.add_edge_async(i, d);
          }
        }
      }
    }

    size_t generate_random(size_t source)
    {
      size_t dest = drand48()*m_n;
      while (dest == source) {
        dest = drand48()*m_n;
      }
      return dest;
    }
};

} // namespace generators

} // namespace stapl

#endif
