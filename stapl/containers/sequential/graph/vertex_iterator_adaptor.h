/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_SEQUENTIAL_GRAPH_VERTEX_ITERATOR_ADAPTOR_HPP
#define STAPL_CONTAINERS_SEQUENTIAL_GRAPH_VERTEX_ITERATOR_ADAPTOR_HPP

#include <boost/iterator/iterator_adaptor.hpp>

namespace stapl {

namespace sequential {

//////////////////////////////////////////////////////////////////////
/// @brief Vertex descriptor iterator.
/// Dereferencing provides the vertex descriptor.
/// @ingroup graphBaseUtil
//////////////////////////////////////////////////////////////////////
template <typename VertexIterator>
class vd_iterator
  : public boost::iterator_adaptor<vd_iterator<VertexIterator>,
    VertexIterator>
{
private:
  typedef boost::iterator_adaptor<vd_iterator<VertexIterator>,
                                  VertexIterator> base_type;
public:
  typedef typename VertexIterator::vertex_descriptor value_type;
  vd_iterator()
  {}

  vd_iterator(VertexIterator iterator)
      : base_type(iterator)
  {}

  value_type operator*() const
  {
    return this->base_reference()->descriptor();
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Vertex data iterator to iterate over the vertex properties.
/// Dereferencing provides the vertex property.
/// @ingroup graphBaseUtil
//////////////////////////////////////////////////////////////////////
template <typename VertexIterator>
class vdata_iterator
  : public boost::iterator_adaptor<vdata_iterator<VertexIterator>,
                                   VertexIterator>
{
private:
  typedef boost::iterator_adaptor<vdata_iterator<VertexIterator>,
                                  VertexIterator> base_type;
public:
  typedef typename VertexIterator::property_type value_type;
  vdata_iterator()
  {}

  vdata_iterator(VertexIterator iterator)
      : base_type(iterator)
  {}

  value_type& operator*()
  {
    return this->base_reference()->property();
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Const vertex data iterator to iterate over the vertex properties.
/// Dereferencing provides the vertex property.
/// @ingroup graphBaseUtil
//////////////////////////////////////////////////////////////////////
template <typename VertexIterator>
class const_vdata_iterator
  : public boost::iterator_adaptor<
             const_vdata_iterator<VertexIterator>, VertexIterator>
{
private:
  typedef boost::iterator_adaptor<const_vdata_iterator<VertexIterator>,
      VertexIterator> base_type;
public:
  typedef typename VertexIterator::property_type value_type;
  const_vdata_iterator()
  { }

  const_vdata_iterator(VertexIterator iterator)
      : base_type(iterator)
  { }

  value_type const& operator*() const
  {
    return this->base_reference()->property();
  }
};

} //end namespace sequential
} //end namespace stapl

#endif
