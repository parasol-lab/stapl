/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_TYPE_TRAITS_IS_CONTAINER_HPP
#define STAPL_CONTAINERS_TYPE_TRAITS_IS_CONTAINER_HPP

#include <boost/mpl/has_xxx.hpp>

namespace stapl {

BOOST_MPL_HAS_XXX_TRAIT_DEF(distribution_type)

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a given type is a STAPL container.
/// The criteria for modeling a container is based on the existence
/// of a nested distribution_type in T.
///
/// @tparam T The type in question
//////////////////////////////////////////////////////////////////////
template<typename T>
struct is_container
 : public has_distribution_type<T>
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a given type is a STAPL container.
/// The criteria for modeling a container is based on the existence
/// of a nested distribution_type in T.
///
/// This metafunction is used in @ref mix_view to determine when the element
/// type of a container is a nested container.
///
/// @tparam T The type in question
//////////////////////////////////////////////////////////////////////
template<typename T>
struct is_nested_container
 : public has_distribution_type<T>
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a given type is a STAPL container.
/// This is a specialization used for map elements. The criteria for
/// modeling a container is based on the existence of a nested
/// distribution_type in T.
///
/// This metafunction is used in @ref mix_view to determine when the element
/// type of a container is a nested container.
///
/// @tparam T The type in question
//////////////////////////////////////////////////////////////////////
template<typename T, typename U>
struct is_nested_container<std::pair<T, U>>
 : public has_distribution_type<U>
{ };

} // namespace stapl

#endif
