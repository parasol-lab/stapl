/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GENERATOR_IDENTITY_HPP
#define STAPL_CONTAINERS_GENERATOR_IDENTITY_HPP

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Generator container whose value for the element at an index
/// is the index itself.
///
/// That is, if we have an identity_container ic, then ic[i] == i must
/// be true.
///
/// @tparam T Type of the index
/// @todo Add size() and domain() methods
////////////////////////////////////////////////////////////////////////
template<typename T>
struct identity_container
{
  typedef T                   value_type;
  typedef value_type          reference;
  typedef value_type          const_reference;
  typedef value_type          index_type;
  typedef index_type          gid_type;

  //////////////////////////////////////////////////////////////////////
  /// @copydoc get_element
  ////////////////////////////////////////////////////////////////////////
  reference operator[](index_type const& idx) const
  { return idx; }

  //////////////////////////////////////////////////////////////////////
  /// @brief Retrieve the value associated with a particular index (i.e.,
  /// return the index.)
  ///
  /// @param idx The index of the element to retrieve
  /// @return Copy of the index.
  ////////////////////////////////////////////////////////////////////////
  reference get_element(index_type const& idx) const
  { return idx; }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the version of the container.
  /// @return Version number of 0
  //////////////////////////////////////////////////////////////////////
  size_t version(void) const
  {
    return 0;
  }

};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization of @ref container_traits for @ref identity_container
///
/// @tparam T Type of value returned by identity_container access methods
//////////////////////////////////////////////////////////////////////
template <typename T>
struct container_traits<identity_container<T>>
{
private:
  typedef identity_container<T> Container;
public:
  typedef typename Container::gid_type        gid_type;
  typedef typename Container::value_type      value_type;
  typedef typename Container::reference       reference;
  typedef typename Container::const_reference const_reference;
};
} // stapl namespace

#endif /* STAPL_CONTAINERS_GENERATOR_IDENTITY_HPP */
