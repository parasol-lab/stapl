/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_STRIP_FAST_VIEW_HPP
#define STAPL_VIEWS_STRIP_FAST_VIEW_HPP

namespace stapl {
namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Computes the type of the view underlying an @ref nfast_view
/// instance.
///
/// Used in the implementation of @ref unique_copy to create a view over
/// elements to be copied using the array_view::set_elements method
//////////////////////////////////////////////////////////////////////
template<typename T>
struct strip_fast_view
{
  using type = T;
};

template<typename View>
struct strip_fast_view<nfast_view<View>>
{
  using type = typename cast_container_view<
                 typename View::base_type, typename View::component_type
               >::type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Computes the type of the view underlying a @ref mix_view
/// instance.
///
/// Used in the implementation of @ref unique_copy to create a view over
/// elements to be copied using the array_view::set_elements method
//////////////////////////////////////////////////////////////////////
template<typename T>
struct strip_mix_view
{
  using type = T;
};

template<typename View, typename Info, typename CID>
struct strip_mix_view<mix_view<View, Info, CID>>
{
  using type = View;
};

//////////////////////////////////////////////////////////////////////
/// @brief Computes the type of the view underlying an @ref repeat_view
/// instance.
///
/// Used in the implementation of @ref unique_copy to create a view over
/// elements to be copied using the array_view::set_elements method
//////////////////////////////////////////////////////////////////////
template<typename T>
struct strip_repeat_view
{
  using type = T;
};

template<typename T, int N, bool IsReadOnly, bool IsView>
struct strip_repeat_view<
         repeat_view<view_impl::repeat_container<T, N, IsReadOnly, IsView>>>
{
  using type = T;
};

} // namespace detail
} // namespace stapl

#endif // STAPL_VIEWS_STRIP_FAST_VIEW_HPP
