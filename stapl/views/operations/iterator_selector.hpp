/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_ITERATOR_SELECTOR_HPP
#define STAPL_VIEWS_ITERATOR_SELECTOR_HPP

#include <stapl/views/type_traits/has_iterator.hpp>
#include <stapl/views/type_traits/is_domain_sparse.hpp>
#include <stapl/views/type_traits/is_identity.hpp>
#include <stapl/views/type_traits/is_view.hpp>

#include <stapl/views/operations/view_iterator.hpp>
#include <stapl/views/operations/const_view_iterator.hpp>

#include <boost/mpl/eval_if.hpp>
#include <boost/mpl/identity.hpp>
#include <boost/mpl/logical.hpp>

namespace stapl {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Helper to return the iterator type provided for the
///        @p View's container.
//////////////////////////////////////////////////////////////////////
template <typename View>
struct get_iterator
{
  typedef typename view_traits<View>::container::iterator type;
};

//////////////////////////////////////////////////////////////////////
/// @brief Helper to return the const_iterator type provided for the
///        @p View's container.
//////////////////////////////////////////////////////////////////////
template <typename View>
struct get_const_iterator
{
  typedef typename view_traits<View>::container::const_iterator type;
};

//////////////////////////////////////////////////////////////////////
/// @brief Helper function to determine the type of iterator to use
///        based on the View type and its value type T
//////////////////////////////////////////////////////////////////////
template <typename View,
          typename T,
          typename Category = std::forward_iterator_tag>
struct iterator_selector
{
  typedef typename boost::mpl::eval_if<
    /* if */ boost::mpl::and_<
               boost::mpl::not_<
                 is_domain_sparse<typename view_traits<View>::domain_type>>,
               is_identity<typename view_traits<View>::map_function>,
               has_iterator<typename view_traits<View>::container>>,
    /*then*/ get_iterator<View>,  // uses the container's iterator type
    /*else*/ boost::mpl::eval_if_c<
               /* if */ is_view<typename view_traits<View>::value_type>::value,
               /*then*/ boost::mpl::identity<detail::view_iterator<View>>,
               /*else*/ boost::mpl::identity<
                          index_iterator<View, Category>>
               > >::type              type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper function to determine the type of iterator to use
///        based on the View type and its value type T
//////////////////////////////////////////////////////////////////////
template <typename View,
          typename T,
          typename Category = std::forward_iterator_tag>
struct const_iterator_selector
{
  typedef typename boost::mpl::eval_if<
    /* if */ boost::mpl::and_<
               boost::mpl::not_<
                 is_domain_sparse<typename view_traits<View>::domain_type> >,
               is_identity<typename view_traits<View>::map_function>,
               has_iterator<typename view_traits<View>::container> >,
    /*then*/ get_iterator<View>,  // uses the container's iterator type
    /*else*/ boost::mpl::eval_if_c<
               /* if */ is_view<typename view_traits<View>::value_type>::value,
               /*then*/ boost::mpl::identity<
                          detail::const_view_iterator<View>>,
               /*else*/ boost::mpl::identity<
                          const_index_iterator<View, Category>>
               > >::type              type;
};

} // namespace detail

} // namespace stapl

#endif // STAPL_VIEWS_ITERATOR_SELECTOR_HPP
