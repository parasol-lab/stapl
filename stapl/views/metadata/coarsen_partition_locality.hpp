/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

// Coarsen partition based on locality information

#ifndef STAPL_VIEWS_METADATA_COARSEN_PARTITION_LOCALITY_HPP
#define STAPL_VIEWS_METADATA_COARSEN_PARTITION_LOCALITY_HPP

#include <stapl/views/metadata/extraction/extract_metadata.hpp>
#include <stapl/views/metadata/coarsen_view.hpp>

namespace stapl {

namespace coarsen_partition_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Helper functor used to coarsen metadata based on locality
///        for the given @c view.
///
/// @todo Function operator should return a shared_ptr.
//////////////////////////////////////////////////////////////////////
template<typename View>
class coarsen_partition
{
  using extractor_type = metadata::extract_metadata<View>;

public:
  using return_type = typename extractor_type::return_type;

  static return_type apply(View* view)
  {
    return extractor_type()(view);
  }
};

} // namespace coarsen_partition_impl

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_COARSEN_PARTITION_LOCALITY_HPP
