/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_CONTAINER_BY_DEMAND_HPP
#define STAPL_VIEWS_METADATA_CONTAINER_BY_DEMAND_HPP

#include <stapl/runtime.hpp>
#include <stapl/views/metadata/metadata_entry.hpp>
#include <stapl/domains/interval.hpp>
#include <stapl/domains/domain_interval.hpp>
#include <stapl/domains/indexed.hpp>
#include <boost/iterator/transform_iterator.hpp>

namespace stapl {

namespace metadata {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Default method for converting between different domains.
///
/// Constructs the target domain by iterating through the source domain
/// and adding one element after another.
//////////////////////////////////////////////////////////////////////
template <typename TargetDomain, typename SourceDomain>
TargetDomain construct_domain_impl(SourceDomain const& dom)
{
  TargetDomain ret_val;
  typename SourceDomain::index_type i      = dom.first();
  typename SourceDomain::index_type i_last = dom.last();

  for (; i != i_last; i = dom.advance(i, 1))
  {
    ret_val += i;
  }

  ret_val += dom.last();
  return ret_val;
}

//////////////////////////////////////////////////////////////////////
/// @brief Functor to convert between different domain types.
///
/// Used in projected_container to construct the view metadata
/// from the container metadata directly.
//////////////////////////////////////////////////////////////////////
template <typename TargetDomain>
struct construct_domain
{
  TargetDomain operator()(TargetDomain const& dom)
  {
    return dom;
  }

  template <typename SourceDomain>
  TargetDomain operator()(SourceDomain const& dom)
  {
    return construct_domain_impl<TargetDomain>(dom);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Functor to convert from a given domain to @ref domset1D domain.
///
/// Used in projected_container to construct the view metadata
/// from the container metadata directly.
//////////////////////////////////////////////////////////////////////
template <typename T>
struct construct_domain<domset1D<T>>
{
  using TargetDomain = domset1D<T>;

  /// Source domain is the same as the target domain - no conversion needed.
  TargetDomain operator()(TargetDomain const& dom)
  {
    return dom;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Convert from an @ref indexed_domain.
  ///
  /// Creates a @ref domset1D domain with just one interval equal to the
  /// @p indexed_domain.
  //////////////////////////////////////////////////////////////////////
  template <typename TT>
  TargetDomain operator()(indexed_domain<TT> const& dom)
  {
    return { dom.first(), dom.last(), dom.is_same_container_domain() };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Convert from a @ref domainset1D domain.
  ///
  /// Uses the default conversion method for two general domains if the
  /// @p domainset1D domain is distributed, otherwise extracts the
  /// @ref domset1D domain wrapped by the @p domainset1D.
  //////////////////////////////////////////////////////////////////////
  template <typename Dist>
  TargetDomain operator()(domainset1D<Dist> const& dom)
  {
    if (dom.get_distribution())
      return construct_domain_impl<TargetDomain>(dom);

    return dom.get_sparse_domain();
  }

  template <typename SourceDomain>
  TargetDomain operator()(SourceDomain const& dom)
  {
    return construct_domain_impl<TargetDomain>(dom);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Functor to convert from a given domain to @ref domainset1D
///   domain.
///
/// Used in projected_container to construct the view metadata
/// from the container metadata directly.
//////////////////////////////////////////////////////////////////////
template <typename T>
struct construct_domain<domainset1D<T>>
{
  using TargetDomain = domainset1D<T>;

  /// Source domain is the same as the target domain - no conversion needed.
  TargetDomain operator()(TargetDomain const& dom)
  {
    return dom;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Convert from an @ref indexed_domain.
  ///
  /// Wraps a @ref domset1D domain with just one interval equal to the
  /// @p indexed_domain.
  //////////////////////////////////////////////////////////////////////
  template <typename TT>
  TargetDomain operator()(indexed_domain<TT> const& dom)
  {
    return { dom.first(), dom.last(), dom.is_same_container_domain() };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Convert from a @ref domset1D domain.
  //////////////////////////////////////////////////////////////////////
  template <typename Dist>
  TargetDomain operator()(domset1D<Dist> const& dom)
  {
    return { dom };
  }

  template <typename SourceDomain>
  TargetDomain operator()(SourceDomain const& dom)
  {
    return construct_domain_impl<TargetDomain>(dom);
  }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Functor used to compute the view's domain metadata from the
///        container's domain.
//////////////////////////////////////////////////////////////////////
template<typename View, typename MDContainer>
struct metadata_ctor_func
{
  typedef typename std::remove_pointer<
    typename MDContainer::second_type>::type md_cont_t;
  typedef typename View::domain_type                      domain_t;
  typedef metadata_entry<
    domain_t,
    typename md_cont_t::value_type::component_type,
    typename md_cont_t::value_type::cid_type
  >                                                       value_type;
  typedef value_type result_type;

  template <typename T>
  value_type operator()(T const& pmd) const
  {
    // copied to remove the proxy type of T
    typename md_cont_t::value_type md = pmd;

    return value_type(
      md.id(), detail::construct_domain<domain_t>()(md.domain()),
      md.component(),
      md.location_qualifier(), md.affinity(), md.handle(), md.location()
    );
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Wrapper around a metadata container to create metadata
///        information on demand, by projecting the container's domain
///        into the view's domain.
///
/// @param View Type of view from which the metadata is created.
/// @tparam MDContainer Type of the metadata container used to
///         represent the metadata information
//////////////////////////////////////////////////////////////////////
template<typename View, typename MDContainer>
struct projected_container
{
  typedef typename std::remove_pointer<typename MDContainer::second_type>::type
    lower_md_cont_type;
  typedef typename lower_md_cont_type::domain_type            domain_type;
  typedef typename lower_md_cont_type::dimensions_type        dimensions_type;
  typedef typename lower_md_cont_type::index_type             index_type;

  typedef typename View::domain_type                          domain_t;

  typedef metadata_entry<domain_t,
    typename lower_md_cont_type::value_type::component_type,
    typename lower_md_cont_type::value_type::cid_type>        value_type;

  typedef value_type                                          reference;

  View                m_view;
  lower_md_cont_type* m_md;
  size_t              m_version;

  typedef metadata_ctor_func<View, MDContainer> transform_t;

  void define_type(typer& t)
  {
    t.member(m_view);
    t.member(m_md);
    t.member(m_version);
  }


  //////////////////////////////////////////////////////////////////////
  /// @brief Constructs a projected_container wrapper over the
  ///        @p c view using the given metadata container @p md.
  //////////////////////////////////////////////////////////////////////
  projected_container(View* view, lower_md_cont_type* md)
    : m_view(*view),
      m_md(md),
      m_version(view->version())
  {
  }

  ~projected_container()
  {
    delete m_md;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the current version number
  //////////////////////////////////////////////////////////////////////
  size_t version(void) const
  {
    return m_version;
  }

  reference operator[](index_type index)
  {
    typename lower_md_cont_type::value_type md_elem=(*m_md)[index];

    return transform_t()(md_elem);
  }

  size_t size() const
  {
   return m_md->size();
  }

  dimensions_type dimensions() const
  {
    return m_md->dimensions();
  }

  domain_type domain() const
  {
    return m_md->domain();
  }

  //////////////////////////////////////////////////////////////////////
  /// @copydoc growable_container::get_local_vid(size_t)
  //////////////////////////////////////////////////////////////////////
  index_type get_local_vid(index_type index) const
  {
    return m_md->get_local_vid(index);
  }

  //////////////////////////////////////////////////////////////////////
  /// @copydoc growable_container::push_back_here(MD const&)
  ///
  /// This class does not support @c push_back_here.
  /// @todo Add a stapl_assert to ensure this method is not called.
  //////////////////////////////////////////////////////////////////////
  template <typename T>
  void push_back_here(T const&)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @copydoc growable_container::local_size
  //////////////////////////////////////////////////////////////////////
  size_t local_size() const
  {
    return m_md->local_size();
  }

  dimensions_type local_dimensions() const
  {
    return m_md->local_dimensions();
  }

  //////////////////////////////////////////////////////////////////////
  /// @copydoc growable_container::get_location_element(size_t)
  //////////////////////////////////////////////////////////////////////
  location_type get_location_element(index_type index) const
  {
    return m_md->get_location_element(index);
  }

  //////////////////////////////////////////////////////////////////////
  /// @copydoc flat_container::update
  /// This class does not support @c push_back_here.
  /// @todo Add a stapl_assert to ensure this method is not called or
  ///       remove it.
  //////////////////////////////////////////////////////////////////////
  void update()
  { }
};

} // namespace metadata

template<typename View, typename MDContainer>
struct metadata_traits<metadata::projected_container<View, MDContainer>>
{
  typedef typename std::remove_pointer<typename MDContainer::second_type>::type
    md_cont_type;
  typedef typename metadata_traits<md_cont_type>::is_isomorphic is_isomorphic;
};

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_CONTAINER_BY_DEMAND_HPP
