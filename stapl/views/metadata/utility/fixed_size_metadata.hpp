
/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_VIEWS_METADATA_UTILITY_FIXED_SIZE_METADATA_HPP
#define STAPL_VIEWS_METADATA_UTILITY_FIXED_SIZE_METADATA_HPP

#include <stapl/containers/array/static_array_fwd.hpp>
#include <stapl/domains/indexed_fwd.hpp>
#include <stapl/containers/distribution/is_distribution_view_fwd.hpp>

namespace stapl {

template<typename Dom, bool Integral>
struct balanced_partition;

template<typename CID>
struct mapper;

namespace metadata {

template<typename D>
struct container_wrapper;


template<typename Partition, typename Mapper>
struct check_balanced_distribution
  : public std::false_type
{ };

template<>
struct
check_balanced_distribution<balanced_partition<indexed_domain<size_t>>,
                            mapper<size_t>>
  : public std::true_type
{ };


template<typename Container>
bool is_balanced_distribution(Container*)
{
  return std::is_base_of<static_array<typename Container::value_type>,
                         Container>::value;
}


template<typename Distribution>
bool is_balanced_distribution(container_wrapper<Distribution>*)
{
  return check_balanced_distribution<
           typename Distribution::partition_type,
           typename Distribution::mapper_type>::value;
}

template<typename View, typename Part>
bool is_fixed_size_md(View const* view, Part const* part)
{
  // Disable static metadata optimization for arbitrary distributions
  if (!is_arbitrary_view_based<typename
        View::view_container_type::distribution_type::partition_type>()(view))
  {
    auto&& view_dom  = view->domain();
    size_t cont_size = view->container().size();
    size_t delta     = cont_size - view_dom.size();

    return is_balanced_distribution(part) &&
           delta < cont_size / view->get_num_locations();
  }
  // else
  return false;
}

} // namespace metadata

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_UTILITY_FIXED_SIZE_METADATA_HPP
