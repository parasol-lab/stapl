/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_PROJECTION_SEGMENTED_HPP
#define STAPL_VIEWS_METADATA_PROJECTION_SEGMENTED_HPP

#include <stapl/runtime.hpp>
#include <stapl/views/metadata/metadata_entry.hpp>
#include <stapl/domains/partitioned_domain.hpp>
#include <stapl/domains/intersect.hpp>
#include <stapl/views/metadata/locality_dist_metadata.hpp>

#include <stapl/views/metadata/container_fwd.hpp>

#include <stapl/containers/type_traits/dimension_traits.hpp>

namespace stapl {

namespace metadata {

//////////////////////////////////////////////////////////////////////
/// @brief Helper functor used to project the domains in the given
///        locality metadata (@c P) to the domain of the given @c
///        View. The result is projected metadata locality
///        information.
///
/// This helper functor is invoked when the given @c View is a
/// partitioned view.
///
/// @todo operator should return a shared_ptr.
//////////////////////////////////////////////////////////////////////
template <typename View, typename P>
struct segmented_metadata_projection
{
  typedef typename std::remove_pointer<typename P::second_type>::type
    lower_md_cont_t;

  // Adjust 1D domains
  template <typename V,typename MD,typename Dim>
  struct adjust_metadata_domains_helper
  {
    typedef typename V::domain_type                      domain_type;
    typedef typename V::map_func_type                    map_func_type;

    typedef typename MD::value_type                      value_type;
    typedef typename value_type::component_type          component_type;
    typedef metadata_entry<domain_type,component_type,
              typename MD::value_type::cid_type >        dom_info_type;
    typedef metadata::growable_container<dom_info_type>  return_type;

    return_type*
    operator()(View* view, lower_md_cont_t* cpart) const
    {
      typedef std::vector<std::pair<domain_type,bool> >  vec_doms_t;

      // get the partitioner from the partitioner_container
      auto par = view->container().partition();
      auto mfg = view->container().mapfunc_generator();

      return_type* res = new return_type();

      using domain_t = typename lower_md_cont_t::domain_type;
      domain_t local_domain(cpart->local_dimensions());

      const bool has_skipped_seg = !view->domain().is_same_container_domain();

      // iterate over the local part of the metadata container
      domain_map(local_domain, [&](typename domain_t::index_type const& i) {
        value_type const& entry = (*cpart)[cpart->get_local_vid(i)];

        component_type sc = entry.component();

        vec_doms_t doms = par.contained_in(entry.domain(), mfg);
        for (auto& dj : doms)
        {
          if (has_skipped_seg)
            dj.first = intersect(dj.first, view->domain());

          if (dj.second) {
            res->push_back_here(dom_info_type(
              typename dom_info_type::cid_type(), dj.first, sc,
              entry.location_qualifier(), entry.affinity(),
              entry.handle(), entry.location()
            ));
          }
          else {
            res->push_back_here(dom_info_type(
              typename dom_info_type::cid_type(), dj.first, NULL,
              entry.location_qualifier(), entry.affinity(),
              entry.handle(), entry.location()
            ));
          }
        }
      });

      res->update();

      delete cpart;

      return res;
    }

  };

  typedef typename View::domain_type                           domain_type;

  typedef typename dimension_traits<
    typename lower_md_cont_t::value_type::domain_type::gid_type
  >::type                                                      dimension_t;

  typedef typename adjust_metadata_domains_helper<
    View, lower_md_cont_t, dimension_t
  >::return_type                                               md_cont_type;

  typedef std::pair<bool, md_cont_type*>                       return_type;

  return_type operator()(View* view, lower_md_cont_t* part,
                         bool part_is_fixed = true) const
  {
    return std::make_pair(false,
      adjust_metadata_domains_helper<View,lower_md_cont_t,dimension_t>()(
        view, part));
  }
};

} // namespace metadata

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_PROJECTION_SEGMENTED_HPP
