/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_NUMBERED_FLOW_HPP
#define STAPL_SKELETONS_NUMBERED_FLOW_HPP

namespace stapl {
namespace skeletons {
namespace flows {

//////////////////////////////////////////////////////////////////////
/// @brief Numbering the flows allows the user to identify each flow
/// and perform the appropriate operations based on the given number
///
/// @ingroup skeletonsFlows
//////////////////////////////////////////////////////////////////////
template <std::size_t N, typename Flow>
class numbered_flow
  : public Flow
{
public:
  explicit numbered_flow(Flow const& flow)
    : Flow(flow)
  { }

  template <int M>
  explicit numbered_flow(numbered_flow<M, Flow> const& flow)
    : Flow(static_cast<Flow const&>(flow))
  { }


  template <typename Coord>
  std::size_t consumer_count(Coord const& producer_coord) const
  {
    return get_flow().template consumer_count<N>(producer_coord);
  }

private:
  Flow get_flow() const
  {
    return static_cast<Flow const&>(*this);
  }

};

template <std::size_t N, typename Flow>
numbered_flow<N, Flow>
make_numbered_flow(Flow const& flow)
{
  return numbered_flow<N, Flow>(flow);
}

} // namespace flows
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_NUMBERED_FLOW_HPP
