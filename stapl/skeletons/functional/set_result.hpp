/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_FUNCTIONAL_SET_RESULT_HPP
#define STAPL_SKELETONS_FUNCTIONAL_SET_RESULT_HPP

#include <type_traits>
#include <utility>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/utility/skeleton.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/operators/elem.hpp>
#include <stapl/skeletons/param_deps/set_result_pd.hpp>
#include <stapl/skeletons/spans/balanced.hpp>
#include <stapl/skeletons/functional/skeleton_traits.hpp>

namespace stapl {
namespace skeletons {
namespace skeletons_impl {

template <std::size_t Arity, typename Op, typename SkeletonTraits>
struct set_result
  : public decltype(
      skeletons::elem<
        default_type<typename SkeletonTraits::span_type, spans::balanced<1>>>(
        skeletons::set_result_pd<
          Arity,
          default_type<typename SkeletonTraits::span_type, spans::balanced<1>>,
          SkeletonTraits::set_result>(
          std::declval<Op>())))
{
  using skeleton_tag_type = tags::set_result;
  using op_type     = Op;
  static constexpr bool setting_result = SkeletonTraits::set_result;
  using span_t =
    default_type<typename SkeletonTraits::span_type, spans::balanced<1>>;
  using base_type = decltype(
                      skeletons::elem<span_t>(
                        skeletons::set_result_pd<
                          Arity, span_t, setting_result>(
                            std::declval<Op>())));

  set_result(Op const& op, SkeletonTraits const& /*traits*/)
    : base_type(skeletons::elem<span_t>(
        skeletons::set_result_pd<Arity, span_t, setting_result>(op)))
  { }

  Op get_op(void) const
  {
    return base_type::nested_skeleton().get_op();
  }

  void define_type(typer& t)
  {
    t.base<base_type>(*this);
  }
};

}

namespace result_of {

template <std::size_t Arity, typename Op, typename SkeletonTraits>
using set_result =
  skeletons_impl::set_result<Arity,
                             typename std::decay<Op>::type,
                             typename std::decay<SkeletonTraits>::type>;

} // namespace result_of


//////////////////////////////////////////////////////////////////////
/// @brief the skeleton which is used to specify which tasks should
///        put their results on the environment result container according
///        to the span which is passed in the @c traits
///
/// @tparam Arity the number of inputs are passed to the skeleton
/// @param op     the workfunction to be used in each @c set_result_pd
///               parametric dependency is spanned or not
/// @param traits the @c skeleton_traits to be used
///
/// @see sink_value
/// @see skeleton_traits
///
/// @ingroup skeletonsFunctional
//////////////////////////////////////////////////////////////////////
template <std::size_t Arity = 1,
          typename Op,
          typename SkeletonTraits = skeletons_impl::default_skeleton_traits>
result_of::set_result<Arity, Op, SkeletonTraits>
set_result(Op&& op, SkeletonTraits&& traits = SkeletonTraits())
{
  return result_of::set_result<Arity, Op, SkeletonTraits>(
    std::forward<Op>(op), std::forward<SkeletonTraits>(traits));
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_FUNCTIONAL_SET_RESULT_HPP
