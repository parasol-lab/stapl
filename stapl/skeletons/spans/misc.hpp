/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_SPANS_MISC_HPP
#define STAPL_SKELETONS_SPANS_MISC_HPP

#include <stapl/views/counting_view.hpp>
#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/tuple/front.hpp>
#include <stapl/skeletons/spans/balanced.hpp>
#include <stapl/skeletons/spans/per_location.hpp>
#include <stapl/skeletons/spans/reduce_to_pow_two.hpp>
#include <stapl/skeletons/spans/nearest_pow_two.hpp>

namespace stapl {
namespace skeletons {
namespace spans {

//////////////////////////////////////////////////////////////////////
/// @brief A span that will just have one element on the location it
/// was created on.
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
class only_once
  : public balanced<1>
{
public:
  using size_type      = std::size_t;
  using dimension_type = typename balanced<>::dimension_type;

  template <typename Spawner, typename... Views>
  void set_size(Spawner const& spawner, Views const&... views)
  {
    using dom_t = skeletons::domain_type<indexed_domain<std::size_t>>;
    balanced::set_size(
      spawner, stapl::counting_view<int>(1));
  }
};

} // namespace spans
} // namespace skeletons
} // namepsace stapl

#endif // STAPL_SKELETONS_SPANS_MISC_HPP
