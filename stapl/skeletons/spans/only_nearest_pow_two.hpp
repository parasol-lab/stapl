/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_SPANS_ONLY_NEAREST_POWER_TWO_HPP
#define STAPL_SKELETONS_SPANS_ONLY_NEAREST_POWER_TWO_HPP

namespace stapl {
namespace skeletons {
namespace spans {

//////////////////////////////////////////////////////////////////////
/// @brief Spans only the nearest power two element to the domain size.
///        It's only use currently is the @c pre_broadcast skeleton
///
/// @see pre_broadcast
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
class only_nearest_pow_two
  : public spans::balanced<1>
{
public:
  using size_type      = std::size_t;
  using dimension_type = typename balanced<>::dimension_type;

  template <typename Coord>
  bool should_spawn(Coord const& skeleton_size, Coord const& coord) const
  {
    std::size_t i = tuple_ops::front(coord);
    std::size_t n = tuple_ops::front(skeleton_size);
    std::size_t nearest_pow2 = 1;
    while (n != 1) {
      nearest_pow2 <<= 1;
      n >>= 1;
    }
    return i == nearest_pow2 - 1;
  }
};

} // namespace spans
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_SPANS_ONLY_NEAREST_POWER_TWO_hpp
