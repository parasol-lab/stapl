/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_MAP_HPP
#define STAPL_SKELETONS_MAP_HPP

#include <stapl/skeletons/functional/zip.hpp>
#include <stapl/skeletons/transformations/coarse/zip.hpp>
#include <stapl/skeletons/executors/execute.hpp>
#include <stapl/views/metadata/coarseners/null.hpp>
#include <stapl/views/metadata/coarseners/default.hpp>
#include <stapl/views/metadata/coarseners/multiview.hpp>

namespace stapl {

namespace map_func_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Reflects the correct span for a map instantiated with a
///        view set.
//////////////////////////////////////////////////////////////////////
template<typename... Views>
struct select_span
{
  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the number of dimensions of the first view
  //////////////////////////////////////////////////////////////////////
  template<typename T, typename... Ts>
  static constexpr std::size_t first(T&& t, Ts&&...)
  {
    return t;
  }

  using type = skeletons::spans::blocked<
    first(dimension_traits<typename std::decay<Views>::type>::type::value...)
  >;
};

//////////////////////////////////////////////////////////////////////
/// @brief Construct and execute a PARAGRAPH that will perform a
/// fine-grained map operation, applying the fine-grain work function
/// to the elements of the views provided.
///
/// @param map_op Fine-grain map work function.
/// @param view   One or more views to process with the map work function.
///
/// @ingroup skeletonsExecutableInternal
//////////////////////////////////////////////////////////////////////
template<typename MapOp, typename ...V>
inline void
map_func(skeletons::tags::with_coarsened_wf, MapOp const& map_op,
         V&&... views)
{
  using span_t = typename select_span<V...>::type;

  skeletons::execute(
    skeletons::execution_params(default_coarsener()),
    skeletons::zip<sizeof...(V)>(
      map_op, skeletons::skeleton_traits<span_t>()),
    std::forward<V>(views)...);
}

//////////////////////////////////////////////////////////////////////
/// @brief Construct and execute a PARAGRAPH that will perform a
/// fine-grained map operation, applying the fine-grain work function
/// to the element of the views provided.
///
/// @param map_op Fine-grain map work function.
/// @param views  One or more views to process with the map work function.
///
/// @ingroup skeletonsExecutableInternal
//////////////////////////////////////////////////////////////////////
template<typename MapOp, typename ...V>
inline void
map_func(skeletons::tags::no_coarsening, MapOp const& map_op, V&&... views)
{
  using span_t = typename select_span<V...>::type;

  skeletons::execute(
    skeletons::default_execution_params(),
    skeletons::zip<sizeof...(V)>(
      map_op, skeletons::skeleton_traits<span_t>()),

    std::forward<V>(views)...);
}

}

//////////////////////////////////////////////////////////////////////
/// @brief Construct and execute a PARAGRAPH that will perform a map operation,
/// applying the fine-grain work function to the element of the views
/// provided.
///
/// @param map_op Fine-grain map work function.
/// @param views  One or more views to process with the map work function.
///
/// @ingroup skeletonsExecutable
//////////////////////////////////////////////////////////////////////
template<typename MapOp, typename ...V>
inline void
map_func(MapOp const& map_op, V&&... views)
{
  using span_t = typename map_func_impl::select_span<V...>::type;

  skeletons::execute(
    skeletons::execution_params(default_coarsener()),
    skeletons::coarse(skeletons::zip<sizeof...(V)>(
      map_op, skeletons::skeleton_traits<span_t>())),
    std::forward<V>(views)...);
}


template<typename Tag, typename MapOp, typename ...V>
inline void
map_func(MapOp const& map_op, V&&... views)
{
  map_func_impl::map_func(Tag(), map_op, std::forward<V>(views)...);
}

} // namespace stapl

#endif // STAPL_SKELETONS_MAP_HPP
