/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_FUNCTIONAL_FARM_HPP
#define STAPL_SKELETONS_FUNCTIONAL_FARM_HPP

#include <type_traits>
#include <utility>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/utility/skeleton.hpp>
#include <stapl/skeletons/operators/elem.hpp>
#include <stapl/skeletons/param_deps/farm_pd.hpp>
#include <stapl/skeletons/spans/misc.hpp>

namespace stapl {
namespace skeletons {
namespace result_of {

template <typename Generator, typename Span>
using farm = result_of::elem<
               stapl::default_type<Span, spans::only_once>,
               stapl::use_default,
               skeletons_impl::farm_pd<
                 typename std::decay<Generator>::type>>;

} // namespace result_of


//////////////////////////////////////////////////////////////////////
/// @brief Given a generator, creates a farm skeleton. The generator
/// can then generate the subsequent tasks of the program.
///
/// An example of this type of farm can be used in the implementation
/// of BFS (Breadth First Search), in which the initial seed define
/// the starting point of the program.
///
/// @tparam Span      the iteration space for the elements in this
///                   skeleton. The default value is only once. Other
///                   spans can be used. In such cases the redundant
///                   additions of elements to farm should be handled
///                   by the generator.
/// @param  generator the generator to be used to create the initial
///                   seeds for this farm. The generator receives
///                   user provides the farm as the first argument to
///                   the generator.
///
/// @see spans::only_once
///
/// @ingroup skeletonsFunctional
//////////////////////////////////////////////////////////////////////
template <typename Span = spans::only_once,
          typename Generator>
result_of::farm<Generator, Span>
farm(Generator&& generator)
{
  return result_of::farm<Generator, Span>(
           farm_pd(std::forward<Generator>(generator)));
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_FUNCTIONAL_FARM_HPP
