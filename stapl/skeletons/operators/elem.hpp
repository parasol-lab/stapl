/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_OPERATORS_ELEM_HPP
#define STAPL_SKELETONS_OPERATORS_ELEM_HPP

#include <type_traits>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/flows/elem_flow.hpp>
#include <stapl/skeletons/spans/balanced.hpp>
#include <stapl/skeletons/spans/blocked.hpp>
#include "elem_impl.hpp"

namespace stapl {
namespace skeletons {
namespace result_of {

template <typename Span, typename PD,
          bool nested = is_nested_skeleton<typename PD::op_type>::value>
struct span_default_type;

template <typename Span, typename PD>
struct span_default_type<Span, PD, false>
{
  using type =
    stapl::default_type<Span, spans::balanced<>>;
};

template <typename Span, typename PD>
struct span_default_type<Span, PD, true>
{
  using skeleton_t =
    skeletons_impl::elem<PD, stapl::default_type<Span, spans::balanced<>>,
                         flows::elem_f::doall>;

  static constexpr size_t num_level = Traverse<skeleton_t>::type::value;
  static constexpr size_t dims      = Traverse<skeleton_t>::dims::value;
  using type = spans::nest_blocked<Span::dims_num::value, dims,
                                   typename PD::op_type, num_level>;
};

template <typename Span, typename Flows, typename PD>
using elem = skeletons_impl::elem<
  typename std::decay<PD>::type,
  typename span_default_type<typename std::decay<Span>::type,
                             typename std::decay<PD>::type>::type,
  stapl::default_type<Flows, flows::elem_f::doall>>;

} // namespace result_of

//////////////////////////////////////////////////////////////////////
/// @brief An elementary is an operator that converts parametric
/// dependencies to skeletons. It wraps a parametric dependency with
/// @c Flows and @c Span information.
///
/// @tparam Span  the iteration space for elements in this elementary.
///               The default span is @c balanced
/// @tparam Flows the flow to be used for the elementary skeleton.
///               The default flow is a @c forked flow
/// @param  pd    the parametric dependency to be wrapped
/// @return an elementary skeleton
///
/// @see flows::elem_f::forked
/// @see spans::balanced
///
/// @ingroup skeletonsOperators
//////////////////////////////////////////////////////////////////////
template <typename Span  = stapl::use_default,
          typename Flows = stapl::use_default,
          typename PD>
result_of::elem<Span, Flows, PD>
elem(PD&& pd, Span&& span)
{
  return result_of::elem<Span, Flows, PD>(
           std::forward<PD>(pd), std::forward<Span>(span));
}

//////////////////////////////////////////////////////////////////////
/// @brief  An overload when the span instance is not passed
/// @tparam Span  the iteration space for elements in this elementary.
///               The default span is @c balanced
/// @tparam Flows the flow to be used for the elementary skeleton.
///               The default flow is a @c forked flow
/// @param  pd    the parametric dependency to be wrapped
/// @return an elementary skeleton
///
/// @see flows::elem_f::forked
/// @see spans::balanced
///
/// @ingroup skeletonsOperators
//////////////////////////////////////////////////////////////////////
template <typename Span  = stapl::use_default,
          typename Flows = stapl::use_default,
          typename PD>
result_of::elem<Span, Flows, PD>
elem(PD&& pd)
{
  return result_of::elem<Span, Flows, PD>(std::forward<PD>(pd));
}


} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_OPERATORS_ELEM_HPP
