/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_TRANSFORMATIONS_NEST_ZIP_HPP
#define STAPL_SKELETONS_TRANSFORMATIONS_NEST_ZIP_HPP


#include <stapl/skeletons/transformations/transform.hpp>
#include <stapl/skeletons/transformations/wrapped_skeleton.hpp>
#include <stapl/skeletons/transformations/optimizers/nested.hpp>
#include <stapl/skeletons/utility/mapper_utils.hpp>
#include <stapl/skeletons/functional/zip.hpp>

namespace stapl {
namespace skeletons {
namespace transformations {

template <typename S, typename SkeletonTag, typename TransformTag>
struct transform;


//////////////////////////////////////////////////////////////////////
/// @brief In the default case, a transformation of a skeleton with
/// the @c tags::nest tag should not modify the skeleton. For example,
/// a nested @ref skeletons::map or @ref skeletons::zip can be executed
/// without applying any transformations.
///
/// @see make_paragraph_skeleton_manager
///
/// @ingroup skeletonsTransformationsNest
//////////////////////////////////////////////////////////////////////
template <typename S, typename Tag, int Arity>
struct transform<S, tags::zip<Tag, Arity>, tags::recursive_nest>
{

  using skeleton_tag_t = tags::zip<Tag, Arity>;
  using Dim            = typename S::span_type::nested_dims_num;
  using mappers_t      = mappers<Dim::value, skeleton_tag_t>;

  static auto call(S const& skeleton)
  STAPL_AUTO_RETURN((
    skeletons::zip<Arity>(
      nest(get_op(skeleton.get_op())),
      skeleton_traits<typename S::span_type, S::set_result>(
        get_nested_filter<Dim::value>(skeleton.get_filter(), skeleton.get_op()),
        mappers_t())
    )
  ))
};

} // namespace transformations
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_TRANSFORMATIONS_NEST_ZIP_HPP
