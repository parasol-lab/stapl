/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_TRANSFORMATIONS_COARSE_COMPOSE_HPP
#define STAPL_SKELETONS_TRANSFORMATIONS_COARSE_COMPOSE_HPP

#include <stapl/utility/utility.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/transformations/wrapped_skeleton.hpp>
#include <stapl/skeletons/transformations/transform.hpp>

#include <stapl/skeletons/transformations/coarse/coarse.hpp>
#include <stapl/skeletons/operators/compose.hpp>
#include <stapl/utility/integer_sequence.hpp>


namespace stapl {
namespace skeletons {
namespace transformations {

template <typename S, typename SkeletonTag, typename CoarseTag>
struct transform;


//////////////////////////////////////////////////////////////////////
/// @brief A coarsening transformation on a composed skeleton
/// applies coarsening on each individual skeleton and composes them
/// again using the flow specification of the original fine-grain
/// composition.
///
/// @tparam S            the composed skeleton to be coarsened
/// @tparam Flows        the flows specification used in the original
///                      composition
/// @tparam CoarseTag    a tag to specify the required specialization for
///                      coarsening
/// @tparam ExecutionTag a tag to specify the execution method used for
///                      the coarsened chunks
///
/// @see skeletonsTagsCoarse
/// @see skeletonsTagsExecution
///
/// @ingroup skeletonsTransformationsCoarse
//////////////////////////////////////////////////////////////////////
template<typename... S, typename Flows,
         typename SkeletonTag, typename CoarseTag, typename ExecutionTag>
struct transform<stapl::skeletons::skeletons_impl::compose<
                   stapl::tuple<S...>, Flows>,
                 SkeletonTag,
                 tags::coarse<CoarseTag, ExecutionTag>>
{
private:
  using skeleton_t = stapl::skeletons::skeletons_impl::compose<
                       stapl::tuple<S...>, Flows>;
  using flows_t    = Flows;
  using ct         = CoarseTag;
  using et         = ExecutionTag;

  template <std::size_t... Indices>
  static auto
  apply_coarse(skeleton_t const& c, stapl::index_sequence<Indices...>&&)
  STAPL_AUTO_RETURN((
    stapl::skeletons::compose<flows_t>(
      stapl::skeletons::coarse<ct, et>(
        c.template get_skeleton<Indices>())...)
  ))

public:
  static auto call(skeleton_t const& skeleton)
  STAPL_AUTO_RETURN((
    apply_coarse(skeleton, stapl::make_index_sequence<sizeof...(S)>())
  ))
};


template<typename Skeleton, typename S, typename CoarseTag,
         typename ExecutionTag>
struct transform<Skeleton,
                 tags::sink_value<S>,
                 tags::coarse<CoarseTag, ExecutionTag>>
  : public transform<typename Skeleton::base_type,
                     typename Skeleton::base_type::skeleton_tag_type,
                     tags::coarse<CoarseTag, ExecutionTag>>
{ };


} // namespace transformations
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_TRANSFORMATIONS_COARSE_COMPOSE_HPP
