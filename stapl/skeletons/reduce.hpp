/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_REDUCE_HPP
#define STAPL_SKELETONS_REDUCE_HPP

#include <stapl/skeletons/executors/execute.hpp>
#include <stapl/skeletons/functional/reduce.hpp>
#include <stapl/skeletons/functional/broadcast_to_locs.hpp>
#include <stapl/skeletons/functional/sink_value.hpp>
#include <stapl/skeletons/transformations/coarse/reduce.hpp>
#include <stapl/views/metadata/coarseners/multiview.hpp>
#include <stapl/views/metadata/coarseners/null.hpp>
#include <boost/utility/result_of.hpp>

namespace stapl {

namespace reduce_helpers {

//////////////////////////////////////////////////////////////////////
/// @brief Reduces the value of elements in a given view by applying
/// @c BinaryOp given an algorithm executor and the skeleton to be used.
///
/// @tparam T       result type of the reduction
/// @tparam C       data coarsening method to be used
/// @param skeleton the reduction skeleton to be used
/// @param view     the input view
/// @param binop    the operation used to reduce the values
///
/// @return returns the reduction value on each location
///
/// @todo the extraneous creation of an array with one element per
///       location should be replaced by the direct results from the
///       PARAGRAPH
///
/// @ingroup skeletonsExecutableInternal
//////////////////////////////////////////////////////////////////////
template<typename T, typename C, typename S, typename View>
typename std::decay<View>::type::value_type
reduce(S const& skeleton, View&& view)
{
  using namespace skeletons;

  return skeletons::execute(
           execution_params<typename df_stored_type<T>::type>(C()),
           compose(skeleton, broadcast_to_locs<true>()),
           std::forward<View>(view));
}

//////////////////////////////////////////////////////////////////////
/// @brief Reduces the value of elements in a given view by applying
/// @c BinaryOp.
///
/// @param view  the input view
/// @param binop the operation used to reduce the values
///
/// @return returns the reduction value on each location
///
/// @todo the extraneous creation of an array with one element per
///       location should be replaced by the direct results from the
///       PARAGRAPH
///
/// @ingroup skeletonsExecutableInternal
//////////////////////////////////////////////////////////////////////
template<typename View, typename BinaryOp>
typename std::decay<View>::type::value_type
reduce(stapl::use_default, View&& view, BinaryOp const& binop)
{
  typedef typename
    boost::result_of<
      BinaryOp(typename std::decay<View>::type::reference,
               typename std::decay<View>::type::reference)
    >::type val_t;

  using namespace skeletons;
  return reduce_helpers::reduce<val_t, default_coarsener>(
           skeletons::coarse(skeletons::reduce(binop)),
           std::forward<View>(view));
}

//////////////////////////////////////////////////////////////////////
/// @brief This specialization is used when no coarsening is intended.
///
/// @param view  the input view
/// @param binop the operation used to reduce the values
///
/// @return returns the reduction value on each location
///
/// @ingroup skeletonsExecutableInternal
//////////////////////////////////////////////////////////////////////
template<typename View, typename BinaryOp>
typename std::decay<View>::type::value_type
reduce(skeletons::tags::no_coarsening, View&& view, BinaryOp const& binop)
{
  typedef typename
    boost::result_of<
      BinaryOp(typename std::decay<View>::type::reference,
               typename std::decay<View>::type::reference)
    >::type val_t;

  return reduce_helpers::reduce<val_t, null_coarsener>(
           skeletons::reduce(binop),
           std::forward<View>(view));
}

}

//////////////////////////////////////////////////////////////////////
/// @brief Users can specify which version of the reduce skeleton to
/// be used for reduction algorithm. The possible choices are
/// @li stapl::use_default which uses the default coarsened reduction
///     skeleton.
/// @li no_coarsening In some cases it is desired to use a reduction
///     in its fine-grained format. One can use this tag to avoid
///     the coarsening phase altogether.
///
/// @param view  the input view
/// @param binop the operation used to reduce the values
/// @tparam Tag which reduction to be used
///
/// @return returns the reduction value on each location
///
/// @see algorithm_fwd.hpp for default values.
///
/// @ingroup skeletonsExecutable
//////////////////////////////////////////////////////////////////////
template <typename Tag = stapl::use_default,
          typename View, typename BinaryOp>
typename std::decay<View>::type::value_type
reduce(View&& view, BinaryOp const& binop)
{
  return reduce_helpers::reduce(Tag(), view, binop);
}

} // namespace stapl

#endif // STAPL_SKELETONS_REDUCE_HPP
