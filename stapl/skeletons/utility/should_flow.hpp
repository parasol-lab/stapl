/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_SKELETONS_UTILITY_SHOULD_FLOW_HPP
#define STAPL_SKELETONS_UTILITY_SHOULD_FLOW_HPP

#include <stapl/skeletons/param_deps/wavefront_utils.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/utility/position.hpp>
#include <stapl/domains/indexed.hpp>

namespace stapl {
namespace skeletons {

/////////////////////////////////////////////////////////////////////////
/// @brief The default should flow that flows all the elements to
///        to the consumers
/// @tparam SkeletonTag the skeleton tag of skeleton
/////////////////////////////////////////////////////////////////////////
template <typename SkeletonTag>
class should_flow
{
public:
  template<typename Sk>
  should_flow(Sk&& skeleton)
  { }

  template <typename Dir>
  void set_direction(Dir&& dir)
  { }

  template <typename Dimension>
  void set_dimension(Dimension&& dimension)
  { }

  bool operator()(std::size_t result_id) const
  {
    return true;
  }
};

/////////////////////////////////////////////////////////////////////////
/// @brief Goes recursively into each nested level and uses the
///        corresponding should flow for the skeleton on that level.
///
/// @tparam SkeletonTag the skeleton tag of skeleton
/////////////////////////////////////////////////////////////////////////
template <int Dim>
class recursive_should_flow
{
  using dir_t                = direction;
  using domain_type          = indexed_domain<std::size_t, Dim>;
  using index_type           = typename domain_type::index_type;
  using traversal_t          = typename domain_type::traversal_type;
  using linearizer_t         = nd_linearize<index_type, traversal_t>;

  dir_t       m_dir;
  index_type  m_total_dims;
  std::vector<std::size_t> m_res_ids;

public:

  void set_direction(dir_t dir)
  {
    m_dir = dir;
  }

  void set_dimension(index_type total_dims)
  {
    m_total_dims = total_dims;
  }

  template <size_t level, typename SkeletonTag, typename Sk, typename Op,
            typename LevelDims, typename Dimension, typename Coord>
  void compute_result_ids(std::true_type,
                          Sk&& sk,
                          Op&& op,
                          Dimension&& total_dims,
                          LevelDims&& level_dims,
                          Coord&& first_coord)
  {
    using op_sk_t    = typename std::decay<Op>::type::wrapped_skeleton_type;
    using op_sk_tag  = typename op_sk_t::skeleton_tag_type;
    using inner_op_t = typename op_sk_t::op_type;

    should_flow<SkeletonTag> cur_should_flow(sk);
    cur_should_flow.set_dimension(level_dims[level]);
    cur_should_flow.set_direction(m_dir);

    auto&& dom = domain_type(level_dims[level]);
    auto&& cur = dom.first();
    auto&& sz  = dom.size();
    auto&& task_dims =
      tuple_ops::transform(total_dims,
                           level_dims[level],
                           stapl::divides<std::size_t>());

    for (std::size_t i = 0; i < sz; ++i)
    {
      auto new_cur_coord   =
        tuple_ops::transform(task_dims, cur, stapl::multiplies<std::size_t>());
      auto new_first_coord =
        tuple_ops::transform(first_coord,
                             new_cur_coord,
                             stapl::plus<std::size_t>());

      if (cur_should_flow(i))
      {
        compute_result_ids<level + 1, op_sk_tag>(
          is_nested_skeleton<inner_op_t>(),
          op.get_skeleton(),
          op.get_skeleton().get_op(),
          task_dims,
          std::forward<LevelDims>(level_dims),
          new_first_coord);
      }
      cur = dom.advance(cur, 1);
    }
  }

  template <size_t level, typename SkeletonTag, typename Sk, typename Op,
            typename Dimension, typename LevelDims, typename Coord>
  void compute_result_ids(std::false_type,
                          Sk&& sk,
                          Op&& op,
                          Dimension&& total_dims,
                          LevelDims&& level_dims,
                          Coord&& first_coord)
  {
    should_flow<SkeletonTag> cur_should_flow(sk);
    cur_should_flow.set_dimension(level_dims[level]);
    cur_should_flow.set_direction(m_dir);

    auto&& dom = domain_type(level_dims[level]);
    auto&& cur = dom.first();
    auto&& sz  = dom.size();

    linearizer_t linearizer(m_total_dims);

    for (std::size_t i = 0; i < sz; ++i)
    {
      auto&& new_cur_coord =
        tuple_ops::transform(first_coord, cur, stapl::plus<std::size_t>());
      if (cur_should_flow(i))
      {
        m_res_ids.push_back(linearizer(new_cur_coord));
      }
      cur = dom.advance(cur, 1);
    }

  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(std::true_type, Op&& op, LevelDims&& level_dims)
  {
    using op_sk_t    = typename std::decay<Op>::type::wrapped_skeleton_type;
    using op_sk_tag  = typename op_sk_t::skeleton_tag_type;
    using inner_op_t = typename op_sk_t::op_type;

    m_res_ids.clear();
    compute_result_ids<0, op_sk_tag>(is_nested_skeleton<inner_op_t>(),
                                     op.get_skeleton(),
                                     op.get_skeleton().get_op(),
                                     m_total_dims,
                                     std::forward<LevelDims>(level_dims),
                                     homogeneous_tuple<Dim>(0));
  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(std::false_type, Op&&, LevelDims&&)
  { }

  template <typename Op, typename LevelDims>
  void compute_result_ids(Op&& op, LevelDims&& level_dims)
  {
    compute_result_ids(
      is_nested_skeleton<typename std::decay<Op>::type>(),
      std::forward<Op>(op),
      std::forward<LevelDims>(level_dims));
  }

  std::vector<std::size_t> get_result_ids(void) const
  {
    return m_res_ids;
  }

  bool operator()(std::size_t result_id) const
  {
    return std::end(m_res_ids) !=
           std::find(m_res_ids.begin(), m_res_ids.end(), result_id);
  }

  void define_type(typer& t)
  {
    t.member(m_dir);
    t.member(m_total_dims);
    t.member(m_res_ids);
  }

};

/////////////////////////////////////////////////////////////////////////
/// @brief  Specialization for 1D dimension
/////////////////////////////////////////////////////////////////////////
template <>
class recursive_should_flow<1>
{
  using dir_t                = direction;
  using domain_type          = indexed_domain<std::size_t, 1>;
  using index_type           = typename domain_type::index_type;
  using traversal_t          = typename domain_type::traversal_type;
  using linearizer_t         = nd_linearize<index_type, traversal_t>;

  dir_t       m_dir;
  index_type  m_total_dims;
  std::vector<std::size_t> m_res_ids;

public:

  void set_direction(dir_t dir)
  {
    m_dir = dir;
  }

  void set_dimension(index_type total_dims)
  {
    m_total_dims = total_dims;
  }

  template <size_t level, typename SkeletonTag, typename Sk, typename Op,
            typename LevelDims, typename Dimension, typename Coord>
  void compute_result_ids(std::true_type,
                          Sk&& sk,
                          Op&& op,
                          Dimension&& total_dims,
                          LevelDims&& level_dims,
                          Coord&& first_coord)
  {
    using op_sk_t    = typename std::decay<Op>::type::wrapped_skeleton_type;
    using op_sk_tag  = typename op_sk_t::skeleton_tag_type;
    using inner_op_t = typename op_sk_t::op_type;

    should_flow<SkeletonTag> cur_should_flow(sk);
    cur_should_flow.set_dimension(level_dims[level]);
    cur_should_flow.set_direction(m_dir);

    auto&& dom = domain_type(level_dims[level]);
    auto&& cur = dom.first();
    auto&& sz  = dom.size();
    auto&& task_dims = total_dims/level_dims[level];

    for (std::size_t i = 0; i < sz; ++i)
    {
      auto new_cur_coord   = task_dims * cur;
      auto new_first_coord = first_coord + new_cur_coord;

      if (cur_should_flow(i))
      {
        compute_result_ids<level + 1, op_sk_tag>(
          is_nested_skeleton<inner_op_t>(),
          op.get_skeleton(),
          op.get_skeleton().get_op(),
          task_dims,
          std::forward<LevelDims>(level_dims),
          new_first_coord);
      }
      cur = dom.advance(cur, 1);
    }
  }

  template <size_t level, typename SkeletonTag, typename Sk, typename Op,
            typename Dimension, typename LevelDims, typename Coord>
  void compute_result_ids(std::false_type,
                          Sk&& sk,
                          Op&& op,
                          Dimension&& total_dims,
                          LevelDims&& level_dims,
                          Coord&& first_coord)
  {
    should_flow<SkeletonTag> cur_should_flow(sk);
    cur_should_flow.set_dimension(level_dims[level]);
    cur_should_flow.set_direction(m_dir);

    auto&& dom = domain_type(level_dims[level]);
    auto&& cur = dom.first();
    auto&& sz  = dom.size();

    linearizer_t linearizer(m_total_dims);

    for (std::size_t i = 0; i < sz; ++i)
    {
      auto&& new_cur_coord = first_coord + cur;
      if (cur_should_flow(i))
      {
        m_res_ids.push_back(linearizer(new_cur_coord));
      }
      cur = dom.advance(cur, 1);
    }

  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(std::true_type, Op&& op, LevelDims&& level_dims)
  {
    using op_sk_t    = typename std::decay<Op>::type::wrapped_skeleton_type;
    using op_sk_tag  = typename op_sk_t::skeleton_tag_type;
    using inner_op_t = typename op_sk_t::op_type;

    m_res_ids.clear();
    compute_result_ids<0, op_sk_tag>(is_nested_skeleton<inner_op_t>(),
                                     op.get_skeleton(),
                                     op.get_skeleton().get_op(),
                                     m_total_dims,
                                     std::forward<LevelDims>(level_dims),
                                     0);
  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(std::false_type, Op&&, LevelDims&&)
  { }

  template <typename Op, typename LevelDims>
  void compute_result_ids(Op&& op, LevelDims&& level_dims)
  {
    compute_result_ids(
      is_nested_skeleton<typename std::decay<Op>::type>(),
      std::forward<Op>(op),
      std::forward<LevelDims>(level_dims));
  }

  std::vector<std::size_t> get_result_ids(void) const
  {
    return m_res_ids;
  }

  bool operator()(std::size_t result_id) const
  {
    return std::end(m_res_ids) !=
           std::find(m_res_ids.begin(), m_res_ids.end(), result_id);
  }

  void define_type(typer& t)
  {
    t.member(m_dir);
    t.member(m_total_dims);
    t.member(m_res_ids);
  }

};


} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_UTILITY_SHOULD_FLOW_HPP
