/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_FROM_ARRAY_HPP
#define STAPL_UTILITY_TUPLE_FROM_ARRAY_HPP

#include <stapl/utility/integer_sequence.hpp>
#include <stapl/utility/tuple/tuple_size.hpp>
#include <stapl/utility/tuple/homogeneous_tuple.hpp>

namespace stapl {
namespace tuple_ops {

template<typename Array,
         typename Indices =
           make_index_sequence<stapl::tuple_size<Array>::value>>
struct from_array_impl;

//////////////////////////////////////////////////////////////////////
/// @brief Convert an std::array to a tuple of homogeneous types
//////////////////////////////////////////////////////////////////////
template<typename Array, std::size_t... Indices>
struct from_array_impl<Array, index_sequence<Indices...>>
{
  using type = typename homogeneous_tuple_type<
    stapl::tuple_size<Array>::value, typename Array::value_type
  >::type;

  static type apply(Array const& a)
  {
    return std::make_tuple(a[Indices]...);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Convert an std::array to a tuple of homogeneous types.
///
/// @param a an std::array
//////////////////////////////////////////////////////////////////////
template<typename Array>
typename from_array_impl<Array>::type
from_array(Array const& a)
{
  return from_array_impl<Array>::apply(a);
}

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_FROM_ARRAY_HPP
