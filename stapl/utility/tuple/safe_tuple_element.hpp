/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_SAFE_TUPLE_ELEMENT_HPP
#define STAPL_UTILITY_TUPLE_SAFE_TUPLE_ELEMENT_HPP

#include <stapl/utility/tuple/tuple_element.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction that acts like tuple_element except when the
/// index is -1, in which case it gives a default type instead.
///
/// @tparam I          the int index of the element to get
/// @tparam Tuple      the tuple to index into
//////////////////////////////////////////////////////////////////////
template<int I, class Tuple, class Default>
struct safe_tuple_element
  : stapl::tuple_element<(size_t) I, Tuple>
{ };

template<class Tuple, class Default>
struct safe_tuple_element<-1, Tuple, Default>
{
  using type = Default;
};


} // namespace stapl
#endif // STAPL_UTILITY_TUPLE_SAFE_TUPLE_ELEMENT_HPP
