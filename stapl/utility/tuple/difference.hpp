/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTIILTY_TUPLE_DIFFERENCE_HPP
#define STAPL_UTIILTY_TUPLE_DIFFERENCE_HPP

#include <type_traits>
#include <stapl/utility/tuple/tuple.hpp>

#include <stapl/utility/tuple/tuple_contains.hpp>
#include <stapl/utility/tuple/homogeneous_tuple.hpp>
#include <stapl/utility/tuple/push_back.hpp>

namespace stapl {
namespace tuple_ops {
namespace result_of {
namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Implementation of difference metafunction. Recursive case.
//////////////////////////////////////////////////////////////////////
template<int CurrentIndex, int LastIndex, typename Larger, typename Smaller,
         typename Running, template<typename...> class Check>
struct difference_impl
{
  using current_element = typename tuple_element<CurrentIndex, Larger>::type;
  using next = typename std::conditional<
    not Check<current_element, Smaller>::value,
    typename tuple_ops::result_of::push_back<Running, current_element>::type,
    Running
  >::type;

  using type = typename difference_impl<
    CurrentIndex+1, LastIndex, Larger, Smaller, next, Check
  >::type;
};

//////////////////////////////////////////////////////////////////////
/// @brief Implementation of difference metafunction. Base case.
//////////////////////////////////////////////////////////////////////
template<int LastIndex, typename Larger, typename Smaller, typename Running,
          template<typename...> class Check>
struct difference_impl<LastIndex, LastIndex, Larger, Smaller, Running, Check>
{
  using type = Running;
};

} // namespace detail


//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to compute the result of set difference between
///   two tuples.
///
///  For example, if Larger is <0,2,4> and Smaller is <0,4>, the result
///  will be <2>.
///
/// @tparam Larger Tuple of elements
/// @tparam Smaller Tuple of elements. Must have less or equal elements than
///    Larger.
//////////////////////////////////////////////////////////////////////
template<typename Larger, typename Smaller,
  template<typename...> class Check = tuple_ops::tuple_contains>
struct difference
{
  using type = typename detail::difference_impl<
    0, tuple_size<Larger>::value, Larger, Smaller, tuple<>, Check
  >::type;
};

} // namespace result_of
} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTIILTY_TUPLE_DIFFERENCE_HPP
