/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_PRINT_HPP
#define STAPL_UTILITY_TUPLE_PRINT_HPP

#include <ostream>
#include <stapl/utility/tuple/tuple.hpp>
#include <string>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Static functor that uses template recursion to print all
///  elements of a tuple with a comma delimiter.
//////////////////////////////////////////////////////////////////////
template<typename Tuple, int Index, int LastIndex>
struct print_tuple_impl
{
  static void apply(std::ostream& os, std::string sep, const Tuple& value)
  {
    os << std::get<Index>(value) << sep;

    print_tuple_impl<Tuple, Index + 1, LastIndex>::apply(os, sep, value);
  }
};


template<typename Tuple, int Index>
struct print_tuple_impl<Tuple, Index, Index>
{
  static void apply(std::ostream& os, std::string sep, const Tuple& value)
  {
    os << std::get<Index>(value);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Fallback implementation of print_tuple for an empty tuple.
/// @param os Output stream to write to.
//////////////////////////////////////////////////////////////////////
inline
std::ostream&
print_tuple(std::ostream& os, tuple<> const&)
{
  return os << "( )";
}

//////////////////////////////////////////////////////////////////////
/// @brief Simple tuple printer with default separator ','
/// @param os Output stream to write tuple to.
/// @param t The tuple to be printed.
//////////////////////////////////////////////////////////////////////
template<typename ...Args>
std::ostream&
print_tuple(std::ostream& os, tuple<Args...> const& t)
{
  os << "(";
  print_tuple_impl<
    tuple<Args...>, 0, sizeof...(Args)-1>::apply(os, ", ", t);
  return os << ")";
}

//////////////////////////////////////////////////////////////////////
/// @brief Simple tuple printer.
/// @param os Output stream to write tuple to.
/// @param sep Separator character
/// @param t The tuple to be printed.
//////////////////////////////////////////////////////////////////////
template<typename ...Args>
std::ostream&
print_tuple(std::ostream& os, std::string sep, tuple<Args...> const& t)
{
  os << "(";
  print_tuple_impl<
    tuple<Args...>, 0, sizeof...(Args)-1>::apply(os, sep, t);
  return os << ")";
}

template<typename T>
std::ostream&
print_tuple(std::ostream& os, T const& t)
{
  return os << t;
}

} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_PRINT_HPP
