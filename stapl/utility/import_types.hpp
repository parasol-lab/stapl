/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_IMPORT_TYPES_HPP
#define STAPL_UTILITY_IMPORT_TYPES_HPP

// Macro to allow importing types from another type.
// For example instead of writing:
//   typedef typename base_type::value_type value_type;
//   typedef typename base_type::reference  reference;
///
// STAPL_IMPORT_TYPES_2(typename base_type, value_type, reference).
//
#define STAPL_IMPORT_TYPE(B, T) \
  typedef B::T T;

#define STAPL_IMPORT_TYPES_1(B, T, ...)\
  typedef B::T T;

#define STAPL_IMPORT_TYPES_2(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_1(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_3(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_2(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_4(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_3(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_5(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_4(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_6(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_5(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_7(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_6(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_8(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_7(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_9(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_8(B, __VA_ARGS__)

#define STAPL_IMPORT_TYPES_10(B, T, ...)\
  typedef B::T T;\
  STAPL_IMPORT_TYPES_9(B, __VA_ARGS__)


#define STAPL_IMPORT_DTYPE(B, T) \
  typedef typename B::T T;

#define STAPL_IMPORT_DTYPES_1(B, T, ...)\
  typedef typename B::T T;

#define STAPL_IMPORT_DTYPES_2(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_1(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_3(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_2(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_4(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_3(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_5(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_4(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_6(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_5(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_7(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_6(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_8(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_7(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_9(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_8(B, __VA_ARGS__)

#define STAPL_IMPORT_DTYPES_10(B, T, ...)\
  typedef typename B::T T;\
  STAPL_IMPORT_DTYPES_9(B, __VA_ARGS__)

#endif
