/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_UTILITY_INTEGER_SEQUENCE_HPP
#define STAPL_UTILITY_INTEGER_SEQUENCE_HPP

#include <cstddef>
#include <type_traits>

namespace stapl {

////////////////////////////////////////////////////////////////////
/// @brief A compile-time sequence of integers.
///
/// This is implemented in C++14 and described in
/// http://open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3658.html
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T, T... Ints>
struct integer_sequence
{
  typedef T value_type;

  static constexpr std::size_t size(void) noexcept
  { return sizeof...(Ints); }
};


////////////////////////////////////////////////////////////////////
/// @brief Helper alias template for @ref integer_sequence for @c std::size_t.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<std::size_t... Ints>
using index_sequence = integer_sequence<std::size_t, Ints...>;


////////////////////////////////////////////////////////////////////
/// @brief Implementation of @ref make_index_sequence.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<std::size_t N, std::size_t... Ints>
struct make_index_sequence_impl
{
  typedef typename make_index_sequence_impl<N-1, N-1, Ints...>::type type;
};

////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref make_index_sequence_impl to end the
///        recursion.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<std::size_t... Ints>
struct make_index_sequence_impl<0, Ints...>
{
  typedef index_sequence<Ints...> type;
};


////////////////////////////////////////////////////////////////////
/// @brief Creates an @ref index_sequence with the values
///        <tt>0, 1, ..., N-1</tt>.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<std::size_t N>
using make_index_sequence = typename make_index_sequence_impl<N>::type;


////////////////////////////////////////////////////////////////////
/// @brief Metafunction to compute whether or not a type is an index_sequence.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_index_sequence : std::false_type {};

template<std::size_t... Indices>
struct is_index_sequence<index_sequence<Indices...>> : std::true_type {};

//////////////////////////////////////////////////////////////////////
/// @brief Get the I'th element in an @see integer_sequence.
//////////////////////////////////////////////////////////////////////
template<std::size_t I, class Sequence>
struct sequence_element;

template<class T, T Head, T... Tail>
struct sequence_element<0, integer_sequence<T, Head, Tail...>>
  : std::integral_constant<T, Head>
{ };

template<std::size_t I, class T, T Head, T... Tail>
struct sequence_element<I, integer_sequence<T, Head, Tail...>>
  : sequence_element<I - 1, integer_sequence<T, Tail...>>
{ };

} // namespace stapl

#endif // STAPL_UTILITY_INTEGER_SEQUENCE_HPP
