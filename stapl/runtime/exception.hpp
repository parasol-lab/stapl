/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_EXCEPTION_HPP
#define STAPL_RUNTIME_EXCEPTION_HPP

#include <exception>
#include "config/debug.hpp"
#include <boost/current_function.hpp>

namespace stapl {

namespace runtime {

void assert_fail(const char* s,
                 const char* file, unsigned int line, const char* function);

void assert_fail(const char* s, const char* function);

void warning(const char* s, const char* function);


//////////////////////////////////////////////////////////////////////
/// @brief Returns the debug level of the runtime.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
int get_debug_level(void) noexcept;


//////////////////////////////////////////////////////////////////////
/// @brief Runtime base exception object.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
class exception
: public std::exception
{
public:
  const char* what(void) const noexcept override
  { return "stapl::runtime::exception"; }
};


//////////////////////////////////////////////////////////////////////
/// @brief Runtime error exception object.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
class runtime_error
: public exception
{
private:
  const char* m_what;

public:
  explicit runtime_error(const char* what_arg = "stapl::runtime::runtime_error")
  : m_what(what_arg)
  { }

  const char* what(void) const noexcept override
  { return m_what; }
};

} // namespace runtime

} // namespace stapl


//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the error message @p M is
/// printed and the program aborts.
///
/// This is not disabled by @c STAPL_NDEBUG or @c STAPL_RUNTIME_NDEBUG.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_CHECK(E, M)                                           \
  ( (E) ?                                                                   \
    (static_cast<void>(0)) :                                                \
    (stapl::runtime::assert_fail("STAPL Runtime Check Failed (" #E "): " M, \
                                 __FILE__, __LINE__, BOOST_CURRENT_FUNCTION)) )


//////////////////////////////////////////////////////////////////////
/// @brief Prints the given message and aborts execution.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_ERROR(M) \
  stapl::runtime::assert_fail("STAPL Runtime Error: " M, BOOST_CURRENT_FUNCTION)


//////////////////////////////////////////////////////////////////////
/// @brief Prints the given message as a warning.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_WARNING(M) \
  stapl::runtime::warning("STAPL Runtime Warning: " M, BOOST_CURRENT_FUNCTION)


#ifdef STAPL_RUNTIME_DEBUG


//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the @p E is printed and the
/// program aborts.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_ASSERT(E)                                            \
  ( (std::uncaught_exception() || (E)) ?                                    \
    (static_cast<void>(0)) :                                                \
    (stapl::runtime::assert_fail("STAPL Runtime Assertion (" #E ") failed", \
                                 __FILE__, __LINE__, BOOST_CURRENT_FUNCTION)) )


//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the error message @p M is
/// printed and the program aborts.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_ASSERT_MSG(E, M)                                \
  ( (std::uncaught_exception() || (E)) ?                               \
    (static_cast<void>(0)) :                                           \
    (stapl::runtime::assert_fail("STAPL Runtime Assertion: \"" M "\"", \
                                 __FILE__, __LINE__, BOOST_CURRENT_FUNCTION)) )


#else


//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the @p E is printed and the
/// program aborts.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_ASSERT(E) \
  static_cast<void>(0)


//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the error message @p M is
/// printed and the program aborts.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_ASSERT_MSG(E, M) \
  static_cast<void>(0)


#endif // STAPL_RUNTIME_DEBUG

#endif
