/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_INSTRUMENTATION_HPP
#define STAPL_RUNTIME_INSTRUMENTATION_HPP

#include "config.hpp"
#include "primitive_traits.hpp"


#ifdef STAPL_RUNTIME_ENABLE_CALLBACKS
// callbacks support is requested
# include "instrumentation/callback.hpp"
#else
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Dummy version when @ref stapl::callback support is not enabled.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_CALL_CALLBACKS(x)
#endif


#ifdef STAPL_RUNTIME_ENABLE_INSTRUMENTATION
// instrument support is requested
# include "instrumentation/instrument.hpp"
#else
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Dummy version when @ref stapl::runtime::instrument support is not
///        enabled.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_CALL_INSTRUMENT(x)
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Dummy version when @ref stapl::runtime::instrument support is not
///        enabled.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_STATISTICS(c, n) static_cast<void>(0)
#endif


#ifdef STAPL_RUNTIME_USE_MPE
// MPE integration requested
# include "instrumentation/mpe.hpp"
#else
# define STAPL_RUNTIME_CALL_MPE(x,y)
#endif


#ifdef STAPL_RUNTIME_ENABLE_NO_COMM_GUARD
// no_comm_guard support is requested
# include "instrumentation/no_comm_guard.hpp"
#else
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Dummy version when @ref stapl::no_comm_guard support is not enabled.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_CALL_NO_COMM_GUARD(x)
#endif


#ifdef STAPL_RUNTIME_USE_VAMPIR
// vampir integration requested
# include "instrumentation/vampir.hpp"
#else
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Dummy version when TAU support is not enabled.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_CALL_VAMPIR(x)
#endif


#ifdef STAPL_RUNTIME_USE_TAU
// TAU integration requested
# include "instrumentation/tau.hpp"
#else
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Dummy version when TAU support is not enabled.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_CALL_TAU(x)
#endif


////////////////////////////////////////////////////////////////////
/// @brief Calls all requested instrumentation tools.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_PROFILE(s, traits)   \
  STAPL_RUNTIME_CALL_INSTRUMENT(s)         \
  STAPL_RUNTIME_CALL_CALLBACKS(s)          \
  STAPL_RUNTIME_CALL_NO_COMM_GUARD(traits) \
  STAPL_RUNTIME_CALL_MPE(s, traits)        \
  STAPL_RUNTIME_CALL_VAMPIR(s)             \
  STAPL_RUNTIME_CALL_TAU(s)                \
  static_cast<void>(0)

#endif
