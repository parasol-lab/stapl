/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_COMMUNICATOR_HPP
#define STAPL_RUNTIME_COMMUNICATOR_HPP

#include "topology.hpp"
#include "../message.hpp"
#include "../utility/any_range.hpp"
#include "../utility/option.hpp"
#include <vector>

namespace stapl {

namespace runtime {

////////////////////////////////////////////////////////////////////
/// @brief This class assists with sending messages between processes.
///
/// @ingroup processCommunication
///
/// @todo Merge it with the @ref runqueue class.
////////////////////////////////////////////////////////////////////
class communicator
{
public:
  using id               = process_id;
  using size_type        = std::size_t;
  using process_id_range = any_range<id>;

  ////////////////////////////////////////////////////////////////////
  /// @brief Initializes all communicators for this process.
  ///
  /// @param opts @ref option object to initialize the communicators with.
  ////////////////////////////////////////////////////////////////////
  static void initialize(option const& opts);

  ////////////////////////////////////////////////////////////////////
  /// @brief Initializes all communicators.
  ////////////////////////////////////////////////////////////////////
  static void finalize(void);

  ////////////////////////////////////////////////////////////////////
  /// @brief Starts the communication layer.
  ////////////////////////////////////////////////////////////////////
  static void start(void);

  ////////////////////////////////////////////////////////////////////
  /// @brief Stops the communication layer.
  ////////////////////////////////////////////////////////////////////
  static void stop(void);

  ////////////////////////////////////////////////////////////////////
  /// @brief Returns the id of this process.
  ////////////////////////////////////////////////////////////////////
  static int get_id(void) noexcept;

  ////////////////////////////////////////////////////////////////////
  /// @brief Returns the number of processes.
  ////////////////////////////////////////////////////////////////////
  static int size(void) noexcept;

  ////////////////////////////////////////////////////////////////////
  /// @brief Returns the number of processes per node.
  ////////////////////////////////////////////////////////////////////
  static int get_num_procs_per_node(void) noexcept;

  ////////////////////////////////////////////////////////////////////
  /// @brief Exits with the given exit code.
  ////////////////////////////////////////////////////////////////////
  static void exit(int exit_code);

  ////////////////////////////////////////////////////////////////////
  /// @brief Locks the communication layer.
  ////////////////////////////////////////////////////////////////////
  static void lock(void);

  ////////////////////////////////////////////////////////////////////
  /// @brief Unlocks the communication layer.
  ////////////////////////////////////////////////////////////////////
  static void unlock(void);

  ////////////////////////////////////////////////////////////////////
  /// @brief Sets the default message size.
  ///
  /// @param size New message size.
  ///
  /// @see message
  ////////////////////////////////////////////////////////////////////
  static void set_default_message_size(const std::size_t size);

  ////////////////////////////////////////////////////////////////////
  /// @brief Polls for incoming messages and returns a @ref message_slist of the
  ///        received messages.
  ///
  /// @param block if @c true, then the @ref communicator will block until it
  ///              receives a message
  ////////////////////////////////////////////////////////////////////
  static message_slist poll(const bool block = false);

  ////////////////////////////////////////////////////////////////////
  /// @brief Sends @p m to the process @p pid.
  ////////////////////////////////////////////////////////////////////
  static void send(const id pid, message_ptr m);

  ////////////////////////////////////////////////////////////////////
  /// @brief Sends @p m to the destinations in @p r.
  ////////////////////////////////////////////////////////////////////
  static void send_all(process_id_range r, message_ptr m);

  ////////////////////////////////////////////////////////////////////
  /// @brief Sends @p m to the destinations in @p v and keeps a copy.
  ////////////////////////////////////////////////////////////////////
  static void forward_and_store(std::vector<id> const& v, message_ptr m);

  ////////////////////////////////////////////////////////////////////
  /// @brief Sends @p m to the destinations in @p v filtering out @p excluded
  ///        and keeps a copy.
  ////////////////////////////////////////////////////////////////////
  static void forward_and_store(std::vector<id> const& v,
                                const id excluded,
                                message_ptr m);

  ////////////////////////////////////////////////////////////////////
  /// @brief Sends @p m to the destination described by @p t and keeps a copy.
  ////////////////////////////////////////////////////////////////////
  static void forward_and_store(topology const& t, message_ptr m);
};

} // namespace runtime

} // namespace stapl

#endif
