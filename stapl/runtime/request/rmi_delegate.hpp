/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_RMI_DELEGATE_HPP
#define STAPL_RUNTIME_RMI_DELEGATE_HPP

#include "../rmi_handle_fwd.hpp"
#include "../type_traits/type_id.hpp"
#include <cstring>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Delegate for an RMI request.
///
/// It describes the request by keeping the type id of the request and a handle
/// to the object it was invoked on.
///
/// Unlike ordinary delegates, the user cannot use this to call the function. It
/// is only a means for comparing requests.
///
/// It is used when combining requests so that two unrelated requests are not
/// accidentally combined.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
class rmi_delegate
{
private:
  class dummy_class;
  typedef void (dummy_class::*pmf_type)(void);

  type_id                          m_request;
  rmi_handle::internal_handle_type m_handle;
  pmf_type                         m_pmf;

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Constructs an invalid @ref rmi_delegate.
  //////////////////////////////////////////////////////////////////////
  constexpr rmi_delegate(void) noexcept
  : m_request(),
    m_pmf(nullptr)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Constructs a new @ref rmi_delegate based on the request type, the
  ///        object handle and the member function pointer.
  //////////////////////////////////////////////////////////////////////
  template<typename Handle, typename MemFun>
  rmi_delegate(const type_id request_id,
               Handle const& h,
               MemFun const& pmf) noexcept
  : m_request(request_id),
    m_handle(h.internal_handle()),
    m_pmf(nullptr)
  {
    static_assert(sizeof(MemFun)<=sizeof(pmf_type),
                  "Cannot store pointer to member function.");
    std::memcpy(&m_pmf, &pmf, sizeof(MemFun));
  }

  friend constexpr bool operator==(rmi_delegate const& x,
                                   rmi_delegate const& y) noexcept
  {
    return (x.m_request==y.m_request &&
            x.m_handle==y.m_handle   &&
            x.m_pmf==y.m_pmf);
  }

  friend constexpr bool operator!=(rmi_delegate const& x,
                                   rmi_delegate const& y) noexcept
  {
    return !(x==y);
  }
};

} // namespace runtime

} // namespace stapl

#endif
