/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_COUNTER_POSIX_CLOCK_GETTIME_TIMER_HPP
#define STAPL_RUNTIME_COUNTER_POSIX_CLOCK_GETTIME_TIMER_HPP

#include "../../exception.hpp"
#if defined(__APPLE)
# include <sys/time.h>
#else
# include <time.h>
#endif

#ifndef STAPL_USE_CLOCK_GETTIME_ID
# if defined(CLOCK_MONOTONIC_RAW)
// best for Linux (since Linux 2.6.28)
#  define STAPL_USE_CLOCK_GETTIME_ID CLOCK_MONOTONIC_RAW
# elif defined(_POSIX_MONOTONIC_CLOCK) || defined(CLOCK_MONOTONIC)
// second best for Linux and best for Solaris
#  define STAPL_USE_CLOCK_GETTIME_ID CLOCK_MONOTONIC
# elif defined(CLOCK_SGI_CYCLE)
// SGI
#  define STAPL_USE_CLOCK_GETTIME_ID CLOCK_SGI_CYCLE
# else
#  error "Automatic clock id discovery for clock_gettime() failed. " \
         "Please provide one through the STAPL_USE_CLOCK_GETTIME_ID flag."
# endif
#endif

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Time counter that uses @c clock_gettime() in POSIX systems.
///
/// It will try to identify the best possible clock id at compile time and can
/// be overridden only by setting the flag
/// @c STAPL_USE_CLOCK_GETTIME_ID=system-provided-value.
///
/// In GNU/Linux you have to link against @c -lrt.
///
/// @ingroup counters
//////////////////////////////////////////////////////////////////////
class clock_gettime_timer
{
public:
  typedef struct timespec raw_value_type;
  typedef double          value_type;

  static const clockid_t clk_id = STAPL_USE_CLOCK_GETTIME_ID;

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the name of the counter as a C string.
  //////////////////////////////////////////////////////////////////////
  static const char* name(void) noexcept
  {
    switch (clk_id) {
#if defined(CLOCK_MONOTONIC_RAW)
    case CLOCK_MONOTONIC_RAW:
      return "clock_gettime(CLOCK_MONOTONIC_RAW)";
#endif
#if defined(CLOCK_MONOTONIC)
    case CLOCK_MONOTONIC:
      return "clock_gettime(CLOCK_MONOTONIC)";
#endif
#if defined(CLOCK_SGI_CYCLE)
    case CLOCK_SGI_CYCLE:
      return "clock_gettime(CLOCK_SGI_CYCLE)";
#endif
#if defined(CLOCK_REALTIME)
    case CLOCK_REALTIME:
      return "clock_gettime(CLOCK_REALTIME)";
#endif
#if defined(CLOCK_PROCESS_CPUTIME_ID)
    case CLOCK_PROCESS_CPUTIME_ID:
      return "clock_gettime(CLOCK_PROCESS_CPUTIME_ID)";
#endif
#if defined(CLOCK_THREAD_CPUTIME_ID)
    case CLOCK_THREAD_CPUTIME_ID:
      return "clock_gettime(CLOCK_THREAD_CPUTIME_ID)";
#endif
    default:
      return "clock_gettime(unknown)";
    }
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the raw value from the counter.
  //////////////////////////////////////////////////////////////////////
  static raw_value_type read(void) noexcept
  {
    struct timespec nt = timespec();
    STAPL_RUNTIME_CHECK((clock_gettime(clk_id, &nt)==0),
                        "clock_gettime() failed");
    return nt;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Normalizes the given raw value to seconds.
  //////////////////////////////////////////////////////////////////////
  static constexpr value_type normalize(raw_value_type const& v) noexcept
  { return (double(v.tv_sec) + double(v.tv_nsec)*1.0E-9); }

private:
  raw_value_type m_v;

public:
  constexpr clock_gettime_timer(void) noexcept
  : m_v()
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Starts the counter.
  //////////////////////////////////////////////////////////////////////
  void start(void) noexcept
  { m_v = read(); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Stops the counter and returns the difference from @ref start() in
  ///        seconds.
  //////////////////////////////////////////////////////////////////////
  value_type stop(void) const noexcept
  {
    const raw_value_type v = read();
    const raw_value_type diff = { (v.tv_sec - m_v.tv_sec),
                                  (v.tv_nsec - m_v.tv_nsec) };
    return normalize(diff);
  }
};

} // namespace stapl

#endif
