/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_COUNTER_CONFIG_HPP
#define STAPL_RUNTIME_COUNTER_CONFIG_HPP

//////////////////////////////////////////////////////////////////////
/// @file
/// Chooses a suitable set of counters based on user preference and platform.
///
/// The user can override the platform-preferred counters by defining
/// @c STAPL_USE_TIMER at compile-time.
///
/// @ingroup counters
//////////////////////////////////////////////////////////////////////

#include "../config/platform.hpp"

#define STAPL_PAPI_TIMER           1
#define STAPL_MPI_WTIME_TIMER      2
#define STAPL_CLOCK_GETTIME_TIMER  3
#define STAPL_GETTIMEOFDAY_TIMER   4
#define STAPL_GETRUSAGE_TIMER      5
#define STAPL_GETTICKCOUNT_TIMER   6

#ifndef STAPL_USE_TIMER
// if no timer specified, attempt to autodetect one
# if defined(STAPL_USE_PAPI)
// PAPI exists, use that one
#  define STAPL_USE_TIMER STAPL_PAPI_TIMER
# elif defined(STAPL_RUNTIME_WINDOWS_TARGET)
// Microsoft Windows VisualC++ - use Windows High Performance Counters
#  define STAPL_USE_TIMER STAPL_GETTICKCOUNT_TIMER
# elif defined(STAPL_RUNTIME_BG_TARGET)
// BlueGene P/Q have gettimeofday() but not clock_gettime()
#  define STAPL_USE_TIMER STAPL_GETTIMEOFDAY_TIMER
# else
// assume a POSIX system, try to detect if clock_gettime() exists
#  include <unistd.h> // for _POSIX_TIMERS (POSIX.1-2001)
#  if _POSIX_TIMERS>0
#   define STAPL_USE_TIMER STAPL_CLOCK_GETTIME_TIMER
#  else
#   if defined(__APPLE) // for CLOCK_REALTIME
#    include <sys/time.h>
#   else
#    include <time.h>
#   endif
#   if defined(CLOCK_REALTIME)
#    define STAPL_USE_TIMER STAPL_CLOCK_GETTIME_TIMER
#   endif
#  endif // _POSIX_TIMERS
# endif // STAPL_USE_PAPI
#endif // STAPL_USE_TIMER

#ifndef STAPL_USE_TIMER
// if autodetection failed, fall back to gettimeofday()
# define STAPL_USE_TIMER STAPL_GETTIMEOFDAY_TIMER
#endif

#endif
