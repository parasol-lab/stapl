/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONCURRENCY_DETECT_DATA_RACE_HPP
#define STAPL_RUNTIME_CONCURRENCY_DETECT_DATA_RACE_HPP

#include "../exception.hpp"
#include <atomic>
#include <boost/current_function.hpp>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Helper class for detecting possible data races in a class static or
///        stand-alone function.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
struct data_race_helper
{
  const char*        m_file;
  const unsigned int m_line;
  const char*        m_function;
  std::atomic<int>&  m_i;

  data_race_helper(const char* file,
                   unsigned int line,
                   const char* function,
                   std::atomic<int>& i)
  : m_file(file),
    m_line(line),
    m_function(function),
    m_i(i)
  {
    if (++m_i!=1)
      assert_fail("Data race detected at entry", m_file, m_line, m_function);
  }

  ~data_race_helper(void)
  {
    if (--m_i!=0)
      assert_fail("Data race detected at exit", m_file, m_line, m_function);
  }
};

} // namespace runtime

} // namespace stapl


//////////////////////////////////////////////////////////////////////
/// @brief Detects data races in a class static or stand-alone function.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_DETECT_DATA_RACE()                   \
  static std::atomic<int> drd ## __LINE__(0);              \
  stapl::runtime::data_race_helper drd_helper ## __LINE__( \
    __FILE__, __LINE__, BOOST_CURRENT_FUNCTION, drd ## __LINE__);

#endif
