/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONCURRENCY_TBB_READ_WRITE_MUTEX_HPP
#define STAPL_RUNTIME_CONCURRENCY_TBB_READ_WRITE_MUTEX_HPP

#include <tbb/queuing_rw_mutex.h>

namespace stapl {

namespace runtime {

namespace tbb_impl {

//////////////////////////////////////////////////////////////////////
/// @brief An implementation of a read-write mutex based on
///        @c tbb::queueing_rw_mutex.
///
/// It implements the interface of @c std::mutex, enhanced to support read or
/// write locking.
///
/// The default locking policy is write locking.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
class read_write_mutex
{
private:
  tbb::queuing_rw_mutex              m_mutex;
  tbb::queuing_rw_mutex::scoped_lock m_lock;

public:
  read_write_mutex(void) = default;

  read_write_mutex(read_write_mutex const&) = delete;
  read_write_mutex& operator=(read_write_mutex const&) = delete;

  void lock(const read_lock_t)
  { m_lock.acquire(m_mutex, false); }

  void lock(const write_lock_t = write_lock)
  { m_lock.acquire(m_mutex, true); }

  bool try_lock(const read_lock_t)
  { return m_lock.try_acquire(m_mutex, false); }

  bool try_lock(const write_lock_t = write_lock)
  { return m_lock.try_acquire(m_mutex, true); }

  void unlock(void)
  { m_lock.release(); }
};

} // namespace tbb_impl


using tbb_impl::read_write_mutex;

} // namespace runtime

} // namespace stapl

#endif
