/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_RMI_SYNC_RMI_HPP
#define STAPL_RUNTIME_RMI_SYNC_RMI_HPP

#include "../aggregator.hpp"
#include "../context.hpp"
#include "../exception.hpp"
#include "../future.hpp"
#include "../instrumentation.hpp"
#include "../primitive_traits.hpp"
#include "../rmi_handle.hpp"
#include "../tags.hpp"
#include "../value_handle.hpp"
#include "../yield.hpp"
#include "../non_rmi/response.hpp"
#include "../request/sync_rmi_request.hpp"
#include "../type_traits/callable_traits.hpp"
#include "../type_traits/transport_qualifier.hpp"
#include <type_traits>
#include <utility>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Synchronous RMI primitive.
///
/// The given member function is called on the object in the destination
/// location.
///
/// @warning This is a function that can harm scalability. It exists to
///          facilitate one-sided synchronization. Use @ref opaque_rmi() or
///          @ref async_rmi() if possible.
///
/// @param dest Destination location.
/// @param h    Handle to the target object.
/// @param pmf  Member function to invoke.
/// @param t    Arguments to pass to the member function.
///
/// @return The return value of the invoked function.
///
/// @ingroup ARMIOneSided
//////////////////////////////////////////////////////////////////////
template<typename Handle, typename MemFun, typename... T>
typename callable_traits<MemFun>::result_type
sync_rmi(unsigned int dest, Handle const& h, MemFun const& pmf, T&&... t)
{
  using namespace stapl::runtime;

  auto& ctx = this_context::get();

  static_assert(is_appropriate_handle<Handle, MemFun>::value,
                "Incompatible qualifiers between handle and member function");
  STAPL_RUNTIME_ASSERT_MSG(h.valid(), "Invalid handle");
  STAPL_RUNTIME_ASSERT_MSG(h.is_valid(dest),
                           "p_object does not exist in destination");

  STAPL_RUNTIME_PROFILE("sync_rmi()", (primitive_traits::blocking |
                                       primitive_traits::ordered  |
                                       primitive_traits::p2p      |
                                       primitive_traits::comm));

  using result_type        = typename callable_traits<MemFun>::result_type;
  using return_handle_type = value_handle<result_type>;
  using response_type      = response<return_handle_type>;

  return_handle_type rh;
  {
    aggregator a{ctx, h, dest};
    const bool on_shmem = a.is_on_shmem();

    if (on_shmem) {
      using request_type =
        sync_rmi_request<
          response_type,
          packed_handle_type,
          MemFun,
          typename transport_qualifier<decltype(t)>::type...>;

      auto const size = request_type::expected_size(std::forward<T>(t)...);
      new(a.allocate(size)) request_type{rh, h, pmf, std::forward<T>(t)...};
    }
    else {
      using request_type =
        sync_rmi_request<response_type,
                         packed_handle_type,
                         MemFun,
                         typename std::remove_reference<T>::type...>;

      auto const size = request_type::expected_size(std::forward<T>(t)...);
      new(a.allocate(size)) request_type{rh, h, pmf, std::forward<T>(t)...};
    }
  }
  return rh.get(ctx);
}

} // namespace stapl

#endif
