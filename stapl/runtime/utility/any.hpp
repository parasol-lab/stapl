/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_ANY_HPP
#define STAPL_RUNTIME_UTILITY_ANY_HPP

#include "../exception.hpp"
#include <algorithm>
#include <typeinfo>
#include <utility>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief @ref any can hold an object of any type. The object can be retrieved
///        only by casting to the correct type.
///
/// Provides the functionality of @c boost::any without dynamic_casts or
/// checks when in non-debug mode.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
class any
{
private:
  //////////////////////////////////////////////////////////////////////
  /// @brief Base class for holding any object.
  //////////////////////////////////////////////////////////////////////
  class placeholder
  {
  public:
    placeholder(void) = default;
    placeholder(placeholder const&) = delete;
    placeholder& operator=(placeholder const&) = delete;
    virtual ~placeholder(void) = default;
    virtual std::type_info const& type(void) const noexcept = 0;
    virtual placeholder* clone(void) const = 0;
  };

  //////////////////////////////////////////////////////////////////////
  /// @brief Implementation of @ref placeholder for objects of type @p T.
  //////////////////////////////////////////////////////////////////////
  template<typename T>
  class holder final
  : public placeholder
  {
  private:
    T m_t;

  public:
    explicit holder(T const& t)
    : m_t(t)
    { }

    explicit holder(T&& t)
    : m_t(std::move(t))
    { }

    std::type_info const& type(void) const noexcept
    { return typeid(T); }

    placeholder* clone(void) const
    { return new holder(m_t); }

    T const& get(void) const noexcept
    { return m_t; }

    T& get(void) noexcept
    { return m_t; }
  };

  placeholder* m_p;

public:
  constexpr any(void) noexcept
  : m_p(nullptr)
  { }

  template<typename T>
  any(T&& t)
  : m_p(new holder<typename std::decay<T>::type>(std::forward<T>(t)))
  { }

  any(any&& other)
  : m_p(other.m_p)
  { other.m_p = nullptr; }

  any(any const& other)
  : m_p(other.empty() ? nullptr : other.m_p->clone())
  { }

  ~any(void)
  { delete m_p; }

  template<typename T>
  any& operator=(T&& other)
  {
    any(std::forward<T>(other)).swap(*this);
    return *this;
  }

  any& operator=(any other)
  {
    other.swap(*this);
    return *this;
  }

  any& swap(any& other)
  {
    using std::swap;
    swap(m_p, other.m_p);
    return *this;
  }

  bool empty(void) const noexcept
  { return !m_p; }

  std::type_info const& type(void) const noexcept
  { return (empty() ? typeid(void) : m_p->type()); }

  template<typename T>
  T const* try_get(void) const noexcept
  {
    if (empty())
      return nullptr;
    STAPL_RUNTIME_ASSERT(typeid(T)==type());
    return &(static_cast<holder<T>*>(m_p)->get());
  }

  template<typename T>
  T* try_get(void) noexcept
  {
    if (empty())
      return nullptr;
    STAPL_RUNTIME_ASSERT(typeid(T)==type());
    return &(static_cast<holder<T>*>(m_p)->get());
  }

  template<typename T>
  T const& get(void) const noexcept
  { return *try_get<T>(); }

  template<typename T>
  T& get(void) noexcept
  { return *try_get<T>(); }
};

} // namespace runtime

} // namespace stapl

#endif
