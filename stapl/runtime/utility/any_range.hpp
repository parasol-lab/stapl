/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_ANY_RANGE_HPP
#define STAPL_RUNTIME_UTILITY_ANY_RANGE_HPP

#include "../exception.hpp"
#include <iterator>
#include <utility>
#include <boost/range/any_range.hpp>
#include <boost/range/size.hpp>

namespace stapl {

namespace runtime {

////////////////////////////////////////////////////////////////////
/// @brief Type erased range of const objects with associated size information.
///
/// @tparam T Range object type.
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
template<typename T>
class any_range
{
public:
  using size_type      = std::size_t;
private:
  using range_type     = boost::any_range<T,
                                          boost::forward_traversal_tag,
                                          const T,
                                          std::ptrdiff_t>;
public:
  using const_iterator = typename range_type::const_iterator;
  using iterator       = const_iterator;

private:
  size_type  m_size;
  range_type m_range;

public:
  any_range(void)
  : m_size(0)
  { }

  template<typename U>
  any_range(U&& u)
  : m_size(boost::size(u)),
    m_range(std::forward<U>(u))
  { }

  template<typename U>
  any_range(U&& u, const size_type size)
  : m_size(size),
    m_range(std::forward<U>(u))
  { STAPL_RUNTIME_ASSERT(boost::size(m_range)==m_size); }

  bool empty(void) const noexcept
  { return (m_size==0); }

  size_type size(void) const
  { return m_size; }

  const_iterator begin(void) const noexcept
  { return m_range.begin(); }

  const_iterator end(void) const noexcept
  { return m_range.end(); }
};

} // namespace runtime

} // namespace stapl

#endif
