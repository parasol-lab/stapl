/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_CALLABLE_TRAITS_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_CALLABLE_TRAITS_HPP

#include <tuple>

namespace stapl {

////////////////////////////////////////////////////////////////////
/// @brief Callable introspection class.
///
/// This class provides member typedef @c object_type for the target object
/// type, member typedef @c result_type for the return type and member
/// typedef @c parameter_types that is an @c std::tuple with the parameter types
/// of the function.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename F>
struct callable_traits
{
  using result_type =
    typename callable_traits<decltype(&F::operator())>::result_type;
  using parameter_types =
    typename callable_traits<decltype(&F::operator())>::parameter_types;
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for pointers to member
///        functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename C,
         typename... T>
struct callable_traits<R(C::*)(T...)>
{
  using object_type     = C;
  using result_type     = R;
  using parameter_types = std::tuple<T...>;
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for pointers to @c const
///        member functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename C,
         typename... T>
struct callable_traits<R(C::*)(T...) const>
{
  using object_type     = C;
  using result_type     = R;
  using parameter_types = std::tuple<T...>;
};

# if __cplusplus >= 201703L
////////////////////////////////////////////////////////////////////
/// NOTE: Added due to C++17 standard: noexcept now is part of a function's
///       signature (but cannot be used for overloading)
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for pointers to @c
///        noexcept member functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename C,
         typename... T>
struct callable_traits<R(C::*)(T...) noexcept>
{
  using object_type     = C;
  using result_type     = R;
  using parameter_types = std::tuple<T...>;
};

////////////////////////////////////////////////////////////////////
/// NOTE: Added due to C++17 standard: noexcept now is part of a function's
///       signature (but cannot be used for overloading)
////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for pointers to @c const
///        noexcept member functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename C,
         typename... T>
struct callable_traits<R(C::*)(T...) const noexcept>
{
  using object_type     = C;
  using result_type     = R;
  using parameter_types = std::tuple<T...>;
};
# endif

////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for pointers to @c volatile
///        member functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename C,
         typename... T>
struct callable_traits<R(C::*)(T...) volatile>
{
  using object_type     = C;
  using result_type     = R;
  using parameter_types = std::tuple<T...>;
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for pointers to @c const
///        @c volatile member functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename C,
         typename... T>
struct callable_traits<R(C::*)(T...) const volatile>
{
  using object_type     = C;
  using result_type     = R;
  using parameter_types = std::tuple<T...>;
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for @c const pointers.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename C>
struct callable_traits<R(C::*const)>
: public callable_traits<R(C::*)>
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename... T>
struct callable_traits<R(T...)>
{
  using result_type     = R;
  using parameter_types = std::tuple<T...>;
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref callable_traits for pointers to functions.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename R,
         typename... T>
struct callable_traits<R(*)(T...)>
: public callable_traits<R(T...)>
{ };

} // namespace stapl

#endif
