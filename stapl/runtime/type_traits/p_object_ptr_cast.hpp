/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_P_OBJECT_PTR_CAST_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_P_OBJECT_PTR_CAST_HPP

#include "has_virtual_base.hpp"
#include "no_access_check_cast.hpp"
#include <type_traits>

namespace stapl {

class p_object;


namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Casts the @c void pointer to pointer to @p T.
///
/// @related p_object_ptr_cast()
/// @ingroup runtimeTypeTraitsImpl
//////////////////////////////////////////////////////////////////////
template<typename T, typename = void>
struct p_object_ptr_cast_impl
{
  static T* apply(void* p) noexcept
  { return static_cast<T*>(p); }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref p_object_ptr_cast_impl for @p T that derives
///        from @ref p_object and does not define @c T::has_virtual_base.
///
/// @related p_object_ptr_cast()
/// @ingroup runtimeTypeTraitsImpl
//////////////////////////////////////////////////////////////////////
template<typename T>
struct p_object_ptr_cast_impl<T,
                              typename std::enable_if<
                                std::is_base_of<p_object, T>::value &&
                                !has_virtual_base<T>::value
                              >::type>
{
  static T* apply(void* p) noexcept
  { return no_access_check_cast<T*>(static_cast<p_object*>(p)); }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref p_object_ptr_cast_impl for @p T that derives
///        from @ref p_object and defines @c T::has_virtual_base.
///
/// @related p_object_ptr_cast()
/// @ingroup runtimeTypeTraitsImpl
///
/// @todo If we had has_virtual_base (or is a virtual base between p_object and
///       T) we can specialize this to allow static_cast if not. We're forced to
///       dynamic_cast because of virtual inheritance.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct p_object_ptr_cast_impl<T,
                              typename std::enable_if<
                                std::is_base_of<p_object, T>::value &&
                                has_virtual_base<T>::value
                              >::type>
{
  static T* apply(void* p) noexcept
  { return dynamic_cast<T*>(static_cast<p_object*>(p)); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Casts the @c void pointer to @p T, where @p T is a distributed
///        object.
///
/// @ingroup runtimeTypeTraits
//////////////////////////////////////////////////////////////////////
template<typename T>
T* p_object_ptr_cast(void* p) noexcept
{
  return p_object_ptr_cast_impl<T>::apply(p);
}

} // namespace runtime

} // namespace stapl

#endif
