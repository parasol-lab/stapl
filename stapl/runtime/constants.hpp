/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONSTANTS_HPP
#define STAPL_RUNTIME_CONSTANTS_HPP

#include "config/types.hpp"
#include <limits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Invalid process id.
///
/// @ingroup ARMI
//////////////////////////////////////////////////////////////////////
constexpr process_id invalid_process_id = -1;


//////////////////////////////////////////////////////////////////////
/// @brief Invalid location id.
///
/// @ingroup ARMI
//////////////////////////////////////////////////////////////////////
constexpr unsigned int invalid_location_id =
  std::numeric_limits<unsigned int>::max();


namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Invalid gang id.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
constexpr gang_id invalid_gang_id = std::numeric_limits<gang_id>::max();


//////////////////////////////////////////////////////////////////////
/// @brief Invalid @ref object_id.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
constexpr object_id invalid_object_id = std::numeric_limits<object_id>::max();

} // namespace runtime

} // namespace stapl

#endif
