/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_IMMUTABLE_SHARED_HPP
#define STAPL_RUNTIME_IMMUTABLE_SHARED_HPP

#include "serialization.hpp"
#include "type_traits/is_basic.hpp"
#include "type_traits/is_copyable.hpp"
#include <memory>
#include <tuple>
#include <type_traits>
#include <utility>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Describes an immutable object.
///
/// @tparam T Object type.
///
/// Immutable objects may be passed by reference when communication happens in
/// shared memory. The object can never be mutated and it is deleted when the
/// last @ref immutable_shared is destroyed.
///
/// Locations that are on shared memory may have a single object accessible
/// through their @ref immutable_shared. Locations that are on different address
/// spaces are guaranteed to have distinct copies of the object.
///
/// @see immutable_wrapper
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T,
         bool = (sizeof(T)<=sizeof(std::shared_ptr<T>) && is_basic<T>::value)>
class immutable_shared
{
public:
  typedef T element_type;

private:
  std::shared_ptr<const T> m_p;

public:
  template<typename... Args>
  explicit immutable_shared(Args&&... args)
  : m_p(std::make_shared<const T>(std::forward<Args>(args)...))
  { }

  immutable_shared(immutable_shared const&) = default;
  immutable_shared(immutable_shared&&) = default;

  operator T const&(void) const noexcept
  { return *m_p; }

  T const& get(void) const noexcept
  { return *m_p; }

  long use_count(void) const
  { return m_p.use_count(); }

  bool unique(void) const
  { return m_p.unique(); }

  void define_type(typer& t)
  { t.member(m_p); }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref immutable_shared for basic types for which
///        <tt>sizeof(T)<=sizeof(std::shared_ptr<T>)</tt>.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T>
class immutable_shared<T, true>
{
public:
  typedef const T       element_type;
  typedef std::tuple<T> member_types;

private:
  T m_t;

public:
  template<typename... Args>
  explicit immutable_shared(Args&&... args)
  : m_t(std::forward<Args>(args)...)
  { }

  immutable_shared(immutable_shared const&) = default;
  immutable_shared(immutable_shared&&) = default;

  operator T const&(void) const noexcept
  { return m_t; }

  T const& get(void) const noexcept
  { return m_t; }

  constexpr long use_count(void) const noexcept
  { return 1l; }

  constexpr bool unique(void) const noexcept
  { return true; }
};


//////////////////////////////////////////////////////////////////////
/// @brief Constructs an immutable object of type @p T.
///
/// Immutable objects may be passed by reference when communication happens in
/// shared memory.
///
/// The object can never be mutated and it is deleted when the last
/// @ref immutable_shared is destroyed.
///
/// @see immutable
/// @related immutable_shared
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T, typename... Args>
immutable_shared<T> make_immutable_shared(Args&&... args)
{
  return immutable_shared<T>(std::forward<Args>(args)...);
}


namespace runtime {

////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_copyable for @ref immutable_shared.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_copyable<immutable_shared<T, false>>
: public std::true_type
{ };

} // namespace runtime

} // namespace stapl

#endif
