/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_BENCHMARKS_FMM_VERIFY_H
#define STAPL_BENCHMARKS_FMM_VERIFY_H

#include "logger.h"

/// Verify results
class Verify
{
public:
  /// Get sum of scalar component of a vector of target bodies
  double getSumScalar(Bodies & bodies)
  {
    // Initialize difference
    double v = 0;
    // Loop over bodies
    for (B_iter B=bodies.begin(); B!=bodies.end(); B++) {
      // Sum of scalar component
      v += B->TRG[0] * B->SRC;
    // End loop over bodies
    }
    // Return difference
    return v;
  }

  /// Get norm of scalar component of a vector of target bodies
  double getNrmScalar(Bodies & bodies)
  {
    // Initialize norm
    double v = 0;
    // Loop over bodies
    for (B_iter B=bodies.begin(); B!=bodies.end(); B++) {
      // Norm of scalar component
      v += B->TRG[0] * B->TRG[0];
    // End loop over bodies
    }
    // Return norm
    return v;
  }

  /// Get difference between scalar component of two vectors of target bodies
  double getDifScalar(Bodies & bodies, Bodies & bodies2)
  {
    // Initialize difference
    double v = 0;
    // Set iterator of bodies2
    B_iter B2 = bodies2.begin();
    // Loop over bodies & bodies2
    for (B_iter B=bodies.begin(); B!=bodies.end(); B++, B2++) {
      // Difference of scalar component
      v += (B->TRG[0] - B2->TRG[0]) * (B->TRG[0] - B2->TRG[0]);
    // End loop over bodies & bodies2
    }
    // Return difference
    return v;
  }

  /// Get difference between scalar component of two vectors of target bodies
  double getRelScalar(Bodies & bodies, Bodies & bodies2)
  {
    // Initialize difference
    double v = 0;
    // Set iterator of bodies2
    B_iter B2 = bodies2.begin();
    // Loop over bodies & bodies2
    for (B_iter B=bodies.begin(); B!=bodies.end(); B++, B2++) {
      v += ((B->TRG[0] - B2->TRG[0]) * (B->TRG[0] - B2->TRG[0]))
            // Difference of scalar component
            / (B2->TRG[0] * B2->TRG[0]);
    }
    // Return difference
    return v;
  }

  /// Get norm of scalar component of a vector of target bodies
  double getNrmVector(Bodies & bodies) {
    // Initialize norm
    double v = 0;
    // Loop over bodies
    for (B_iter B=bodies.begin(); B!=bodies.end(); B++) {
      // Norm of vector x component
      v += B->TRG[1] * B->TRG[1]
           // Norm of vector y component
           +  B->TRG[2] * B->TRG[2]
           // Norm of vector z component
           +  B->TRG[3] * B->TRG[3];
    }
    // Return norm
    return v;
  }

  /// Get difference between scalar component of two vectors of target bodies
  double getDifVector(Bodies & bodies, Bodies & bodies2)
  {
    // Initialize difference
    double v = 0;
    // Set iterator of bodies2
    B_iter B2 = bodies2.begin();
    // Loop over bodies & bodies2
    for (B_iter B=bodies.begin(); B!=bodies.end(); B++, B2++) {
      // Difference of vector x component
      v += (B->TRG[1] - B2->TRG[1]) * (B->TRG[1] - B2->TRG[1])
           // Difference of vector y component
           +  (B->TRG[2] - B2->TRG[2]) * (B->TRG[2] - B2->TRG[2])
           // Difference of vector z component
           +  (B->TRG[3] - B2->TRG[3]) * (B->TRG[3] - B2->TRG[3]);
    }
    // Return difference
    return v;
  }

  /// Get difference between scalar component of two vectors of target bodies
  double getRelVector(Bodies & bodies, Bodies & bodies2)
  {
    // Initialize difference
    double v = 0;
    // Set iterator of bodies2
    B_iter B2 = bodies2.begin();
    // Loop over bodies & bodies2
    for (B_iter B=bodies.begin(); B!=bodies.end(); B++, B2++) {
      // Difference of vector x component
      v += ((B->TRG[1] - B2->TRG[1]) * (B->TRG[1] - B2->TRG[1]) +
           // Difference of vector y component
           (B->TRG[2] - B2->TRG[2]) * (B->TRG[2] - B2->TRG[2]) +
           // Difference of vector z component
           (B->TRG[3] - B2->TRG[3]) * (B->TRG[3] - B2->TRG[3]))
           // Norm of vector x component
           / (B2->TRG[1] * B2->TRG[1] +
           // Norm of vector y component
           B2->TRG[2] * B2->TRG[2] +
           // Norm of vector z component
           B2->TRG[3] * B2->TRG[3]);
    }
    // Return difference
    return v;
  }

  /// Print relative L2 norm scalar error
  void print(std::string title, double v)
  {
    // If verbose flag is true
    if (logger::verbose) {
      // Set format
      std::cout << std::setw(logger::stringLength) << std::left
                // Set title
                << title << " : "
                << std::setprecision(logger::decimal) << std::scientific
                // Print potential error
                << v << std::endl;
    }
  }
};

#endif // STAPL_BENCHMARKS_FMM_VERIFY_H
