/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_BENCHMARKS_FMM_KAHAN_H
#define STAPL_BENCHMARKS_FMM_KAHAN_H

#include <iostream>
#ifndef __CUDACC__
#define __host__
#define __device__
#define __forceinline__
#endif
//! Operator overloading for Kahan summation
template<typename T>
struct kahan
{
  T s;
  T c;
  __host__ __device__ __forceinline__
  // Default constructor
  kahan(){}
  __host__ __device__ __forceinline__
  // Copy constructor (scalar)
  kahan(T const& v)
  {
    s = v;
    c = 0;
  }
  __host__ __device__ __forceinline__
  // Copy constructor (structure)
  kahan(kahan const& v)
  {
    s = v.s;
    c = v.c;
  }
  __host__ __device__ __forceinline__
  // Destructor
  ~kahan(){}
  __host__ __device__ __forceinline__
  // Scalar assignment
  kahan const& operator=(const T v)
  {
    s = v;
    c = 0;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Scalar compound assignment (add)
  kahan const& operator+=(const T v)
  {
    T y = v - c;
    T t = s + y;
    c = (t - s) - y;
    s = t;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Scalar compound assignment (subtract)
  kahan const& operator-=(const T v)
  {
    T y = - v - c;
    T t = s + y;
    c = (t - s) - y;
    s = t;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Scalar compound assignment (multiply)
  kahan const& operator*=(const T v)
  {
    c *= v;
    s *= v;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Scalar compound assignment (divide)
  kahan const& operator/=(const T v)
  {
    c /= v;
    s /= v;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Vector assignment
  kahan const& operator=(kahan const& v)
  {
    s = v.s;
    c = v.c;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Vector compound assignment (add)
  kahan const& operator+=(kahan const& v)
  {
    T y = v.s - c;
    T t = s + y;
    c = (t - s) - y;
    s = t;
    y = v.c - c;
    t = s + y;
    c = (t - s) - y;
    s = t;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Vector compound assignment (subtract)
  kahan const& operator-=(kahan const& v)
  {
    T y = - v.s - c;
    T t = s + y;
    c = (t - s) - y;
    s = t;
    y = - v.c - c;
    t = s + y;
    c = (t - s) - y;
    s = t;
    return *this;
  }
  __host__ __device__ __forceinline__
  // Vector compound assignment (multiply)
  kahan const& operator*=(kahan const& v)
  {
    c *= (v.c + v.s);
    s *= (v.c + v.s);
    return *this;
  }
  __host__ __device__ __forceinline__
  // Vector compound assignment (divide)
  kahan const& operator/=(kahan const& v)
  {
    c /= (v.c + v.s);
    s /= (v.c + v.s);
    return *this;
  }
  __host__ __device__ __forceinline__
  // Vector arithmetic (negation)
  kahan operator-() const
  {
    kahan temp;
    temp.s = -s;
    temp.c = -c;
    return temp;
  }
  __host__ __device__ __forceinline__
  // Type-casting (lvalue)
  operator       T ()       {return s+c;}
  __host__ __device__ __forceinline__
  // Type-casting (rvalue)
  operator const T () const {return s+c;}
  // Output stream
  friend std::ostream &operator<<(std::ostream & s, kahan const& v)
  {
    s << (v.s + v.c);
    return s;
  }
  // Input stream
  friend std::istream &operator>>(std::istream & s, kahan & v)
  {
    s >> v.s;
    v.c = 0;
    return s;
  }
};

#endif // STAPL_BENCHMARKS_FMM_KAHAN_H
