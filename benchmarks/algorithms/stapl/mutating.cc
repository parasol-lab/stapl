/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include "../utilities.hpp"

#include <vector>
#include <iostream>
#include <sstream>
#include <stapl/array.hpp>
#include <stapl/algorithms/functional.hpp>

template <typename T1, typename T2>
struct equal_plus_one
{
  typedef bool result_type;

  equal_plus_one()
  {
  }

  result_type operator() (const T1& x, const T2& y) const
  {
    return x == y+1;
  }
};

stapl::exit_code stapl_main(int argc, char *argv[])
{
  // Default values. Size is the number of elements in the tested arrays.
  // Duplicate Ratio is used in the data generation of unique copy to specify
  // the number of duplicates in the input.
  // Weak_scaling weak_scaling specifies whether the data should be generated
  // sequentially to ensure the same input is used for all core counts in a
  // strong scaling study, or in parallel which allows for faster generation
  // needed in weak scaling where the input grows with core count.
  size_t size = atoll(argv[1]);
  double duplicate_ratio = 0.001;
  int weak_scaling = 0; // True or false.
  int data_distribution = 0; // 0=balanced, 1=block, 2=block_cyclic
  stapl::distribution_spec<> data_dist = stapl::balance(size);
  stapl::distribution_spec<> data_distx2 = stapl::balance(size*2);
  int block_size=2048;

  if (argc > 5)
  {
    size = atoll(argv[1]);
    duplicate_ratio = atof(argv[2]);
    weak_scaling = atoi(argv[3]);
    data_distribution = atoi(argv[4]);
    block_size = atoi(argv[5]);
  }
  else if (argc > 4)
  {
    size = atoll(argv[1]);
    duplicate_ratio = atof(argv[2]);
    data_distribution = atoi(argv[3]);
    block_size = atoi(argv[4]);
  }
  else if (argc > 3)
  {
    size = atoll(argv[1]);
    data_distribution = atoi(argv[2]);
    block_size = atoi(argv[3]);
  }

  if (data_distribution == 1)
  {
    data_dist = stapl::block(size,block_size);
    data_distx2 = stapl::block(size*2,block_size);
  }
  else if (data_distribution == 2)
  {
    data_dist = stapl::block_cyclic(size,block_size);
    data_distx2 = stapl::block_cyclic(size*2,block_size);
  }

  std::vector<double> gen_samples(10,0.), gen_n_samples(10,0.),
    rep_samples(10,0.), rif_samples(10,0.), tra_samples(10,0.),
    mer_samples(10,0.);

  bool gen_correct(true), gen_n_correct(true), rep_correct(true),
    rif_correct(true), tra_correct(true), mer_correct(true);

  counter_t timer;

  // GENERATE
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont(data_dist);
    array_type_vw x(xcont);

    timer.reset();
    timer.start();

    stapl::generate(x, []{ return 81372.0; });

    gen_samples[sample] = timer.stop();

    stapl::do_once([&xcont, &gen_correct, &size]{
     gen_correct  = gen_correct && (xcont[std::rand()%size] == 81372.0);
    });
  }
  report_result("b_generate","STAPL",gen_correct, gen_samples);

  // GENERATE N
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont(data_dist);
    array_type_vw x(xcont);

    timer.reset();
    timer.start();

    stapl::generate_n(x, 0, size, []{ return 81372.0; });

    gen_n_samples[sample] = timer.stop();

    stapl::do_once([&xcont, &gen_n_correct, &size]{
      gen_n_correct  = gen_n_correct && (xcont[std::rand()%size] == 81372.0);
    });
  }
  report_result("b_generate_n","STAPL",gen_n_correct, gen_n_samples);

  // REPLACE IF and REPLACE
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont(data_dist);
    array_type_vw x(xcont);

    fill_random(x, weak_scaling);
    timer.reset();
    timer.start();

    stapl::replace_if(x, std::bind(std::less<double>(),
      std::placeholders::_1, 0.25), 0.5);

    rif_samples[sample] = timer.stop();

    auto result0 = stapl::find_if(x, std::bind(std::less<double>(),
      std::placeholders::_1, 0.25));

    rif_correct = rif_correct && stapl::is_null_reference(result0);

    timer.reset();
    timer.start();

    stapl::replace(x, 0.5, 0.125);

    rep_samples[sample] = timer.stop();

    auto result1 = stapl::accumulate(x, 0.);

    rep_correct = rep_correct && result1/size > 0.49 && result1/size < 0.51;
  }
  report_result("b_replace","STAPL", rep_correct, rep_samples);
  report_result("b_replace_if","STAPL", rif_correct, rif_samples);

  // TRANSFORM
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont(data_dist);
    array_type_vw x(xcont);

    fill_random(x, weak_scaling);
    array_type original(xcont);
    array_type_vw o(original);
    timer.reset();
    timer.start();

    stapl::transform(x, x, [](double i) { return i+1;});

    tra_samples[sample] = timer.stop();

    bool test = stapl::equal(x, o, equal_plus_one<double, double>());
    tra_correct = tra_correct && test;
   }
  report_result("b_transform","STAPL", tra_correct, tra_samples);

  // MERGE
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont1(data_dist);
    array_type_vw x1(xcont1);
    array_type xcont2(data_dist);
    array_type_vw x2(xcont2);
    array_type xcont3(data_distx2);
    array_type_vw x3(xcont3);

    fill_random(x1, weak_scaling);
    fill_random(x2, weak_scaling);

    // merge requires sorted inputs
    stapl::do_once([&x1,&x2]{
      std::sort(x1.begin(), x1.end());
      std::sort(x2.begin(), x2.end());
    });

    double post_merge = stapl::accumulate(x1,0.0);
    post_merge += stapl::accumulate(x2,0.0);

    timer.reset();
    timer.start();

    stapl::merge(x1, x2, x3);

    mer_samples[sample] = timer.stop();

    double pre_merge = stapl::accumulate(x3,0.0);
    if ((post_merge * 1.05) <= pre_merge || (post_merge * 0.95) >= pre_merge )
      mer_correct = false;
  }
  report_result("b_merge","STAPL",mer_correct, mer_samples);

  return EXIT_SUCCESS;
}
