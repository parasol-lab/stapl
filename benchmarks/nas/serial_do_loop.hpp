/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SERIAL_DO_LOOP_HPP
#define SERIAL_DO_LOOP_HPP

namespace stapl {

// The Beautiful Recursive version that is slow & blows out the stack,
// but really is the essence of the algorithm...
/*
//  if (n != 1)
//  {
//    auto res = serial_do_loop(n - 1, wf, z, r, p, rho);
//
//    return wf(n, get<0>(res), get<1>(res), get<2>(res), get<3>(res));
//  }
//
//  return wf(n, z, r, p, rho);
*/

namespace detail {

struct reset_element
{
  template<typename V>
  void operator()(composition::map_view<V>& lhs,
                  composition::map_view<V> const& rhs) const
  {
    lhs = rhs;
  }

  // FIXME - a unified proxy overload is just fine (all below specializations
  // do the same thing).  Good for debugging as written now, as now proxies I'm
  // not expecting slide by...
  //

  template<typename T, typename View>
  void operator()(proxy<T, edge_accessor<View> >& lhs,
                  proxy<T, edge_accessor<View> > const& rhs) const
  {
    proxy_core_access::reset(lhs, rhs);
  }


  template<typename T, typename Reference, typename Functor>
  void operator()(proxy<T, unary_tm_accessor<Reference, Functor> >& lhs,
                  proxy<T, unary_tm_accessor<Reference, Functor> > const& rhs) const
  {
    proxy_core_access::reset(lhs, rhs);
  }

  template<typename T, typename Reference1, typename Reference2, typename Functor>
  void operator()(proxy<T, binary_tm_accessor<Reference1, Reference2, Functor> >& lhs,
                  proxy<T, binary_tm_accessor<Reference1, Reference2, Functor> > const& rhs) const
  {
    proxy_core_access::reset(lhs, rhs);
  }

}; // struct reset_element

} // namespace detail


// FIXME - generalize to arbitrary number of arguments, and collapse these two
// function template signatures into that...
//
template<typename WF, typename View1D, typename Reference>
auto
serial_do_loop(std::size_t n, WF const& wf,
               View1D const& z, View1D const& r, View1D const& p, Reference rho)
  -> decltype(wf(n, z, r, p, rho))
{
  stapl_assert(n > 0, "serial_do_loop encountered n == 0");

  auto result = wf(0, z, r, p, rho);

  for (std::size_t i = 1; i < n; ++i)
  {
    vs_map(detail::reset_element(),
           result,
           wf(i, get<0>(result), get<1>(result), get<2>(result), get<3>(result))
    );
  }

  return result;
}


template<typename WF, typename View1D, typename ZetaRef>
auto
serial_do_loop(std::size_t n, WF wf, View1D x, ZetaRef zeta)
  -> decltype(wf(n, x, zeta))
{
  stapl_assert(n > 0, "serial_do_loop encountered n == 0");

  auto result = wf(0, x, zeta);
               
  for (std::size_t i = 1; i < n; ++i)
  {
    vs_map(detail::reset_element(),
           result, 
           wf(i, get<0>(result), get<1>(result))
    );
  }

  return result;
}

} // namespace stapl

#endif // ifndef SERIAL_DO_LOOP_HPP
