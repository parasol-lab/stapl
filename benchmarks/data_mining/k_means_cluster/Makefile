# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifndef STAPL
  STAPL = $(shell echo "$(PWD)" | sed 's,/benchmarks/data_mining/k_means_cluster,,')
endif

include $(STAPL)/GNUmakefile.STAPLdefaults

OBJS:=km_nr.exe km_seq.exe km_stl.exe km_stapl.exe

ifdef MPI_HOME
  OBJS += km_mpi.exe km_mpi_block_cyclic.exe
endif

# cannot compile: km_stapl_block_cyclic.exe

default: all

test: all

all: compile

compile: $(OBJS)


km_seq.o: km_seq.cc
	${CC} -I${STAPL} ${CXXFLAGS} km_seq.cc -c $<

km_stl.o: km_stl.cc
	${CC} -I${STAPL} ${CXXFLAGS} km_stl.cc -c $<

km_mpi.o: km_mpi.cc
	${CC} -I${STAPL} ${CXXFLAGS} km_mpi.cc -c $<

km_mpi_block_cyclic.o: km_mpi_block_cyclic.cc
	${CC} -I${STAPL} ${CXXFLAGS} km_mpi_block_cyclic.cc -c $<

km_stapl.o: km_stapl.cc
	${CC} ${STAPL_CXXFLAGS} ${CXXFLAGS} km_stapl.cc -c $<

km_stapl_block_cyclic.o: km_stapl_block_cyclic.cc
	${CC} ${STAPL_CXXFLAGS} ${CXXFLAGS} km_stapl_block_cyclic.cc -c $<


km_seq.exe: km_seq.o
	${CC} ${CXXFLAGS} km_seq.o -o km_seq.exe ${LIB} ${LIB_EPILOGUE}

km_stl.exe: km_stl.o
	${CC} ${CXXFLAGS} km_stl.o -o km_stl.exe ${LIB} ${LIB_EPILOGUE}

km_nr.exe: km_nr.o
	${CC} ${CXXFLAGS} km_nr.o -o km_nr.exe ${LIB} ${LIB_EPILOGUE}

km_mpi.exe: km_mpi.o
	${CC} ${CXXFLAGS} km_mpi.o -o km_mpi.exe ${LIB} ${LIB_EPILOGUE}

km_mpi_block_cyclic.exe: km_mpi_block_cyclic.o
	${CC} ${CXXFLAGS} km_mpi_block_cyclic.o -o km_mpi_block_cyclic.exe ${LIB} ${LIB_EPILOGUE}

km_stapl.exe: km_stapl.o
	${CC} ${STAPL_CXXFLAGS} ${CXXFLAGS} km_stapl.o -o km_stapl.exe ${STAPL_LIBRARIES} ${LIB} ${LIB_EPILOGUE}

km_stapl_block_cyclic.exe: km_stapl_block_cyclic.o
	${CC} ${STAPL_CXXFLAGS} ${CXXFLAGS} km_stapl_block_cyclic.o -o km_stapl_block_cyclic.exe ${STAPL_LIBRARIES} ${LIB} ${LIB_EPILOGUE}


clean:
	rm -f *.o *.exe

