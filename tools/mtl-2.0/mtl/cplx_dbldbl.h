/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef MTL_CPLX_DLBDLB_H
#define MTL_CPLX_DLBDLB_H

#include "mtl/mtl_complex.h"
#include "doubledouble.h"
#include "mtl/mtl_config.h"

#include <iosfwd>

namespace std {

// stupid g++
inline complex<doubledouble> sqrt(complex<doubledouble> const& cx)
{
  doubledouble re, im;
  if( cx.imag()== doubledouble(0) ) {
    re = sqrt( abs( cx.real() ) );
    im = 0;
  } else {
    re = sqrt ((abs(cx.real()) + abs(cx))*(doubledouble)0.5);
    im = cx.imag()/(2*re);
  }	
  if( cx.real()< doubledouble(0) ) {
    doubledouble temp;
    if( cx.imag()>= doubledouble(0) ) {
      temp=re; re=im; 
    } else {
      temp=-re; re=-im; 
    }	
    im = temp;
  } 
  return complex<doubledouble>(re,im);
}

inline std::ostream& 
operator<<(std::ostream& os, complex<doubledouble> const& x)
{
  cout << "(" << x.real() << "," << x.imag() << ")" << endl;
  return os;
}

inline complex<doubledouble> 
operator/(complex<doubledouble> const & c1, complex<doubledouble> const & c2)
{
  // JGS should check div by zero?
  doubledouble value = norm(c2);
  return complex<doubledouble> (
      (c1.real ()*c2.real ()+c1.imag ()*c2.imag ())/value,
      (c2.real ()*c1.imag ()-c1.real ()*c2.imag ())/value);
}

} /* namespace std */

#endif
